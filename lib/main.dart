import 'package:bloc_pattern/bloc_pattern.dart';
import 'package:faen_app_admin/screens/login_screen.dart';
import 'package:flutter/material.dart';

import 'blocs/orders_bloc.dart';
import 'blocs/revenue_control_bloc.dart';
import 'blocs/user_bloc.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return BlocProvider(
      blocs: [
        Bloc((i) => UserBloc()),
        Bloc((i) => OrdersBloc()),
        Bloc((i) => RevenueControlBloc()),
      ],
      child: MaterialApp(
        title: 'FAEN Admin App',
        theme: ThemeData(
          /*textTheme: TextTheme(
              body1: TextStyle(
                color: Colors.white,
              ),
              body2: TextStyle(
                  color: Colors.white,
                ),
                display1: TextStyle(
                  color: Colors.white,
                ),
                subtitle: TextStyle(
                  color: Colors.white,
                ),
                title: TextStyle(
                  color: Colors.white,
                )
            ),*/
          textTheme: TextTheme(
            bodyText2: TextStyle(color: Colors.white),
          ),
          cardColor: Colors.black.withOpacity(0.70),
          fontFamily: "BebasNeue",
          cursorColor: Color.fromARGB(255, 4, 64, 0),
          focusColor: Colors.green[900],
          scaffoldBackgroundColor: Colors.black87,
          appBarTheme: AppBarTheme(color: Colors.black.withOpacity(0.7)),
          textSelectionColor: Colors.green[900],
          textSelectionHandleColor: Colors.green[900],
          primaryColor: Colors.black,
          //primaryColor: Color.fromARGB(255, 4, 64, 0)
        ),
        debugShowCheckedModeBanner: false,
        home: LoginScreen(),
      ),
    );
  }
}
