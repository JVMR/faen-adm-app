import 'package:cloud_firestore/cloud_firestore.dart';

class FeedData {
  String id;

  String title;
  String subTitle;
  String description;

  String image; //url da imagem se houver
  FeedData.fromDocument(DocumentSnapshot snapshot) {
    id = snapshot.documentID;
    image = snapshot.data["image"];
    title = snapshot.data["title"];
    subTitle = snapshot.data["subTitle"];
    description = snapshot.data["description"];
  }

  Map<String, dynamic> toResumeMap() {
    return {
      "title": title,
      "description": description,
    };
  }
}
