import 'package:fl_chart/fl_chart.dart';
import 'package:flutter/material.dart';
import 'package:flutter_neumorphic/flutter_neumorphic.dart';

class MonthToMonthChart extends StatefulWidget {
  final List<dynamic> datas;

  MonthToMonthChart(this.datas);
  @override
  _MonthToMonthChartState createState() => _MonthToMonthChartState(datas);
}

class _MonthToMonthChartState extends State<MonthToMonthChart> {
  List<Color> gradientColors = [
    //Color.fromARGB(255, 4, 64, 0),
    Colors.green[900],
    const Color(0xff23b6e6),
    //Color.fromARGB(255, 4, 64, 0)
    //const Color(0xff02d39a),
    // Colors.green
  ];
  final List<dynamic> datas;
  _MonthToMonthChartState(this.datas);

  bool showAvg = false;

  @override
  Widget build(BuildContext context) {
    //Color color = Colors.grey[850];
    return Stack(
      children: <Widget>[
        AspectRatio(
          aspectRatio: 1.50,
          child: Neumorphic(
            //margin: EdgeInsets.all(5.0),
            padding: const EdgeInsets.only(
                right: 18.0, left: 12.0, top: 24, bottom: 12),
            drawSurfaceAboveChild: true,
            boxShape: NeumorphicBoxShape.roundRect(
              BorderRadius.circular(12.0),
            ),
            style: NeumorphicStyle(
              depth: -10.0,
              intensity: 0.3,

              //surfaceIntensity: 0.3,
              //shape: NeumorphicShape.convex,
              lightSource: DateTime.now().hour % 2 == 0
                  ? LightSource.topLeft
                  : LightSource.topRight,
              /* page % 2 == 0
            ? LightSource.lerp(LightSource.left, LightSource.left, 1.0)
            : LightSource.lerp(LightSource.right, LightSource.right, 0.5),*/
              color: Colors.black54,
            ),
            child: LineChart(
                // showAvg ? realData() : mainData(),
                realData()),
          ),
          /*Container(
            decoration: const BoxDecoration(
              borderRadius: BorderRadius.all(
                Radius.circular(18),
              ),
              color: Color.fromRGBO(46, 46, 46, 1),
              //color: Color(0xff232d37)
            ),
            child: Padding(
              padding: const EdgeInsets.only(
                  right: 18.0, left: 12.0, top: 24, bottom: 12),
              child: LineChart(
                showAvg ? realData() : mainData(),
              ),
            ),
          ),*/
        ),
        /* SizedBox(
          width: 60,
          height: 34,
          child: FlatButton(
            onPressed: () {
              setState(() {
                showAvg = !showAvg;
              });
            },
            child: Text(
              'avg',
              style: TextStyle(
                  fontSize: 12,
                  color:
                      showAvg ? Colors.white.withOpacity(0.5) : Colors.white),
            ),
          ),
        ),*/
      ],
    );
  }

  LineChartData realData() {
    List<FlSpot> listFlSpot = [];
    Map<int, String> titles = {};
    double maxValueY = -1;
    listFlSpot.insert(0, FlSpot(0.0, 0.0));
    for (int i = 1; i <= datas.length; i++) {
      double valueY = datas[i - 1].data["totalOrderCash"];
      listFlSpot.insert(i, FlSpot(i.toDouble(), valueY));
      titles[valueY.floor()] = valueY.floor().toString();
      if (maxValueY < valueY) {
        maxValueY = valueY;
      }
      //listFlSpot.insert(i, FlSpot(i.toDouble(), i.toDouble() + 1));
      //  print(datas[i].data["totalOrderCash"]);
    }
    print(titles);
    /* listFlSpot.insert(
        datas.length, FlSpot(datas.length.toDouble(), datas.length.toDouble()));*/

    return LineChartData(
      gridData: FlGridData(
        show: true,
        drawVerticalLine: true,
        getDrawingHorizontalLine: (value) {
          return titles[value] != null
              ? FlLine(
                  color: Colors.white54,
                  strokeWidth: 1,
                )
              : FlLine(
                  color: Colors.transparent,
                  strokeWidth: 1,
                );
        },
        getDrawingVerticalLine: (value) {
          return FlLine(
            color: Colors.white54,
            strokeWidth: 1,
          );
        },
      ),
      titlesData: FlTitlesData(
        //Título dos eixo X
        show: true,
        bottomTitles: SideTitles(
          showTitles: true,
          reservedSize: 22,
          textStyle: const TextStyle(
              fontFamily: 'BebasNeue',
              color: Colors.white70,
              fontWeight: FontWeight.bold,
              fontSize: 16),
          getTitles: (value) {
            int index = value.toInt() - 1;
            String title = index == -1 ? "0" : "";
            if (index > -1 && index < datas.length) {
              title = datas[index] != null
                  ? datas[index].documentID.substring(0, 1)
                  : "";
            }
            return title;
            // }

            // return title;
            /* switch (value.toInt()) {
              case 2:
                return 'MAR';
              case 5:
                return 'JUN';
              case 8:
                return 'SEP';
            }
            return '';*/
          },
          margin: 10,
        ),
        leftTitles: SideTitles(
          //Eixo Y
          showTitles: true,
          textStyle: const TextStyle(
            // color: Colors.amber,
            fontFamily: 'BebasNeue',
            color: Colors.white70,
            fontWeight: FontWeight.bold,
            fontSize: 16,
          ),
          getTitles: (value) {
            // String title = value.floorToDouble().toString();
            //return title;
            /*for (bool find = false, int i = 0; !find && i < datas.length; i++) {
              if (doc.data["totalOrderCash"] == value) {
                title = doc.data["totalOrderCash"].toString();
                break;
              }
            }*/
            //print(titles.toString() + value.toString());
            String title =
                titles[value.toInt()] != null ? "R\$" + titles[value] : "";
            return title;
            //return title;

            /*if (value.toInt() < datas.length) {
              String axisYTitle = datas[value.toInt()]
                  .data["totalOrderCash"]
                  .toInt()
                  .toString();
              return axisYTitle;
            } else {
              return "";
            }*/
            /* switch (value.toInt()) {
              case 100:
                return 'R\$100';
              case 200:
                return 'R\$200';
              case 300:
                return 'R\$300';
              case 400:
                return 'R\$400';
            }*/
            // return '';
          },
          reservedSize: 38,
          margin: 10,
        ),
      ),
      borderData: FlBorderData(
          show: true, border: Border.all(color: Colors.white54, width: 1)),
      minX: 0,
      maxX: datas.length.toDouble(),
      minY: 0,
      maxY: maxValueY.ceilToDouble() + 200,
      lineBarsData: [
        LineChartBarData(
          spots: listFlSpot,
          /* datas.map((doc) {
            double x = double.parse(doc.documentID.substring(0, 1));
            return FlSpot(x, doc.data["totalOrderCash"]);
          }).toList(),*/
          /* const [
            //Primeiro número eixo X, segundo número eixo Y
            FlSpot(0, 3),
            FlSpot(2.6, 2),
            FlSpot(4.9, 5),
            FlSpot(6.8, 3.1),
            FlSpot(8, 4),
            FlSpot(9.5, 3),
            FlSpot(11, 4),
          ],*/
          isCurved: true,
          colors: gradientColors,
          barWidth: 5,
          isStrokeCapRound: true,
          dotData: const FlDotData(
            show: true,
            dotColor: Colors.white,
          ),
          belowBarData: BarAreaData(
            show: true,
            colors:
                gradientColors.map((color) => color.withOpacity(0.3)).toList(),
          ),
        ),
      ],
    );
  }

  LineChartData mainData() {
    return LineChartData(
      gridData: FlGridData(
        show: true,
        drawVerticalLine: true,
        getDrawingHorizontalLine: (value) {
          return const FlLine(
            color: Color(0xff37434d),
            strokeWidth: 1,
          );
        },
        getDrawingVerticalLine: (value) {
          return const FlLine(
            color: Color(0xff37434d),
            strokeWidth: 1,
          );
        },
      ),
      titlesData: FlTitlesData(
        //Título dos eixo X
        show: true,
        bottomTitles: SideTitles(
          showTitles: true,
          reservedSize: 22,
          textStyle: const TextStyle(
              color: Color(0xff68737d),
              fontWeight: FontWeight.bold,
              fontSize: 16),
          getTitles: (value) {
            switch (value.toInt()) {
              case 2:
                return 'MAR';
              case 5:
                return 'JUN';
              case 8:
                return 'SEP';
            }
            return '';
          },
          margin: 8,
        ),
        leftTitles: SideTitles(
          //Eixo Y
          showTitles: true,
          textStyle: const TextStyle(
            color: Color(0xff67727d),
            fontWeight: FontWeight.bold,
            fontSize: 15,
          ),
          getTitles: (value) {
            switch (value.toInt()) {
              case 1:
                return '10k';
              case 3:
                return '30k';
              case 5:
                return '50k';
            }
            return '';
          },
          reservedSize: 28,
          margin: 12,
        ),
      ),
      borderData: FlBorderData(
          show: true,
          border: Border.all(color: const Color(0xff37434d), width: 1)),
      minX: 0,
      maxX: 11,
      minY: 0,
      maxY: 6,
      lineBarsData: [
        LineChartBarData(
          spots: const [
            //Primeiro número eixo X, segundo número eixo Y
            FlSpot(0, 3),
            FlSpot(2.6, 2),
            FlSpot(4.9, 5),
            FlSpot(6.8, 3.1),
            FlSpot(8, 4),
            FlSpot(9.5, 3),
            FlSpot(11, 4),
          ],
          isCurved: true,
          colors: gradientColors,
          barWidth: 5,
          isStrokeCapRound: true,
          dotData: const FlDotData(
            show: false,
          ),
          belowBarData: BarAreaData(
            show: true,
            colors:
                gradientColors.map((color) => color.withOpacity(0.3)).toList(),
          ),
        ),
      ],
    );
  }

  LineChartData avgData() {
    return LineChartData(
      lineTouchData: const LineTouchData(enabled: false),
      gridData: FlGridData(
        show: true,
        drawHorizontalLine: true,
        getDrawingVerticalLine: (value) {
          return const FlLine(
            color: Color(0xff37434d),
            strokeWidth: 1,
          );
        },
        getDrawingHorizontalLine: (value) {
          return const FlLine(
            color: Color(0xff37434d),
            strokeWidth: 1,
          );
        },
      ),
      titlesData: FlTitlesData(
        show: true,
        bottomTitles: SideTitles(
          showTitles: true,
          reservedSize: 22,
          textStyle: const TextStyle(
              color: Color(0xff68737d),
              fontWeight: FontWeight.bold,
              fontSize: 16),
          getTitles: (value) {
            switch (value.toInt()) {
              case 2:
                return 'MAR';
              case 5:
                return 'JUN';
              case 8:
                return 'SEP';
            }
            return '';
          },
          margin: 8,
        ),
        leftTitles: SideTitles(
          showTitles: true,
          textStyle: const TextStyle(
            color: Color(0xff67727d),
            fontWeight: FontWeight.bold,
            fontSize: 15,
          ),
          getTitles: (value) {
            switch (value.toInt()) {
              case 1:
                return '10k';
              case 3:
                return '30k';
              case 5:
                return '50k';
            }
            return '';
          },
          reservedSize: 28,
          margin: 12,
        ),
      ),
      borderData: FlBorderData(
          show: true,
          border: Border.all(color: const Color(0xff37434d), width: 1)),
      minX: 0,
      maxX: 11,
      minY: 0,
      maxY: 6,
      lineBarsData: [
        LineChartBarData(
          spots: const [
            FlSpot(0, 3.44),
            FlSpot(2.6, 3.44),
            FlSpot(4.9, 3.44),
            FlSpot(6.8, 3.44),
            FlSpot(8, 3.44),
            FlSpot(9.5, 3.44),
            FlSpot(11, 3.44),
          ],
          isCurved: true,
          colors: [
            ColorTween(begin: gradientColors[0], end: gradientColors[1])
                .lerp(0.2),
            ColorTween(begin: gradientColors[0], end: gradientColors[1])
                .lerp(0.2),
          ],
          barWidth: 5,
          isStrokeCapRound: true,
          dotData: const FlDotData(
            show: false,
          ),
          belowBarData: BarAreaData(show: true, colors: [
            ColorTween(begin: gradientColors[0], end: gradientColors[1])
                .lerp(0.2)
                .withOpacity(0.1),
            ColorTween(begin: gradientColors[0], end: gradientColors[1])
                .lerp(0.2)
                .withOpacity(0.1),
          ]),
        ),
      ],
    );
  }
}
