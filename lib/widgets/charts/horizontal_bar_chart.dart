import 'dart:math';
import 'package:charts_flutter/flutter.dart' as charts;
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:flutter_neumorphic/flutter_neumorphic.dart';

class HorizontalBarChart extends StatelessWidget {
  final List<charts.Series> seriesList;
  final bool animate;

  HorizontalBarChart(this.seriesList, {this.animate});

  /// Creates a [BarChart] with sample data and no transition.
  factory HorizontalBarChart.withSampleData() {
    return new HorizontalBarChart(
      _createSampleData(),
      // Disable animations for image tests.
      animate: false,
    );
  }

  // EXCLUDE_FROM_GALLERY_DOCS_START
  // This section is excluded from being copied to the gallery.
  // It is used for creating random series data to demonstrate animation in
  // the example app only.
  factory HorizontalBarChart.withRandomData() {
    return new HorizontalBarChart(_createRandomData());
  }

  factory HorizontalBarChart.withRealData(DocumentSnapshot document) {
    return new HorizontalBarChart(_createSeriesData(document), animate: true);
  }

  /// Create random data.
  static List<charts.Series<OrdinalSales, String>> _createRandomData() {
    final random = new Random();

    final data = [
      new OrdinalSales('2014', random.nextInt(100)),
      new OrdinalSales('2015', random.nextInt(100)),
      new OrdinalSales('2016', random.nextInt(100)),
      new OrdinalSales('2017', random.nextInt(100)),
    ];

    return [
      new charts.Series<OrdinalSales, String>(
        id: 'Sales',
        domainFn: (OrdinalSales sales, _) => sales.year,
        measureFn: (OrdinalSales sales, _) => sales.sales,
        data: data,
        // Set a label accessor to control the text of the bar label.
        labelAccessorFn: (OrdinalSales sales, _) =>
            '${sales.year}: \$${sales.sales.toString()}',
        insideLabelStyleAccessorFn: (OrdinalSales sales, _) {
          final color = (sales.year == '2014')
              ? charts.MaterialPalette.red.shadeDefault
              : charts.MaterialPalette.yellow.shadeDefault.darker;
          return new charts.TextStyleSpec(color: color);
        },
        outsideLabelStyleAccessorFn: (OrdinalSales sales, _) {
          final color = (sales.year == '2014')
              ? charts.MaterialPalette.red.shadeDefault
              : charts.MaterialPalette.yellow.shadeDefault.darker;
          return new charts.TextStyleSpec(color: color);
        },
      )
    ];
  }

  static List<charts.Series<dynamic, String>> _createSeriesData(
      DocumentSnapshot document) {
    //final random = new Random();
    //List<String> exemple = [];
    final data = document.data["titlesProducts"].map((title) {
      return SoldData(title, document.data["productsSold"][title]);
    }).toList();
    print(data);

    return [
      new charts.Series<dynamic, String>(
        id: 'Sales',
        domainFn: (dynamic sales, _) => sales.title,
        measureFn: (dynamic sales, _) => sales.solds,
        //  overlaySeries: true,
        areaColorFn: (sales, _) {
          return charts.ColorUtil.fromDartColor(Colors.green[900]);
        },

        seriesColor: charts.ColorUtil.fromDartColor(Colors.green[900]),
        data: data,

        // Set a label accessor to control the text of the bar label.
        labelAccessorFn: (dynamic sales, _) =>
            '${sales.title}: ${sales.solds.toString()}',
        insideLabelStyleAccessorFn: (dynamic sales, _) {
          final color = (sales.title == '2014')
              ? charts.MaterialPalette.red.shadeDefault
              : charts.MaterialPalette.yellow.shadeDefault.darker;
          return new charts.TextStyleSpec(
            color: color,
          );
        },
        outsideLabelStyleAccessorFn: (dynamic sales, _) {
          final color = (sales.title == '2014')
              ? charts.MaterialPalette.red.shadeDefault
              : charts.MaterialPalette.yellow.shadeDefault.darker;
          return new charts.TextStyleSpec(color: color);
        },
      )
    ];
  }

  @override
  Widget build(BuildContext context) {
    // For horizontal bar charts, set the [vertical] flag to false.
    return Neumorphic(
      margin: EdgeInsets.zero,
      padding:
          const EdgeInsets.only(right: 5.0, left: 5.0, top: 5.0, bottom: 10),
      drawSurfaceAboveChild: true,
      boxShape: NeumorphicBoxShape.roundRect(
        BorderRadius.circular(12.0),
      ),
      style: NeumorphicStyle(
        depth: -10.0,
        intensity: 0.3,

        //surfaceIntensity: 0.3,
        //shape: NeumorphicShape.convex,
        lightSource: DateTime.now().hour % 2 == 0
            ? LightSource.topLeft
            : LightSource.topRight,
        /* page % 2 == 0
            ? LightSource.lerp(LightSource.left, LightSource.left, 1.0)
            : LightSource.lerp(LightSource.right, LightSource.right, 0.5),*/
        color: Colors.black54,
      ),
      child: charts.BarChart(
        seriesList,
        animate: animate,
        vertical: false,
        barRendererDecorator: charts.BarLabelDecorator<String>(
            outsideLabelStyleSpec: charts.TextStyleSpec(
          color: charts.MaterialPalette.yellow.shadeDefault.darker,
        )),
        // Hide domain axis.
        domainAxis: charts.OrdinalAxisSpec(renderSpec: charts.NoneRenderSpec()),
      ),
    );
    /*return Container(
      decoration: const BoxDecoration(
        borderRadius: BorderRadius.all(
          Radius.circular(18),
        ),
        color: Color.fromRGBO(46, 46, 46, 1),
        //color: Color(0xff232d37)
      ),
      child: charts.BarChart(
        seriesList,
        animate: animate,
        vertical: false,
        barRendererDecorator: charts.BarLabelDecorator<String>(
            outsideLabelStyleSpec: charts.TextStyleSpec(
          color: charts.MaterialPalette.yellow.shadeDefault.darker,
        )),
        // Hide domain axis.
        domainAxis: charts.OrdinalAxisSpec(renderSpec: charts.NoneRenderSpec()),
      ),
    );*/
  }

  /// Create one series with sample hard coded data.
  static List<charts.Series<OrdinalSales, String>> _createSampleData() {
    final data = [
      new OrdinalSales('2014', 5),
      new OrdinalSales('2015', 25),
      new OrdinalSales('2016', 100),
      new OrdinalSales('2017', 75),
    ];

    return [
      new charts.Series<OrdinalSales, String>(
        id: 'Sales',
        domainFn: (OrdinalSales sales, _) => sales.year,
        measureFn: (OrdinalSales sales, _) => sales.sales,
        data: data,
      )
    ];
  }
}

class SoldData {
  final String title;
  final int solds;

  SoldData(this.title, this.solds);
}

/// Sample ordinal data type.
class OrdinalSales {
  final String year;
  final int sales;

  OrdinalSales(this.year, this.sales);
}
