import 'package:faen_app_admin/widgets/tiles/drawer_tile.dart';
import 'package:flutter/material.dart';

class CustomDrawer extends StatelessWidget {
  final PageController pageController;

  CustomDrawer(this.pageController);

  @override
  Widget build(BuildContext context) {
    Widget _buildDrawerBack() => Container(
          decoration: BoxDecoration(
            gradient: LinearGradient(
              colors: [
                Colors.white,
                Theme.of(context).primaryColor,
              ],
              begin: Alignment.topCenter,
              end: Alignment.bottomCenter,
            ),
          ),
        );

    return Drawer(
      child: Stack(
        children: <Widget>[
          _buildDrawerBack(),
          ListView(
            padding: EdgeInsets.only(left: 32.0, top: 16.0),
            children: <Widget>[
              Container(
                margin: EdgeInsets.only(bottom: 8.0),
                padding: EdgeInsets.fromLTRB(0.0, 16.0, 16.0, 8.0),
                height: 170.0,
                child: Stack(
                  children: <Widget>[
                    Positioned(
                      top: 8.0,
                      left: 0.0,
                      child: Text(
                        "FAEN App - Admin",
                        style: TextStyle(
                          //fontFamily: 'LemonMilk',
                          fontSize: 34.0,
                          /*fontWeight: FontWeight.w700*/
                        ),
                      ),
                    ),
                    Positioned(
                      left: 0.0,
                      bottom: 0.0,
                      child: Container(
                        child: Column(
                          children: <Widget>[
                            Text(
                              "Bem-vindo Administrador !",
                              style: TextStyle(
                                fontSize: 18.0,
                                fontWeight: FontWeight.bold,
                              ),
                            ),
                            SizedBox(
                              height: 5,
                            ),
                          ],
                        ),
                      ),
                    ),
                  ],
                ),
              ),
              Divider(),
              DrawerTile(Icons.person, "Clientes", pageController, 0),
              DrawerTile(Icons.shopping_cart, "Pedidos", pageController, 1),
              DrawerTile(Icons.list, "Produtos", pageController, 2),
              DrawerTile(Icons.people, "Parceria", pageController, 3),
            ],
          ),
        ],
      ),
    );
  }
}
