import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:faen_app_admin/screens/edit_post_feed_screen.dart';
import 'package:flutter/material.dart';
import 'package:flutter_markdown/flutter_markdown.dart';
import 'package:transparent_image/transparent_image.dart';

class FeedCard extends StatelessWidget {
  final DocumentSnapshot feedData;

  FeedCard(this.feedData);

  @override
  Widget build(BuildContext context) {
    return Card(
      elevation: 8.0,
      color: Colors.black,
      margin: EdgeInsets.symmetric(horizontal: 8.0, vertical: 4.0),
      child: InkWell(
        onTap: () {
          Navigator.of(context).push(MaterialPageRoute(
              builder: (context) => EditPostFeedScreen(postFeed: feedData)));
        },
        child: Column(
          mainAxisSize: MainAxisSize.min,
          crossAxisAlignment: CrossAxisAlignment.stretch,
          children: <Widget>[
            Flexible(
                flex: 2,
                child: feedData.data["image"] != null
                    ? FadeInImage.memoryNetwork(
                        placeholder: kTransparentImage,
                        image: feedData.data["image"],
                        fit: BoxFit.cover,
                        height: (MediaQuery.of(context).size.width * 0.98) / 2,
                        width: MediaQuery.of(context).size.width * 0.98)
                    : Icon(Icons.error_outline,
                        color: Colors.red[900], size: 96)),
            //SizedBox(height: 5.0),
            Flexible(
                flex: 1,
                child: Padding(
                  padding: const EdgeInsets.all(10.0),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    mainAxisSize: MainAxisSize.min,
                    children: <Widget>[
                      SelectableText.rich(
                        TextSpan(
                          text: feedData.data["title"] + "\n",
                          style: TextStyle(
                              fontWeight: FontWeight.bold,
                              color: Colors.white,
                              fontSize: 18.0),
                          children: <TextSpan>[
                            TextSpan(
                              text: feedData.data["subTitle"] + "\n",
                              style: TextStyle(
                                  height: 1.4,
                                  fontWeight: FontWeight.w600,
                                  color: Colors.white70,
                                  fontSize: 16.0),
                            ),
                            /*TextSpan(
                              text: feedData.data["description"],
                              style: TextStyle(
                                  fontWeight: FontWeight.normal,
                                  color: Colors.white70,
                                  fontSize: 16.0),
                            ),*/
                          ],
                        ),
                        textAlign: TextAlign.justify,
                      ),
                      /*MarkdownBody(
                        data: feedData.data["description"],
                      ),*/
                      _buildMarkdown(
                          feedData.data["description"] ?? "", context),
                    ],
                  ),
                )),
            // SizedBox(height: 5.0),
          ],
        ),
      ),
    );
  }

  Widget _buildMarkdown(String data, BuildContext context) {
    return MarkdownBody(
      data: data,
      styleSheet: MarkdownStyleSheet(
        checkbox: TextStyle(color: Colors.white),
        h1Align: WrapAlignment.center,
        textAlign: WrapAlignment.spaceBetween,
        p: Theme.of(context).textTheme.bodyText2.copyWith(
              fontFamily: 'Lato',
              color: Colors.white70,
              fontSize: 16.0,
            ),
        h1: Theme.of(context).textTheme.headline5.copyWith(
              fontFamily: 'Lato',
              color: Colors.white70,
              fontSize: 16.0,
            ),
        h2: Theme.of(context).textTheme.headline6.copyWith(
              fontFamily: 'Lato',
              color: Colors.white70,
              fontSize: 16.0,
            ),
        h3: Theme.of(context).textTheme.subtitle2.copyWith(
              fontFamily: 'Lato',
              color: Colors.white70,
              fontSize: 16.0,
            ),
        h4: Theme.of(context).textTheme.bodyText1.copyWith(
              fontFamily: 'Lato',
              color: Colors.white70,
              fontSize: 16.0,
            ),
        h5: Theme.of(context).textTheme.bodyText1.copyWith(
              fontFamily: 'Lato',
              color: Colors.white70,
              fontSize: 16.0,
            ),
        h6: Theme.of(context).textTheme.bodyText1.copyWith(
              fontFamily: 'Lato',
              color: Colors.white70,
              fontSize: 16.0,
            ),
      ),
    );
  }
}
