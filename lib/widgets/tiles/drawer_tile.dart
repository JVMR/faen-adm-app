import 'package:flutter/material.dart';

class DrawerTile extends StatelessWidget {
  final IconData icon;
  final String text;
  final PageController controller;
  final int page;

  DrawerTile(this.icon, this.text, this.controller, this.page);

  @override
  Widget build(BuildContext context) {
    return Material(
      borderRadius: BorderRadius.only(
        topLeft: Radius.circular(12.5),
        bottomLeft: Radius.circular(12.5),
      ),
      color: controller.page.round() == page
          ? Color.fromRGBO(255, 255, 255, 0.7)
          : Colors.transparent,
      child: InkWell(
        onTap: () {
          Navigator.of(context).pop();

          controller.jumpToPage(page);
        },
        child: Container(
          height: 60.0,
          child: Row(
            children: <Widget>[
              Icon(
                icon,
                size: 32.0,
                color: controller.page.round() == page
                    ? Theme.of(context).primaryColor
                    : Colors.black,
              ),
              SizedBox(width: 32.0),
              Text(
                text,
                style: TextStyle(
                  fontSize: 16.0,
                  fontWeight: controller.page.round() == page
                      ? FontWeight.bold
                      : FontWeight.normal,
                  color: controller.page.round() == page
                      ? Theme.of(context).primaryColor
                      : Colors.black,
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
