import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:faen_app_admin/screens/edit_product_screen.dart';
import 'package:faen_app_admin/widgets/dialogs/edit_category_dialog.dart';
import 'package:flutter/material.dart';

class CategoryTile extends StatefulWidget {
  final DocumentSnapshot category;

  CategoryTile(this.category);

  @override
  _CategoryTileState createState() => _CategoryTileState();
}

class _CategoryTileState extends State<CategoryTile> {
  bool isExpanded = false;
  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.symmetric(horizontal: 16, vertical: 4),
      child: Card(
        child: ExpansionTile(
          onExpansionChanged: (value) {
            setState(() {
              isExpanded = value;
            });
          },
          leading: GestureDetector(
            onTap: () {
              showDialog(
                  context: context,
                  builder: (context) => EditCategoryDialog(
                        category: widget.category,
                      ));
            },
            child: CircleAvatar(
              backgroundImage: widget.category.data["icon"] != null
                  ? NetworkImage(widget.category.data["icon"])
                  : null,
              backgroundColor: Colors.green[900],
              child: widget.category.data["icon"] != null
                  ? null
                  : Text(
                      "${widget.category.data["title"].substring(0, 1)}",
                      style: TextStyle(color: Colors.white),
                    ),
            ),
          ),
          title: Text(
            widget.category.data["title"],
            style: TextStyle(
                color: isExpanded ? Colors.white70 : Colors.white,
                fontWeight: FontWeight.w500),
          ),
          children: <Widget>[
            FutureBuilder<QuerySnapshot>(
              future:
                  widget.category.reference.collection("items").getDocuments(),
              builder: (context, snapshot) {
                if (!snapshot.hasData) return Container();
                return Column(
                  children: snapshot.data.documents.map((doc) {
                    return ListTile(
                      leading: CircleAvatar(
                        backgroundImage: NetworkImage(doc.data["images"][0]),
                      ),
                      title: Text(doc.data["title"],
                          style: TextStyle(color: Colors.white)),
                      trailing:
                          Text("R\$${doc.data["price"].toStringAsFixed(2)}"),
                      onTap: () {
                        Navigator.of(context).push(MaterialPageRoute(
                            builder: (context) => ProductScreen(
                                  categoryId: widget.category.documentID,
                                  product: doc,
                                )));
                      },
                    );
                  }).toList()
                    ..add(ListTile(
                      leading: CircleAvatar(
                        backgroundColor: Colors.transparent,
                        child: Icon(
                          Icons.add,
                          color: Colors.white,
                        ),
                      ),
                      title: Text("Adicionar",
                          style: TextStyle(color: Colors.white)),
                      onTap: () {
                        Navigator.of(context).push(MaterialPageRoute(
                            builder: (context) => ProductScreen(
                                  categoryId: widget.category.documentID,
                                )));
                      },
                    )),
                );
              },
            )
          ],
        ),
      ),
    );
  }
}
