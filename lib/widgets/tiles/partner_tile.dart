import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:faen_app_admin/screens/edit_partner_screen.dart';
import 'package:flutter/material.dart';

class PartnerTile extends StatelessWidget {
  final DocumentSnapshot partner;

  PartnerTile(this.partner);
  @override
  Widget build(BuildContext context) {
    return Card(
      elevation: 8.0,
      // color: Colors.black,
      margin: EdgeInsets.symmetric(horizontal: 8, vertical: 5),
      child: InkWell(
        onTap: () {
          Navigator.of(context).push(MaterialPageRoute(
              builder: (context) => EditPartnerScreen(
                    partner: partner,
                  )));
        },
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.stretch,
          mainAxisAlignment: MainAxisAlignment.start,
          mainAxisSize: MainAxisSize.min,
          children: <Widget>[
            Flexible(
              flex: 6,
              child: Padding(
                padding: const EdgeInsets.fromLTRB(8, 10, 5, 8),
                child: SizedBox(
                  height: 140,
                  width: 140,
                  child: Hero(
                    tag: partner.data["image"],
                    child: CircleAvatar(
                      //radius: 25,
                      backgroundColor: Colors.green[900],
                      backgroundImage: partner.data["image"] != null
                          ? NetworkImage(partner.data["image"])
                          : null,
                    ),
                  ),
                ),
              ),
            ),
            Flexible(
              flex: 1,
              child: Padding(
                padding: const EdgeInsets.only(
                    top: 5.0, bottom: 5.0, right: 2.0, left: 2.0),
                child: Text(partner.data["title"],
                    overflow: TextOverflow.ellipsis,
                    softWrap: true,
                    textAlign: TextAlign.center,
                    style: TextStyle(
                        fontWeight: FontWeight.bold, color: Colors.white)),
              ),
            ),
            Divider(color: Colors.grey[600]),
            Flexible(
              flex: 1,
              child: Padding(
                padding: const EdgeInsets.only(top: 2.0, bottom: 2.0),
                child: Text(
                  partner.data["subTitle"],
                  textAlign: TextAlign.center,
                  style: TextStyle(color: Colors.white),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
