import 'package:faen_app_admin/screens/edit_user_screen.dart';
import 'package:faen_app_admin/widgets/dialogs/user_info_dialog.dart';
import 'package:flutter/material.dart';
import 'package:shimmer/shimmer.dart';

class UserTile extends StatelessWidget {
  final Map<String, dynamic> user;
  final String typeAdmUser;

  UserTile(this.user, this.typeAdmUser);

  @override
  Widget build(BuildContext context) {
    final textStyle = TextStyle(color: Colors.white);

    if (user.containsKey("money"))
      return Container(
        color: Colors.black87,
        child: ListTile(
          //dense: true,
          /*  onTap: () {
            Navigator.of(context).push(
                MaterialPageRoute(builder: (context) => EditUserScreen(user)));
          },*/
          onTap: typeAdmUser == "Parceiro"
              ? () {
                  showDialog(
                      context: context,
                      builder: (context) => UserInfoDialog(user));
                }
              : () {
                  Navigator.of(context).push(MaterialPageRoute(
                      builder: (context) => EditUserScreen(user)));
                },
          leading: Hero(
            tag: user["photo"] ?? user["name"],
            child: CircleAvatar(
              backgroundColor: Color.fromARGB(255, 4, 64, 0),
              backgroundImage:
                  user["photo"] != null ? NetworkImage(user["photo"]) : null,
              child: user["photo"] == null
                  ? Text(
                      "${user["name"].substring(0, 1)}",
                      style: TextStyle(color: Colors.white),
                    )
                  : null,
              /* FadeInImage.memoryNetwork(
                placeholder: kTransparentImage,
                image: user["photo"],
                fit: BoxFit.cover,
              ),*/
            ),
          ),
          //contentPadding: EdgeInsets.symmetric(horizontal: 16.0),
          title: Text(
            user["name"],
            style: TextStyle(
                fontFamily: 'Lato',
                // fontWeight: FontWeight.w700,
                color: Colors.white),
          ),
          subtitle: Text(
            user["typeUser"],
            style: textStyle,
          ),
          trailing: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.end,
            children: <Widget>[
              Text(
                "Pedidos: ${user["orders"]}",
                textAlign: TextAlign.right,
                style: TextStyle(
                    fontFamily: 'Lato',
                    // fontWeight: FontWeight.bold,
                    color: Colors.white),
              ),
              Text(
                "Gasto: R\$${user["money"].toStringAsFixed(2)}",
                style: TextStyle(
                    fontFamily: 'Lato',
                    //fontWeight: FontWeight.bold,
                    color: Colors.white),
              )
            ],
          ),
        ),
      );
    else
      return Container(
        color: Colors.black87,
        margin: EdgeInsets.symmetric(horizontal: 16, vertical: 8),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.start,
          crossAxisAlignment: CrossAxisAlignment.stretch,
          mainAxisSize: MainAxisSize.min,
          children: <Widget>[
            SizedBox(
              width: 50,
              height: 50,
              child: Shimmer.fromColors(
                  child: Container(
                    margin: EdgeInsets.symmetric(vertical: 4),
                    decoration: BoxDecoration(
                      shape: BoxShape.circle,
                      color: Colors.white.withAlpha(50),
                    ),
                  ),
                  baseColor: Colors.white,
                  highlightColor: Colors.grey),
            ),
            Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                SizedBox(
                  width: 200,
                  height: 20,
                  child: Shimmer.fromColors(
                      child: Container(
                        color: Colors.white.withAlpha(50),
                        margin: EdgeInsets.symmetric(vertical: 4),
                      ),
                      baseColor: Colors.white,
                      highlightColor: Colors.grey),
                ),
                SizedBox(
                  width: 50,
                  height: 20,
                  child: Shimmer.fromColors(
                      child: Container(
                        color: Colors.white.withAlpha(50),
                        margin: EdgeInsets.symmetric(vertical: 4),
                      ),
                      baseColor: Colors.white,
                      highlightColor: Colors.grey),
                )
              ],
            ),
          ],
        ),
      );
  }
}
