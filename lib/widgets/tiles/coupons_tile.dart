import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:flutter_neumorphic/flutter_neumorphic.dart';

class CouponsTile extends StatelessWidget {
  final DocumentSnapshot coupon;
  final VoidCallback setUpdate;

  CouponsTile(this.coupon, this.setUpdate);

  @override
  Widget build(BuildContext context) {
    return Neumorphic(
      style: NeumorphicStyle(
        depth: -20.0,
        intensity: 0.30,
        lightSource: LightSource.top,
        //surfaceIntensity: 1.0,
        color: Colors.black87,
        //intensity: 0.5,
      ),
      child: ListTile(
        leading: Icon(Icons.card_giftcard, color: Colors.white, size: 35.0),
        title: RichText(
          text: TextSpan(
              text: "Código: ",
              style: TextStyle(fontWeight: FontWeight.normal, fontSize: 15.0),
              children: <TextSpan>[
                TextSpan(
                    text: coupon.documentID,
                    style:
                        TextStyle(fontWeight: FontWeight.bold, fontSize: 16.0))
              ]),
        ),
        subtitle: RichText(
          text: TextSpan(
              text: "Desconto: ",
              style: TextStyle(fontWeight: FontWeight.normal, fontSize: 15.0),
              children: <TextSpan>[
                TextSpan(
                    text: coupon.data["percent"].toString() + "%",
                    style:
                        TextStyle(fontWeight: FontWeight.bold, fontSize: 16.0))
              ]),
        ),
        trailing: IconButton(
            icon: Icon(
              Icons.delete_outline,
              color: Colors.red[900],
              size: 30.0,
            ),
            onPressed: () async {
              showAlertDialog(context);
            }),
      ),
    );
  }

  showAlertDialog(BuildContext context) {
    Widget botaoVoltar = FlatButton(
      color: Colors.transparent,
      child: Text(
        "Cancelar",
        style: TextStyle(color: Theme.of(context).primaryColor),
      ),
      onPressed: () {
        Navigator.pop(context);
      },
    );
    Widget botaoDeletar = FlatButton(
      color: Colors.transparent,
      child: Text(
        "Deletar",
        style: TextStyle(color: Colors.red[900]),
      ),
      onPressed: () {
        coupon.reference.delete();
        Navigator.pop(context);
        setUpdate();
        /*Navigator.of(context).pushAndRemoveUntil(
            MaterialPageRoute(builder: (context) => CouponsScreen()),
            ModalRoute.withName("HomeTab"));*/
      },
    );
    showDialog(
      context: context,
      builder: (BuildContext context) {
        return AlertDialog(
            backgroundColor: Colors.grey[850].withOpacity(0.70),
            elevation: 15.0,
            shape: OutlineInputBorder(
              borderSide: BorderSide(color: Colors.white10),
              borderRadius: BorderRadius.circular(15.0),
            ),
            title: Text("Tem certeza que deseja apagar ${coupon.documentID} ?",
                style: TextStyle(color: Colors.white),
                textAlign: TextAlign.center),
            content: Text(
                "Se deletado, não será possível recuperar o cupom.\n"
                "Caso queira, após ter deletado você pode criar um novo cupom com o mesmo código e desconto.",
                style: TextStyle(color: Colors.white),
                textAlign: TextAlign.justify),
            actions: [botaoVoltar, botaoDeletar]);
      },
    );
  }
}
