import 'package:bloc_pattern/bloc_pattern.dart';
import 'package:faen_app_admin/blocs/user_bloc.dart';
import 'package:flutter/material.dart';

class AdmHeaderTile extends StatefulWidget {
  @override
  _AdmHeaderTileState createState() => _AdmHeaderTileState();
}

class _AdmHeaderTileState extends State<AdmHeaderTile> {
  DateTime date = DateTime.now().toLocal();
  final _userBloc = BlocProvider.getBloc<UserBloc>();
  Map<String, dynamic> admData;
  String saudacao = "";
  String name = "";

  @override
  void initState() {
    super.initState();
    admData = _userBloc.admUserData;
    var fullName = admData["name"].split(" ");
    name = fullName[0] + " " + fullName[1] + " : )";
    saudacao = getSaudacao();
  }

  @override
  Widget build(BuildContext context) {
    return Card(
      margin: EdgeInsets.only(bottom: 8.0),
      color: Colors.black.withOpacity(0.70),
      shape: OutlineInputBorder(
        borderSide: BorderSide(color: Colors.white10),
        borderRadius: BorderRadius.circular(15.0),
      ),
      child: ListTile(
        leading: CircleAvatar(
          backgroundColor: Color.fromARGB(255, 4, 64, 0),
          backgroundImage:
              admData["photo"] != null ? NetworkImage(admData["photo"]) : null,
          child: admData["photo"] == null
              ? Text(
                  "${admData["name"].substring(0, 1)}",
                  style: TextStyle(color: Colors.white),
                )
              : null,
        ),
        title: RichText(
            text: TextSpan(
                text: "$saudacao, ",
                style: TextStyle(
                  fontWeight: FontWeight.bold,
                  fontSize: 16,
                ),
                children: <TextSpan>[
              TextSpan(
                  text: name, style: TextStyle(fontWeight: FontWeight.normal)),
            ])),
        subtitle: Text(
          admData["typeUser"],
          style: TextStyle(
            fontFamily: 'BebasNeue',
            color: Colors.white,
          ),
        ),
      ),
    );
  }

  String getSaudacao() {
    int atualHour = date.hour;
    if (atualHour > 4 && atualHour <= 11) {
      //Manhã das 5hrs às 12:59
      return "Bom dia";
    } else if (atualHour > 11 && atualHour <= 18) {
      //Tarde das 13hrs Às 18hrs
      return "Boa tarde";
    } else if (atualHour > 18 && atualHour <= 23) {
      return "Boa noite";
    } else {
      return "Boa madrugada";
    }
  }
}
