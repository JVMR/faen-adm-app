import 'package:bloc_pattern/bloc_pattern.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:faen_app_admin/blocs/revenue_control_bloc.dart';
import 'package:faen_app_admin/models/notification_sender.dart';
import 'package:faen_app_admin/screens/order_screen.dart';
import 'package:flutter/material.dart';

import '../order_header.dart';

class OrderTile extends StatelessWidget {
  final DocumentSnapshot order;
  final NotificationSender _notificationSender = NotificationSender();

  OrderTile(this.order);

  /*final states = [
    "",
    "Em preparação",
    "Em transporte",
    "Aguardando Entrega",
    "Entregue"
  ];*/
  final states = [
    "",
    "Aguardando pagamento",
    "Pedido em preparação",
    "Pedido pronto",
    "Entregue"
  ];

  @override
  Widget build(BuildContext context) {
    String bitIdOrder =
        "#${order.documentID.substring(order.documentID.length - 7, order.documentID.length)}";
    final _revenueControlBloc = BlocProvider.getBloc<RevenueControlBloc>();
    return Container(
      margin: EdgeInsets.symmetric(horizontal: 16, vertical: 4),
      child: Card(
        child: ExpansionTile(
          key: Key(order.documentID),
          initiallyExpanded: order.data["status"] != 4,
          title: Text(
            "$bitIdOrder - "
            "${states[order.data["status"]]}",
            style: TextStyle(
                color:
                    order.data["status"] != 4 ? Colors.white70 : Colors.green),
          ),
          children: <Widget>[
            Padding(
              padding: EdgeInsets.only(left: 16, right: 16, top: 0, bottom: 8),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.stretch,
                children: <Widget>[
                  OrderHeader(order),
                  Column(
                    mainAxisSize: MainAxisSize.min,
                    children: order.data["products"].map<Widget>((p) {
                      String size = p["size"] ?? "";
                      return ListTile(
                        // title: Text(p["product"]["title"] + " " + size),
                        title: RichText(
                          text: TextSpan(
                            text: p["product"]["title"] + " ",
                            style: TextStyle(
                                fontFamily: 'BebasNeue',
                                fontSize: 15.0,
                                color: Colors.white,
                                fontWeight: FontWeight.w500),
                            children: <TextSpan>[
                              TextSpan(
                                text: size,
                                style: TextStyle(
                                    color: Colors.white,
                                    fontWeight: FontWeight.w400),
                              ),
                            ],
                          ),
                        ),
                        subtitle: Text(
                          p["category"] + "/" + p["pid"],
                          style: TextStyle(
                              color: Colors.white, fontStyle: FontStyle.italic),
                        ),
                        trailing: Text(
                          p["quantity"].toString(),
                          style: TextStyle(color: Colors.white, fontSize: 20),
                        ),
                        contentPadding: EdgeInsets.zero,
                      );
                    }).toList(),
                  ),
                  RichText(
                    text: TextSpan(
                        text: "Pedido realizado em ",
                        style: TextStyle(
                            fontFamily: 'BebasNeue',
                            color: Colors.white,
                            fontSize: 14.0,
                            fontStyle: FontStyle.italic),
                        children: <TextSpan>[
                          TextSpan(
                            text: order.data["orderDate"],
                            style: TextStyle(
                                color: Colors.white,
                                fontStyle: FontStyle.italic,
                                fontWeight: FontWeight.bold),
                          ),
                        ]),
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: <Widget>[
                      FlatButton(
                        onPressed: () {
                          //showAlertDialog(context, "$bitIdOrder");
                          Navigator.of(context).push(MaterialPageRoute(
                              builder: (context) => OrderScreen(order)));
                          /*Firestore.instance
                              .collection("users")
                              .document(order["clientId"])
                              .collection("orders")
                              .document(order.documentID)
                              .delete();
                          order.reference.delete();*/
                        },
                        textColor: Colors.blue[800],
                        // child: Text("Excluir"),
                        child: Text("+ Info"),
                      ),
                      FlatButton(
                        onPressed: order.data["status"] > 1
                            ? () {
                                order.reference.updateData(
                                    {"status": order.data["status"] - 1});
                              }
                            : null,
                        disabledTextColor: Colors.white30,
                        textColor: Colors.white70,
                        child: Text("Regredir"),
                      ),
                      FlatButton(
                        onPressed: order.data["status"] < 4
                            ? () async {
                                await order.reference.updateData(
                                    {"status": order.data["status"] + 1});
                                sendNotificationForStatusUpdate(
                                    order.data["clientId"],
                                    order.data["status"] + 1);
                                if (order.data["status"] == 3) {
                                  _revenueControlBloc.addCash(
                                      order.data["totalPrice"].toString());
                                }
                              }
                            : null,
                        textColor: Colors.green,
                        child: Text("Avançar"),
                      )
                    ],
                  )
                ],
              ),
            )
          ],
        ),
      ),
    );
  }

  sendNotificationForStatusUpdate(String uid, int status) async {
    String bodyNotification = status == 3
        ? "O seu pedido está pronto para ser retirado : )"
        : "Status atual: ${states[status]}";
    bool sucesso = await _notificationSender.sendNotificationToUser(
        title: "O status do pedido foi atualizado !",
        body: bodyNotification,
        uid: uid);
    print("Retorno da notificação $sucesso");
  }

  showAlertDialog(context, String bitIdOrder) {
    Widget botaoDeletar = FlatButton(
      color: Colors.transparent,
      child: Text(
        "Deletar",
        style: TextStyle(color: Colors.red[900]),
      ),
      onPressed: () {
        //Navigator.pop(context);
        Firestore.instance
            .collection("users")
            .document(order["clientId"])
            .collection("orders")
            .document(order.documentID)
            .delete();
        order.reference.delete();
        Navigator.pop(context);
      },
    );
    Widget botaoCancelar = FlatButton(
      color: Colors.transparent,
      child: Text(
        "Cancelar",
        style: TextStyle(color: Colors.green[900]),
      ),
      onPressed: () {
        Navigator.pop(context);
      },
    );
    showDialog(
      context: context,
      builder: (BuildContext context) {
        return AlertDialog(
            elevation: 2.0,
            shape:
                OutlineInputBorder(borderRadius: BorderRadius.circular(12.5)),
            title: Text(
              "Tem certeza que deseja deletar o pedido $bitIdOrder ?",
              textAlign: TextAlign.center,
            ),
            content: Text(
              "Depois de deletado não será possível recuperar as informações desse pedido !",
              textAlign: TextAlign.justify,
            ),
            actions: [botaoDeletar, botaoCancelar]);
      },
    );
  }
}
