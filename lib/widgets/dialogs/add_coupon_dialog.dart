import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:flutter_neumorphic/flutter_neumorphic.dart';

class AddCouponDialog extends StatefulWidget {
  @override
  _AddCouponDialogState createState() => _AddCouponDialogState();
}

class _AddCouponDialogState extends State<AddCouponDialog> {
  //Firestore _firestore = Firestore();
  final _formKey = GlobalKey<FormState>();

  final _idController = TextEditingController();
  final _percentController = TextEditingController();
  final _idFocus = FocusNode();
  final _percentFocus = FocusNode();

  bool isObscure = true;

  bool isLoading = false;
  bool result = false;
  bool isCreated = false;
  String message = "";
  String errorMessage = "";

  @override
  void initState() {
    super.initState();
  }

  @override
  void dispose() {
    super.dispose();
    _idController.dispose();
    _percentController.dispose();
    _idFocus.dispose();
    _percentFocus.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Dialog(
        backgroundColor: Colors.black.withOpacity(0.70),
        shape: OutlineInputBorder(
          borderSide: BorderSide(color: Colors.white10),
          borderRadius: BorderRadius.circular(15.0),
        ),
        child: Card(
          margin: EdgeInsets.zero,
          color: Colors.black.withOpacity(0.70),
          shape: OutlineInputBorder(
            borderSide: BorderSide(color: Colors.white10),
            borderRadius: BorderRadius.circular(15.0),
          ),
          child: Form(
            key: _formKey,
            child: SingleChildScrollView(
              padding: const EdgeInsets.all(8.0),
              child: Column(
                mainAxisSize: MainAxisSize.min,
                children: <Widget>[
                  //SizedBox(height: 8.0),
                  ListTile(
                    //  contentPadding: EdgeInsets.zero,
                    leading: Icon(Icons.card_giftcard,
                        size: 40, color: Colors.white),
                    title: Text("Adicionar novo Cupom",
                        textAlign: TextAlign.center,
                        style: TextStyle(
                            fontSize: 16.0,
                            fontWeight: FontWeight.bold,
                            color: Colors.white)),
                  ),

                  SizedBox(height: 10.0),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                    children: <Widget>[
                      SizedBox(
                        //height: 150,
                        width: MediaQuery.of(context).size.width * 0.40,
                        child: TextFormField(
                          focusNode: _idFocus,
                          autocorrect: true,
                          enableSuggestions: true,
                          controller: _idController,
                          style: TextStyle(
                              fontFamily: 'Lato', color: Colors.white),
                          decoration: InputDecoration(
                            hoverColor: Colors.white70,
                            /* contentPadding:
                                EdgeInsets.only(right: 5.0, left: 5.0),*/
                            labelText: "Código",
                            hintText: "Ex: 10%OFF",
                            hintStyle: TextStyle(
                                color: Colors.white54,
                                fontStyle: FontStyle.italic),
                            labelStyle: TextStyle(color: Colors.white70),
                            enabledBorder: OutlineInputBorder(
                                borderSide: BorderSide(
                                  color: Colors.white60,
                                ),
                                borderRadius: BorderRadius.circular(12.5)),
                            focusedBorder: OutlineInputBorder(
                                borderSide: BorderSide(
                                  color: Colors.green[900],
                                ),
                                borderRadius: BorderRadius.circular(12.5)),
                            focusedErrorBorder: OutlineInputBorder(
                                borderSide: BorderSide(
                                  color: Colors.red[900],
                                ),
                                borderRadius: BorderRadius.circular(12.5)),
                            errorBorder: OutlineInputBorder(
                                borderSide: BorderSide(
                                  color: Colors.red[900],
                                ),
                                borderRadius: BorderRadius.circular(12.5)),
                          ),
                          keyboardType: TextInputType.text,
                          onFieldSubmitted: (_) {
                            FocusScope.of(context).requestFocus(_percentFocus);
                          },
                          validator: (text) {
                            if (text.isEmpty) {
                              FocusScope.of(context).requestFocus(_idFocus);
                              return "Insira o código !";
                            } else
                              return null;
                          },
                        ),
                      ),
                      SizedBox(
                        width: 5,
                      ),
                      SizedBox(
                        width: MediaQuery.of(context).size.width * 0.30,
                        child: TextFormField(
                          controller: _percentController,
                          focusNode: _percentFocus,
                          keyboardType: TextInputType.number,
                          style: TextStyle(
                              fontFamily: 'Lato', color: Colors.white),
                          decoration: InputDecoration(
                            suffixText: "%",
                            suffixStyle: TextStyle(
                                fontFamily: 'Lato', color: Colors.white70),
                            /*contentPadding:
                                EdgeInsets.only(right: 5.0, left: 5.0),*/
                            labelText: "Desconto",
                            hintText: "Ex: 10",
                            hintStyle: TextStyle(
                                color: Colors.white54,
                                fontStyle: FontStyle.italic),
                            helperStyle:
                                TextStyle(height: 0.5, color: Colors.white70),
                            labelStyle: TextStyle(color: Colors.white70),
                            enabledBorder: OutlineInputBorder(
                                borderSide: BorderSide(
                                  color: Colors.white60,
                                ),
                                borderRadius: BorderRadius.circular(12.5)),
                            focusedBorder: OutlineInputBorder(
                                borderSide: BorderSide(
                                  color: Colors.green[900],
                                ),
                                borderRadius: BorderRadius.circular(12.5)),
                            focusedErrorBorder: OutlineInputBorder(
                                borderSide: BorderSide(
                                  color: Colors.red[900],
                                ),
                                borderRadius: BorderRadius.circular(12.5)),
                            errorBorder: OutlineInputBorder(
                                borderSide: BorderSide(
                                  color: Colors.red[900],
                                ),
                                borderRadius: BorderRadius.circular(12.5)),
                          ),
                          // obscureText: isObscure,
                          onFieldSubmitted: (_) {
                            if (_formKey.currentState.validate()) {
                              addCoupon(_idController.text,
                                  int.parse(_percentController.text));
                            }
                          },
                          validator: (text) {
                            if (text.isEmpty)
                              return "Insira o desconto !";
                            else
                              return null;
                          },
                        ),
                      ),
                    ],
                  ),
                  SizedBox(height: 10.0),
                  Visibility(
                    visible: result,
                    child: RichText(
                      text: TextSpan(
                          text: message + "\n",
                          style: TextStyle(
                            color:
                                isCreated ? Colors.green[900] : Colors.red[900],
                            fontWeight: FontWeight.bold,
                            fontSize: 16.0,
                          ),
                          children: <TextSpan>[
                            TextSpan(
                              text: errorMessage,
                              style: TextStyle(
                                  color: Colors.red[900],
                                  fontWeight: FontWeight.normal,
                                  fontSize: 16.0),
                            )
                          ]),
                    ),
                  ),
                  Row(
                    mainAxisSize: MainAxisSize.min,
                    mainAxisAlignment: MainAxisAlignment.end,
                    // mainAxisAlignment: MainAxisAlignment.end,
                    children: <Widget>[
                      FlatButton(
                          padding: EdgeInsets.zero,
                          child: Text(
                            "Voltar",
                            style: TextStyle(color: Colors.red[900]),
                          ),
                          onPressed: !isLoading
                              ? () {
                                  Navigator.of(context).pop();
                                }
                              : null),
                      FlatButton(
                        padding: EdgeInsets.zero,
                        child: Text(
                          "Adicionar",
                          style: TextStyle(color: Colors.green[900]),
                        ),
                        onPressed: !isLoading
                            ? () {
                                if (_formKey.currentState.validate()) {
                                  addCoupon(_idController.text,
                                      int.parse(_percentController.text));
                                }
                              }
                            : null,
                      ),
                    ],
                  ),
                  Visibility(
                    visible: isLoading,
                    child: LinearProgressIndicator(
                      backgroundColor: Colors.grey[850],
                      valueColor: AlwaysStoppedAnimation(Colors.green),
                    ),
                    /*child: NeumorphicProgress(
                      duration: Duration(minutes: 1),
                      style: ProgressStyle(
                        accent: Colors.greenAccent,
                        variant: Theme.of(context).primaryColor,
                      ),
                    ),*/
                  )
                ],
              ),
            ),
          ),
        ));
  }

  Future<void> addCoupon(String id, int percent) async {
    setState(() {
      isLoading = true;
    });
    try {
      await Firestore.instance
          .collection("coupons")
          .document(id)
          .setData(<String, dynamic>{"percent": percent});
      _onSuccess();
    } catch (e) {
      print(e.toString());
      _onFail(e.toString());
    }
  }

  void _onSuccess() {
    setState(() {
      isLoading = false;
      message = "Cupom criado com sucesso !";
      result = true;
      isCreated = true;
    });
    Future.delayed(Duration(seconds: 2)).then((_) {
      Navigator.of(context).pop();
    });
  }

  void _onFail(String errorMessage) {
    setState(() {
      isLoading = false;
      result = true;
      message = "Falha ao criar !";
      this.errorMessage = errorMessage;
      isCreated = false;
    });
    /* Future.delayed(Duration(seconds: 7)).then((_) {
      Navigator.of(context).pop();
    });*/
  }
}
