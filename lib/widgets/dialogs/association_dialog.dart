import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:flutter_datetime_picker/flutter_datetime_picker.dart';
import 'package:intl/intl.dart';

class AssociationDialog extends StatefulWidget {
  @override
  _AssociationDialogState createState() => _AssociationDialogState();
}

class _AssociationDialogState extends State<AssociationDialog> {
  bool isLoading = false;
  bool _changeDate = false;
  DateTime _newExpiresDate = DateTime.now();

  @override
  void initState() {
    super.initState();
  }

  static const associationState = <String>[
    "Aberto",
    "Encerrado",
  ];

  String _associationState = ""; // = "Aberto";

  final List<DropdownMenuItem<String>> _dropDownMenuItems = associationState
      .map((String value) => DropdownMenuItem(
            value: value,
            child: Text(
              value,
              textAlign: TextAlign.center,
              style: TextStyle(color: Colors.green[700]),
            ),
          ))
      .toList();

  @override
  Widget build(BuildContext context) {
    return Dialog(
        backgroundColor: Colors.black.withOpacity(0.70),
        shape: OutlineInputBorder(
          borderSide: BorderSide(color: Colors.white10),
          borderRadius: BorderRadius.circular(15.0),
        ),
        child: Card(
          margin: EdgeInsets.zero,
          color: Colors.black.withOpacity(0.70),
          shape: OutlineInputBorder(
            borderSide: BorderSide(color: Colors.white10),
            borderRadius: BorderRadius.circular(15.0),
          ),
          child: SingleChildScrollView(
            padding: const EdgeInsets.all(8.0),
            child: FutureBuilder<DocumentSnapshot>(
                future: Firestore.instance
                    .collection("association")
                    .document(DateTime.now().year.toString())
                    .get(),
                builder: (context, snapshot) {
                  if (!snapshot.hasData)
                    return Center(
                        child: CircularProgressIndicator(
                      valueColor: AlwaysStoppedAnimation(Colors.white),
                    ));
                  else {
                    // _associationState = snapshot.data["state"];
                    return Column(
                      mainAxisSize: MainAxisSize.min,
                      children: <Widget>[
                        Center(
                          child: ListTile(
                            leading: Icon(Icons.card_membership,
                                size: 35, color: Colors.white),
                            title: Text("Associações 20-21",
                                textAlign: TextAlign.center,
                                style: TextStyle(
                                    fontSize: 17.0,
                                    fontWeight: FontWeight.bold,
                                    color: Colors.white)),
                          ),
                        ),
                        SizedBox(height: 16.0),
                        ListTile(
                          dense: true,
                          contentPadding: EdgeInsets.zero,
                          title: Text("Período de Associação: ",
                              style: TextStyle(
                                  color: Colors.white, fontSize: 16.0)),
                          trailing: Card(
                            shape: OutlineInputBorder(
                                borderSide: BorderSide(color: Colors.white54),
                                borderRadius: BorderRadius.circular(12.5)),
                            margin: EdgeInsets.zero,
                            color: Colors.white10,
                            child: Padding(
                              padding:
                                  const EdgeInsets.only(right: 3.0, left: 3.0),
                              child: DropdownButton(
                                value: _associationState == ""
                                    ? snapshot.data["state"]
                                    : _associationState,
                                //style: TextStyle(color: Colors.green[700]),
                                items: _dropDownMenuItems,
                                onChanged: ((newValue) {
                                  setState(() {
                                    _associationState = newValue;
                                  });
                                }),
                              ),
                            ),
                          ),
                        ),
                        SizedBox(height: 16.0),
                        ListTile(
                            dense: true,
                            contentPadding: EdgeInsets.zero,
                            title: Text("Carteirinhas válidas até: ",
                                style: TextStyle(
                                    color: Colors.white, fontSize: 16.0)),
                            trailing: Text(
                              snapshot.data["expiresDate"],
                              /* DateFormat("dd-MM-yyyy").format(
                                  DateTime.parse(snapshot.data["expiresDate"])),*/
                              style: TextStyle(
                                  color: Colors.white,
                                  fontSize: 16.0,
                                  fontWeight: FontWeight.w500),
                            )),
                        SizedBox(height: 16.0),
                        FlatButton(
                            onPressed: () {
                              DatePicker.showDatePicker(context,
                                  showTitleActions: true,
                                  minTime: DateTime.now(),
                                  currentTime: DateTime.now(),
                                  onChanged: (date) {}, onConfirm: (date) {
                                setState(() {
                                  _changeDate = true;
                                  _newExpiresDate = date;
                                });
                              }, locale: LocaleType.pt);
                            },
                            child: Text(
                              "Alterar data de validade",
                              style: TextStyle(
                                  color: Colors.blue[800],
                                  fontSize: 16.0,
                                  fontWeight: FontWeight.w500),
                            )),
                        Visibility(
                          visible: _changeDate,
                          child: ListTile(
                              dense: true,
                              contentPadding: EdgeInsets.zero,
                              title: Text("Nova data de validade: ",
                                  style: TextStyle(
                                      color: Colors.white70, fontSize: 16.0)),
                              trailing: Text(
                                DateFormat("dd-MM-yyyy")
                                    .format(_newExpiresDate),
                                style: TextStyle(
                                    color: Colors.white70,
                                    fontSize: 16.0,
                                    fontWeight: FontWeight.w500),
                              )),
                        ),
                        Row(
                          mainAxisSize: MainAxisSize.min,
                          // mainAxisAlignment: MainAxisAlignment.end,
                          children: <Widget>[
                            FlatButton(
                                padding: EdgeInsets.zero,
                                child: Text("Cancelar",
                                    style: TextStyle(
                                        color: Colors.red[900],
                                        fontSize: 16.0)),
                                onPressed: !isLoading
                                    ? () {
                                        Navigator.of(context).pop();
                                      }
                                    : null),
                            FlatButton(
                              padding: EdgeInsets.zero,
                              child: Text(
                                "Salvar",
                                style: TextStyle(
                                    color: Colors.green[700], fontSize: 16.0),
                              ),
                              onPressed: !isLoading
                                  ? () async {
                                      await saveData(snapshot.data);
                                    }
                                  : null,
                            ),
                          ],
                        ),
                        Visibility(
                          visible: isLoading,
                          child: LinearProgressIndicator(
                            backgroundColor: Colors.grey[850],
                            valueColor: AlwaysStoppedAnimation(Colors.green),
                          ),
                        )
                      ],
                    );
                  }
                }),
          ),
        ));
  }

  Future<void> saveData(DocumentSnapshot doc) async {
    String dateToUpdate = "";
    setState(() {
      isLoading = true;
      dateToUpdate = DateFormat("dd-MM-yyy").format(_newExpiresDate) ==
              DateFormat("dd-MM-yyy").format(DateTime.now())
          ? doc.data["expiresDate"]
          : DateFormat("dd-MM-yyyy").format(_newExpiresDate);
      //isLoading = true;
      _associationState =
          _associationState == "" ? doc.data["state"] : _associationState;
    });
    print(dateToUpdate);
    try {
      await doc.reference.updateData({
        "state": _associationState,
        "expiresDate": dateToUpdate,
      });
    } catch (e) {}
    setState(() {
      isLoading = false;
    });
    Navigator.of(context).pop();
  }
}
