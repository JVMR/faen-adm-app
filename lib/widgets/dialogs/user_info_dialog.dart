import 'package:faen_app_admin/screens/photo_view_screen.dart';
import 'package:flutter/material.dart';
import 'package:transparent_image/transparent_image.dart';

class UserInfoDialog extends StatefulWidget {
  final Map<String, dynamic> userData;
  UserInfoDialog(this.userData);
  @override
  _UserInfoDialogState createState() => _UserInfoDialogState(userData);
}

class _UserInfoDialogState extends State<UserInfoDialog> {
  final Map<String, dynamic> userData;
  DateTime date = DateTime.now().toLocal();
  _UserInfoDialogState(this.userData);

  @override
  Widget build(BuildContext context) {
    return Dialog(
        backgroundColor: Colors.black.withOpacity(0.70),
        shape: OutlineInputBorder(
          borderSide: BorderSide(color: Colors.white10),
          borderRadius: BorderRadius.circular(15.0),
        ),
        child: Card(
            margin: EdgeInsets.zero,
            color: Colors.black.withOpacity(0.70),
            shape: OutlineInputBorder(
              borderSide: BorderSide(color: Colors.white10),
              borderRadius: BorderRadius.circular(15.0),
            ),
            child: SingleChildScrollView(
                padding: const EdgeInsets.all(8.0),
                child: Column(
                  mainAxisSize: MainAxisSize.min,
                  children: <Widget>[
                    GestureDetector(
                      onTap: () {
                        Navigator.of(context).push(MaterialPageRoute(
                            builder: (context) => PhotoViewScreen(
                                userData["photo"], userData["name"])));
                      },
                      child: SizedBox(
                        height: MediaQuery.of(context).size.height * 0.30,
                        width: MediaQuery.of(context).size.height * 0.30,
                        child: Card(
                          elevation: 8.0,
                          clipBehavior: Clip.antiAlias,
                          shape: RoundedRectangleBorder(
                            borderRadius:
                                BorderRadius.all(Radius.circular(15.0)),
                          ),
                          child: Hero(
                            tag: userData["photo"],
                            child: FadeInImage.memoryNetwork(
                              placeholder: kTransparentImage,
                              image: userData["photo"] ?? "",
                              fit: BoxFit.cover,
                            ),
                          ),
                        ),
                      ),
                    ),
                    SelectableText.rich(
                      TextSpan(
                        text: userData["name"] + "\n",
                        style: TextStyle(
                          fontSize: 16,
                          fontFamily: 'Lato',
                        ),
                        children: <TextSpan>[
                          TextSpan(
                              text: userData["typeUser"],
                              style: TextStyle(
                                fontSize: 16,
                                fontFamily: 'BebasNeue',
                              )),
                        ],
                      ),
                      textAlign: TextAlign.center,
                    ),
                    Align(
                      alignment: Alignment.bottomRight,
                      child: FlatButton(
                        onPressed: () {
                          Navigator.of(context).pop();
                        },
                        child: Text("Ok"),
                        textColor: Colors.green[900],
                        // color: Colors.greenAccent[700],
                      ),
                    ),
                  ],
                ))));
  }
}
