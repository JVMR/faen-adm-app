import 'package:faen_app_admin/models/notification_sender.dart';
import 'package:flutter/material.dart';

class SendNotificationDialog extends StatefulWidget {
  final String uid;
  final String userName;

  SendNotificationDialog(this.uid, this.userName);
  @override
  _SendNotificationDialogState createState() => _SendNotificationDialogState();
}

class _SendNotificationDialogState extends State<SendNotificationDialog> {
  final _formKey = GlobalKey<FormState>();
  NotificationSender _notificationSender = NotificationSender();

  final _titleController = TextEditingController();
  final _bodyController = TextEditingController();
  final _titleFocus = FocusNode();
  final _bodyFocus = FocusNode();

  String message = "";

  bool result = false;
  bool sent = false;
  bool isLoading = false;

  @override
  void dispose() {
    super.dispose();
    _titleController.dispose();
    _bodyController.dispose();
    _titleFocus.dispose();
    _bodyFocus.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Dialog(
        backgroundColor: Colors.black.withOpacity(0.60),
        shape: OutlineInputBorder(
          borderSide: BorderSide(color: Colors.white10),
          borderRadius: BorderRadius.circular(15.0),
        ),
        child: Card(
          margin: EdgeInsets.zero,
          color: Colors.black.withOpacity(0.70),
          shape: OutlineInputBorder(
            borderSide: BorderSide(color: Colors.white10),
            borderRadius: BorderRadius.circular(15.0),
          ),
          child: Form(
            key: _formKey,
            child: SingleChildScrollView(
              padding: const EdgeInsets.all(8.0),
              child: Column(
                mainAxisSize: MainAxisSize.min,
                children: <Widget>[
                  /*Text("Confirme suas credenciais de administrador !",
                      textAlign: TextAlign.center,
                      style: TextStyle(
                          fontSize: 16.0,
                          fontWeight: FontWeight.bold,
                          color: Colors.white70)),*/
                  ListTile(
                    //  contentPadding: EdgeInsets.zero,
                    leading: Icon(Icons.send, size: 35, color: Colors.white70),
                    title: Text("Enviar notificação para ${widget.userName}",
                        textAlign: TextAlign.center,
                        style: TextStyle(
                            fontSize: 16.0,
                            fontWeight: FontWeight.bold,
                            color: Colors.white70)),
                  ),
                  SizedBox(height: 10.0),
                  TextFormField(
                    focusNode: _titleFocus,
                    autocorrect: true,
                    enableSuggestions: true,
                    controller: _titleController,
                    style: TextStyle(fontFamily: 'Lato', color: Colors.white70),
                    decoration: InputDecoration(
                      hoverColor: Colors.white70,
                      labelText: "Título",
                      labelStyle: TextStyle(color: Colors.white70),
                      enabledBorder: OutlineInputBorder(
                          borderSide: BorderSide(
                            color: Colors.white60,
                          ),
                          borderRadius: BorderRadius.circular(12.5)),
                      focusedBorder: OutlineInputBorder(
                          borderSide: BorderSide(
                            color: Colors.green[900],
                          ),
                          borderRadius: BorderRadius.circular(12.5)),
                      focusedErrorBorder: OutlineInputBorder(
                          borderSide: BorderSide(
                            color: Colors.red[900],
                          ),
                          borderRadius: BorderRadius.circular(12.5)),
                      errorBorder: OutlineInputBorder(
                          borderSide: BorderSide(
                            color: Colors.red[900],
                          ),
                          borderRadius: BorderRadius.circular(12.5)),
                    ),
                    keyboardType: TextInputType.emailAddress,
                    onFieldSubmitted: (_) {
                      FocusScope.of(context).requestFocus(_bodyFocus);
                    },
                    validator: (text) {
                      if (text.isEmpty) {
                        FocusScope.of(context).requestFocus(_titleFocus);
                        return "Título não pode ser vazio !";
                      } else
                        return null;
                    },
                  ),
                  SizedBox(height: 16.0),
                  TextFormField(
                    controller: _bodyController,
                    maxLines: 3,
                    style: TextStyle(fontFamily: 'Lato', color: Colors.white70),
                    decoration: InputDecoration(
                      labelText: "Corpo da Notificação",
                      labelStyle: TextStyle(color: Colors.white70),
                      enabledBorder: OutlineInputBorder(
                          borderSide: BorderSide(
                            color: Colors.white60,
                          ),
                          borderRadius: BorderRadius.circular(12.5)),
                      focusedBorder: OutlineInputBorder(
                          borderSide: BorderSide(
                            color: Colors.green[900],
                          ),
                          borderRadius: BorderRadius.circular(12.5)),
                      focusedErrorBorder: OutlineInputBorder(
                          borderSide: BorderSide(
                            color: Colors.red[900],
                          ),
                          borderRadius: BorderRadius.circular(12.5)),
                      errorBorder: OutlineInputBorder(
                          borderSide: BorderSide(
                            color: Colors.red[900],
                          ),
                          borderRadius: BorderRadius.circular(12.5)),
                    ),
                    //obscureText: isObscure,
                    onFieldSubmitted: (_) async {
                      if (_formKey.currentState.validate()) {
                        await sendNotification(
                            _titleController.text, _bodyController.text);
                      }
                    },
                    validator: (text) {
                      if (text.isEmpty)
                        return "Corpo não pode ser vazio !";
                      else
                        return null;
                    },
                  ),
                  SizedBox(height: 16.0),
                  Visibility(
                    visible: result,
                    child: RichText(
                      text: TextSpan(
                        text: message + "\n",
                        style: TextStyle(
                          color: sent ? Colors.green[900] : Colors.red[900],
                          fontWeight: FontWeight.bold,
                          fontSize: 16.0,
                        ),
                        /*  children: <TextSpan>[
                            TextSpan(
                              text: errorMessage,
                              style: TextStyle(
                                  color: Colors.red[900],
                                  fontWeight: FontWeight.normal,
                                  fontSize: 16.0),
                            )
                          ]*/
                      ),
                    ),
                  ),
                  Row(
                    mainAxisSize: MainAxisSize.min,
                    // mainAxisAlignment: MainAxisAlignment.end,
                    children: <Widget>[
                      FlatButton(
                          padding: EdgeInsets.zero,
                          child: Text(
                            "Cancelar",
                            style: TextStyle(color: Colors.red[900]),
                          ),
                          onPressed: !isLoading
                              ? () {
                                  Navigator.of(context).pop();
                                }
                              : null),
                      FlatButton(
                        padding: EdgeInsets.zero,
                        child: Text(
                          "Enviar",
                          style: TextStyle(color: Colors.green[900]),
                        ),
                        onPressed: !isLoading
                            ? () async {
                                if (_formKey.currentState.validate()) {
                                  await sendNotification(_titleController.text,
                                      _bodyController.text);
                                }
                              }
                            : null,
                      ),
                      /*Visibility(
                        visible: result,
                        child: Text("Message",
                            style: TextStyle(
                                color:
                                    sent ? Colors.green[900] : Colors.red[900]),
                            textAlign: TextAlign.center,
                            softWrap: true),
                      ),*/
                    ],
                  ),
                  Visibility(
                    visible: isLoading,
                    child: LinearProgressIndicator(
                      backgroundColor: Colors.grey[850],
                      valueColor: AlwaysStoppedAnimation(Colors.green),
                    ),
                  )
                ],
              ),
            ),
          ),
        ));
  }

  Future<void> sendNotification(title, body) async {
    setState(() {
      isLoading = true;
    });
    bool success = await _notificationSender.sendNotificationToUser(
        title: title, body: body, uid: widget.uid);

    setState(() {
      sent = success;
      if (success) {
        isLoading = false;
        message = "Notificação enviada !";
        result = true;
      } else {
        isLoading = false;
        result = true;
        message = "Falha ao enviar a notificação !";
        // this.errorMessage = errorMessage;
      }
    });
    Future.delayed(Duration(seconds: 4)).then((_) {
      Navigator.of(context).pop();
    });
  }
}
