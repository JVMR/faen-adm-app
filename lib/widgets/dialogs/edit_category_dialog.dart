import 'dart:io';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:faen_app_admin/blocs/category_bloc.dart';
import 'package:flutter/material.dart';

import '../image_source_sheet.dart';

class EditCategoryDialog extends StatefulWidget {
  final DocumentSnapshot category;

  EditCategoryDialog({this.category});

  @override
  _EditCategoryDialogState createState() =>
      _EditCategoryDialogState(category: category);
}

class _EditCategoryDialogState extends State<EditCategoryDialog> {
  final CategoryBloc _categoryBloc;

  final TextEditingController _controller;

  _EditCategoryDialogState({DocumentSnapshot category})
      : _categoryBloc = CategoryBloc(category),
        _controller = TextEditingController(
            text: category != null ? category.data["title"] : "");

  @override
  Widget build(BuildContext context) {
    return Dialog(
      backgroundColor: Colors.black.withOpacity(0.70),
      shape: OutlineInputBorder(
        borderSide: BorderSide(color: Colors.white10),
        borderRadius: BorderRadius.circular(15.0),
      ),
      child: Card(
        margin: EdgeInsets.zero,
        color: Colors.black.withOpacity(0.70),
        shape: OutlineInputBorder(
          borderSide: BorderSide(color: Colors.white10),
          borderRadius: BorderRadius.circular(15.0),
        ),
        child: Column(
          mainAxisSize: MainAxisSize.min,
          children: <Widget>[
            Padding(
              padding: const EdgeInsets.only(top: 8.0, bottom: 3.0),
              child: Text(
                widget.category != null
                    ? "Editar Categoria"
                    : "Adicionar Categoria",
                textAlign: TextAlign.center,
                style: TextStyle(fontSize: 16.0, fontWeight: FontWeight.bold),
              ),
            ),
            ListTile(
              contentPadding: EdgeInsets.symmetric(horizontal: 10),
              leading: GestureDetector(
                onTap: () {
                  showModalBottomSheet(
                      context: context,
                      builder: (context) => ImageSourceSheet(
                            onImageSelected: (image) {
                              Navigator.of(context).pop();
                              _categoryBloc.setImage(image);
                            },
                          ));
                },
                child: StreamBuilder(
                    stream: _categoryBloc.outImage,
                    builder: (context, snapshot) {
                      if (snapshot.data != null)
                        return CircleAvatar(
                          child: snapshot.data is File
                              ? Image.file(
                                  snapshot.data,
                                  fit: BoxFit.cover,
                                )
                              : Image.network(
                                  snapshot.data,
                                  fit: BoxFit.cover,
                                ),
                          backgroundColor: Colors.transparent,
                        );
                      else
                        return Icon(
                          Icons.image,
                          color: Colors.white,
                          size: 48.0,
                        );
                    }),
              ),
              title: StreamBuilder<String>(
                  stream: _categoryBloc.outTitle,
                  builder: (context, snapshot) {
                    return TextField(
                      controller: _controller,
                      onChanged: _categoryBloc.setTitle,
                      style: TextStyle(
                        color: Colors.white,
                        fontFamily: 'Lato',
                      ),
                      decoration: InputDecoration(
                        errorText: snapshot.hasError ? snapshot.error : null,
                        enabledBorder: OutlineInputBorder(
                            borderSide: BorderSide(
                              color: Colors.white54,
                            ),
                            borderRadius: BorderRadius.circular(15.0)),
                        focusedBorder: OutlineInputBorder(
                            borderSide: BorderSide(
                              color: Colors.green[900],
                            ),
                            borderRadius: BorderRadius.circular(15.0)),
                        focusedErrorBorder: OutlineInputBorder(
                            borderSide: BorderSide(
                              color: Colors.red[900],
                            ),
                            borderRadius: BorderRadius.circular(15.0)),
                        errorBorder: OutlineInputBorder(
                            borderSide: BorderSide(
                              color: Colors.red[900],
                            ),
                            borderRadius: BorderRadius.circular(15.0)),
                      ),
                    );
                  }),
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: <Widget>[
                StreamBuilder<bool>(
                    stream: _categoryBloc.outDelete,
                    builder: (context, snapshot) {
                      if (!snapshot.hasData) return Container();
                      return FlatButton(
                        child: Text("Excluir"),
                        textColor: Colors.red,
                        disabledTextColor: Colors.white54,
                        onPressed: snapshot.data
                            ? () {
                                _categoryBloc.delete();
                                Navigator.of(context).pop();
                              }
                            : null,
                      );
                    }),
                StreamBuilder<bool>(
                    stream: _categoryBloc.submitValid,
                    builder: (context, snapshot) {
                      return FlatButton(
                        child: Text("Salvar"),
                        textColor: Colors.green[900],
                        disabledTextColor: Colors.white54,
                        onPressed: snapshot.hasData
                            ? () async {
                                await _categoryBloc.saveData();
                                Navigator.of(context).pop();
                              }
                            : null,
                      );
                    }),
              ],
            ),
            StreamBuilder<bool>(
              stream: _categoryBloc.outLoading,
              initialData: false,
              builder: (context, snapshot) {
                if (!snapshot.hasData) return Container();
                if (snapshot.data)
                  return LinearProgressIndicator(
                    backgroundColor: Colors.grey[850],
                    valueColor: AlwaysStoppedAnimation(Colors.green),
                  );
                else
                  return Container();
                /*return Opacity(
                  opacity: snapshot.data ? 1.0 : 0.0,
                  child: LinearProgressIndicator(
                    backgroundColor: Colors.green,
                  ),
                );*/
              },
            )
          ],
        ),
      ),
    );
  }
}
