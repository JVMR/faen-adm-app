import 'package:faen_app_admin/models/add_user_class.dart';
import 'package:faen_app_admin/screens/home_screen.dart';
import 'package:flutter/material.dart';

class ConfirmAdmDialog extends StatefulWidget {
  final Map<String, dynamic> userData;
  final String userPass;

  ConfirmAdmDialog(this.userData, this.userPass);
  @override
  _ConfirmAdmDialogState createState() =>
      _ConfirmAdmDialogState(userData, userPass);
}

class _ConfirmAdmDialogState extends State<ConfirmAdmDialog> {
  final Map<String, dynamic> userData;
  final String userPass;

  _ConfirmAdmDialogState(this.userData, this.userPass);

  AddUserClass _addUserClass = AddUserClass();
  final _formKey = GlobalKey<FormState>();

  final _emailController = TextEditingController();
  final _passController = TextEditingController();
  final _emailFocus = FocusNode();
  final _passFocus = FocusNode();

  bool isObscure = true;

  bool isLoading = false;
  bool result = false;
  bool isCreated = false;
  String message = "";
  String errorMessage = "";

  @override
  void initState() {
    super.initState();
  }

  @override
  void dispose() {
    super.dispose();
    _emailController.dispose();
    _passController.dispose();
    _emailFocus.dispose();
    _passFocus.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Dialog(
        backgroundColor: Colors.black.withOpacity(0.70),
        shape: OutlineInputBorder(
          borderSide: BorderSide(color: Colors.white10),
          borderRadius: BorderRadius.circular(15.0),
        ),
        child: Card(
          margin: EdgeInsets.zero,
          color: Colors.black.withOpacity(0.70),
          shape: OutlineInputBorder(
            borderSide: BorderSide(color: Colors.white10),
            borderRadius: BorderRadius.circular(15.0),
          ),
          child: Form(
            key: _formKey,
            child: SingleChildScrollView(
              padding: const EdgeInsets.all(8.0),
              child: Column(
                mainAxisSize: MainAxisSize.min,
                children: <Widget>[
                  Text("Confirme suas credenciais de administrador !",
                      textAlign: TextAlign.center,
                      style: TextStyle(
                          fontSize: 16.0,
                          fontWeight: FontWeight.bold,
                          color: Colors.white70)),
                  SizedBox(height: 16.0),
                  TextFormField(
                    focusNode: _emailFocus,
                    autocorrect: true,
                    enableSuggestions: true,
                    controller: _emailController,
                    style: TextStyle(color: Colors.white70),
                    decoration: InputDecoration(
                      hoverColor: Colors.white70,
                      labelText: "Email",
                      labelStyle: TextStyle(color: Colors.white70),
                      enabledBorder: OutlineInputBorder(
                          borderSide: BorderSide(
                            color: Colors.white60,
                          ),
                          borderRadius: BorderRadius.circular(12.5)),
                      focusedBorder: OutlineInputBorder(
                          borderSide: BorderSide(
                            color: Theme.of(context).primaryColor,
                          ),
                          borderRadius: BorderRadius.circular(12.5)),
                      focusedErrorBorder: OutlineInputBorder(
                          borderSide: BorderSide(
                            color: Colors.red[900],
                          ),
                          borderRadius: BorderRadius.circular(12.5)),
                      errorBorder: OutlineInputBorder(
                          borderSide: BorderSide(
                            color: Colors.red[900],
                          ),
                          borderRadius: BorderRadius.circular(12.5)),
                    ),
                    keyboardType: TextInputType.emailAddress,
                    onFieldSubmitted: (_) {
                      FocusScope.of(context).requestFocus(_passFocus);
                    },
                    validator: (text) {
                      if (text.isEmpty || !text.contains("@")) {
                        FocusScope.of(context).requestFocus(_emailFocus);
                        return "Email inválido !";
                      } else
                        return null;
                    },
                  ),
                  SizedBox(height: 16.0),
                  TextFormField(
                    controller: _passController,
                    style: TextStyle(color: Colors.white70),
                    decoration: InputDecoration(
                      suffixIcon: IconButton(
                        icon: isObscure
                            ? Icon(Icons.visibility, color: Colors.white54)
                            : Icon(Icons.visibility_off, color: Colors.white54),
                        onPressed: () {
                          setState(() {
                            isObscure = !isObscure;
                          });
                        },
                      ),
                      labelText: "Senha",
                      labelStyle: TextStyle(color: Colors.white70),
                      enabledBorder: OutlineInputBorder(
                          borderSide: BorderSide(
                            color: Colors.white60,
                          ),
                          borderRadius: BorderRadius.circular(12.5)),
                      focusedBorder: OutlineInputBorder(
                          borderSide: BorderSide(
                            color: Theme.of(context).primaryColor,
                          ),
                          borderRadius: BorderRadius.circular(12.5)),
                      focusedErrorBorder: OutlineInputBorder(
                          borderSide: BorderSide(
                            color: Colors.red[900],
                          ),
                          borderRadius: BorderRadius.circular(12.5)),
                      errorBorder: OutlineInputBorder(
                          borderSide: BorderSide(
                            color: Colors.red[900],
                          ),
                          borderRadius: BorderRadius.circular(12.5)),
                    ),
                    obscureText: isObscure,
                    onFieldSubmitted: (_) {
                      if (_formKey.currentState.validate()) {
                        toCreateUser();
                      }
                    },
                    validator: (text) {
                      if (text.isEmpty || text.length < 6)
                        return "Senha inválida !";
                      else
                        return null;
                    },
                  ),
                  SizedBox(height: 16.0),
                  Visibility(
                    visible: result,
                    child: RichText(
                      text: TextSpan(
                          text: message + "\n",
                          style: TextStyle(
                            color:
                                isCreated ? Colors.green[900] : Colors.red[900],
                            fontWeight: FontWeight.bold,
                            fontSize: 16.0,
                          ),
                          children: <TextSpan>[
                            TextSpan(
                              text: errorMessage,
                              style: TextStyle(
                                  color: Colors.red[900],
                                  fontWeight: FontWeight.normal,
                                  fontSize: 16.0),
                            )
                          ]),
                    ),
                  ),
                  Row(
                    mainAxisSize: MainAxisSize.min,
                    // mainAxisAlignment: MainAxisAlignment.end,
                    children: <Widget>[
                      FlatButton(
                          padding: EdgeInsets.zero,
                          child: Text(
                            "Cancelar",
                            style: TextStyle(color: Colors.red[900]),
                          ),
                          onPressed: !isLoading
                              ? () {
                                  Navigator.of(context).pop();
                                }
                              : null),
                      FlatButton(
                        padding: EdgeInsets.zero,
                        child: Text(
                          "Confirmar",
                          style: TextStyle(color: Colors.green[900]),
                        ),
                        onPressed: !isLoading
                            ? () {
                                if (_formKey.currentState.validate()) {
                                  toCreateUser();
                                }
                              }
                            : null,
                      ),

                      /*Visibility(
                        visible: result,
                        child: Text("Message",
                            style: TextStyle(
                                color: isCreated
                                    ? Colors.green[900]
                                    : Colors.red[900]),
                            textAlign: TextAlign.center,
                            softWrap: true),
                      ),*/
                    ],
                  ),
                  Visibility(
                    visible: isLoading,
                    child: LinearProgressIndicator(
                      backgroundColor: Colors.grey[850],
                      valueColor: AlwaysStoppedAnimation(Colors.green),
                    ),
                  )
                ],
              ),
            ),
          ),
        ));
  }

  void toCreateUser() {
    setState(() {
      isLoading = true;
    });
    _addUserClass.addUser(
        admData: {
          "email": _emailController.text,
          "password": _passController.text
        },
        userData: userData,
        pass: userPass,
        cancel: onCancel,
        onSuccess: _onSuccess,
        onFail: _onFail);
  }

  void onCancel() {}

  void _onSuccess() {
    setState(() {
      isLoading = false;
      message = "Usuário criado com sucesso !";
      result = true;
      isCreated = true;
    });
    Future.delayed(Duration(seconds: 4)).then((_) {
      Navigator.of(context).pop();
      Navigator.of(context).pushReplacement(
          MaterialPageRoute(builder: (context) => HomeScreen()));
    });
  }

  void _onFail(String errorMessage) {
    setState(() {
      isLoading = false;
      result = true;
      message = "Falha ao criar o usuário !";
      this.errorMessage = errorMessage;
      isCreated = false;
    });
    /* Future.delayed(Duration(seconds: 7)).then((_) {
      Navigator.of(context).pop();
    });*/
  }
}
