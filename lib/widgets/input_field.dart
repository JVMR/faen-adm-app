import 'package:flutter/material.dart';

class InputField extends StatelessWidget {
  final IconData icon;
  final String label;
  final bool obscure;
  final Stream<String> stream;
  final Function(String) onChanged;

  InputField(
      {this.icon, this.label, this.obscure, this.stream, this.onChanged});

  @override
  Widget build(BuildContext context) {
    return StreamBuilder<String>(
        stream: stream,
        builder: (context, snapshot) {
          return TextField(
            onChanged: onChanged,
            decoration: InputDecoration(
              prefixIcon: Icon(
                icon,
                color: Colors.white70,
              ),
              labelText: label,
              labelStyle: TextStyle(color: Colors.white70),
              enabledBorder: OutlineInputBorder(
                  borderSide: BorderSide(
                    color: Colors.white70,
                  ),
                  borderRadius: BorderRadius.circular(15.0)),
              focusedBorder: OutlineInputBorder(
                  borderSide: BorderSide(
                    color: Colors.green[900],
                  ),
                  borderRadius: BorderRadius.circular(15.0)),
              focusedErrorBorder: OutlineInputBorder(
                  borderSide: BorderSide(
                    color: Colors.red[900],
                  ),
                  borderRadius: BorderRadius.circular(15.0)),
              errorBorder: OutlineInputBorder(
                  borderSide: BorderSide(
                    color: Colors.red[900],
                  ),
                  borderRadius: BorderRadius.circular(15.0)),
              errorText: snapshot.hasError ? snapshot.error : null,
            ),
            style: TextStyle(fontFamily: 'Lato', color: Colors.white),
            obscureText: obscure,
          );
        });
  }
}
