import 'package:flutter/material.dart';

import 'image_source_sheet.dart';

class ImagesWidget extends FormField<List> {
  ImagesWidget({
    BuildContext context,
    FormFieldSetter<List> onSaved,
    FormFieldValidator<List> validator,
    List initialValue,
    bool autoValidate = false,
  }) : super(
            onSaved: onSaved,
            validator: validator,
            initialValue: initialValue,
            autovalidate: autoValidate,
            builder: (state) {
              double size = MediaQuery.of(context).size.width * 0.30;
              return Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  Container(
                    //  height: 124,
                    height: MediaQuery.of(context).size.width * 0.35,
                    padding: EdgeInsets.only(top: 16, bottom: 8),
                    child: ListView(
                      scrollDirection: Axis.horizontal,
                      children: state.value.map<Widget>((i) {
                        return SizedBox(
                          height: size,
                          width: size,
                          child: Card(
                            elevation: 8.0,
                            clipBehavior: Clip.antiAlias,
                            shape: RoundedRectangleBorder(
                              borderRadius:
                                  BorderRadius.all(Radius.circular(15.0)),
                            ),
                            margin: EdgeInsets.only(right: 8),
                            child: GestureDetector(
                              child: i is String
                                  ? Image.network(
                                      i,
                                      fit: BoxFit.cover,
                                    )
                                  : Image.file(
                                      i,
                                      fit: BoxFit.cover,
                                    ),
                              onLongPress: () {
                                state.didChange(state.value..remove(i));
                              },
                            ),
                          ),
                        );
                      }).toList()
                        ..add(GestureDetector(
                          child: SizedBox(
                            height: size,
                            width: size,
                            child: Card(
                              elevation: 8.0,
                              clipBehavior: Clip.antiAlias,
                              shape: RoundedRectangleBorder(
                                borderRadius:
                                    BorderRadius.all(Radius.circular(15.0)),
                              ),
                              margin: EdgeInsets.only(right: 8),
                              color: Colors.black38,
                              child: Icon(
                                Icons.camera_enhance,
                                color: Colors.white,
                              ),
                              // color: Colors.white.withAlpha(50),
                            ),
                          ),
                          onTap: () {
                            showModalBottomSheet(
                                context: context,
                                builder: (context) => ImageSourceSheet(
                                      onImageSelected: (image) {
                                        state
                                            .didChange(state.value..add(image));
                                        Navigator.of(context).pop();
                                      },
                                    ));
                          },
                        )),
                    ),
                  ),
                  state.hasError
                      ? Text(
                          state.errorText,
                          style: TextStyle(color: Colors.red, fontSize: 12),
                        )
                      : Container()
                ],
              );
            });
}
