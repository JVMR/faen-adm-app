import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:faen_app_admin/widgets/tiles/category_tile.dart';
import 'package:flutter/material.dart';

class ProductsTab extends StatefulWidget {
  @override
  _ProductsTabState createState() => _ProductsTabState();
}

class _ProductsTabState extends State<ProductsTab>
    with AutomaticKeepAliveClientMixin {
  @override
  Widget build(BuildContext context) {
    super.build(context);

    return RefreshIndicator(
      backgroundColor: Colors.white.withOpacity(0.9),
      color: Colors.greenAccent[700],
      onRefresh: () async {
        await Future.delayed(Duration(seconds: 1), () {
          setState(() {});
        });
      },
      child: StreamBuilder<QuerySnapshot>(
        stream: Firestore.instance.collection("products").snapshots(),
        builder: (context, snapshot) {
          if (!snapshot.hasData)
            return Center(
              child: CircularProgressIndicator(
                valueColor: AlwaysStoppedAnimation(Colors.white),
              ),
            );
          return ListView.builder(
            itemCount: snapshot.data.documents.length,
            itemBuilder: (context, index) {
              return CategoryTile(snapshot.data.documents[index]);
            },
          );
        },
      ),
    );
  }

  @override
  bool get wantKeepAlive => true;
}
