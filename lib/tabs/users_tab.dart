import 'package:bloc_pattern/bloc_pattern.dart';
import 'package:faen_app_admin/blocs/user_bloc.dart';
import 'package:faen_app_admin/widgets/tiles/user_tile.dart';
import 'package:flutter/material.dart';
import 'package:flutter_staggered_animations/flutter_staggered_animations.dart';

class UsersTab extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final _userBloc = BlocProvider.getBloc<UserBloc>();

    return Column(
      children: <Widget>[
        Card(
          /* shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.all(Radius.circular(15.0)),
          ),*/
          margin: EdgeInsets.all(10.0),
          child: Padding(
            padding: const EdgeInsets.fromLTRB(10.0, 5.0, 5.0, 5.0),
            child: TextField(
              style: TextStyle(fontFamily: 'Lato', color: Colors.white),
              decoration: InputDecoration(
                  hintText: "Pesquisar",
                  hintStyle:
                      TextStyle(fontFamily: 'Lato', color: Colors.white70),
                  icon: Icon(
                    Icons.search,
                    color: Colors.white,
                  ),
                  border: InputBorder.none),
              onChanged: _userBloc.onChangedSearch,
            ),
          ),
        ),
        Container(
          color: Colors.black87,
          child: Divider(
            color: Colors.white70,
            height: 1.0,
          ),
        ),
        Expanded(
          child: StreamBuilder<List>(
              stream: _userBloc.outUsers,
              builder: (context, snapshot) {
                if (!snapshot.hasData)
                  return Center(
                    child: CircularProgressIndicator(
                      valueColor: AlwaysStoppedAnimation(Colors.white),
                    ),
                  );
                else if (snapshot.data.length == 0)
                  return Center(
                    child: Text(
                      "Nenhum usuário encontrado!",
                      style: TextStyle(fontSize: 16.0, color: Colors.white),
                    ),
                  );
                else
                  return AnimationLimiter(
                    child: ListView.separated(
                        //padding: EdgeInsets.zero,

                        itemBuilder: (context, index) {
                          return AnimationConfiguration.staggeredList(
                              position: index,
                              duration: const Duration(milliseconds: 375),
                              child: SlideAnimation(
                                  verticalOffset: 80.0,
                                  child: FadeInAnimation(
                                      child: UserTile(snapshot.data[index],
                                          _userBloc.admUserData["typeUser"]))));
                        },
                        separatorBuilder: (context, index) {
                          return AnimationConfiguration.staggeredList(
                              position: index,
                              duration: const Duration(milliseconds: 275),
                              delay: const Duration(milliseconds: 10),
                              child: FadeInAnimation(
                                  child: Container(
                                color: Colors.black87,
                                child: Divider(color: Colors.white12),
                              )));
                        },
                        itemCount: snapshot.data.length),
                  );
              }),
        )
      ],
    );
  }
}
