import 'package:bloc_pattern/bloc_pattern.dart';
import 'package:faen_app_admin/blocs/orders_bloc.dart';
import 'package:faen_app_admin/widgets/tiles/order_tile.dart';
import 'package:flutter/material.dart';

class OrdersTab extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final _ordersBloc = BlocProvider.getBloc<OrdersBloc>();

    return StreamBuilder<List>(
        stream: _ordersBloc.outOrders,
        builder: (context, snapshot) {
          if (!snapshot.hasData)
            return Center(
              child: CircularProgressIndicator(
                valueColor: AlwaysStoppedAnimation(Colors.green),
              ),
            );
          else if (snapshot.data.length == 0)
            return Center(
              child: Text(
                "Nenhum pedido encontrado!",
                style: TextStyle(color: Colors.white),
              ),
            );

          return ListView.builder(
              // padding: EdgeInsets.fromLTRB(0.0, 0.0, 0.0, 0.0),
              itemCount: snapshot.data.length,
              itemBuilder: (context, index) {
                return OrderTile(snapshot.data[index]);
              });
        });
  }
}
/*final states = [
    "",
    "Aguardando pagamento",
    "Pagamento Confirmado",
    "Aguardando Entrega",
    "Entregue"
  ];*/
