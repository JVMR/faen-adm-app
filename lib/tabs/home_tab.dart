import 'package:bloc_pattern/bloc_pattern.dart';
import 'package:faen_app_admin/blocs/user_bloc.dart';
import 'package:faen_app_admin/screens/coupons_screen.dart';
import 'package:faen_app_admin/screens/feed_screen.dart';
import 'package:faen_app_admin/screens/revenue_control_screen.dart';
import 'package:faen_app_admin/screens/send_notification_screen.dart';
import 'package:faen_app_admin/widgets/dialogs/association_dialog.dart';
import 'package:faen_app_admin/widgets/tiles/adm_header_tile.dart';
import 'package:flutter/material.dart';
import 'package:flutter_icons/flutter_icons.dart';
import 'package:flutter_neumorphic/flutter_neumorphic.dart';

class HomeTab extends StatefulWidget {
  final PageController controller;
  HomeTab(this.controller);
  @override
  _HomeTabState createState() => _HomeTabState();
}

class _HomeTabState extends State<HomeTab> {
  bool changePage = false;
  final _userBloc = BlocProvider.getBloc<UserBloc>();
  // int page = 0;
  List<Widget> _pages = [
    FeedScreen(),
    SendNotificationScreen(),
    RevenueControlScreen(),
    CouponsScreen()
  ];
  @override
  Widget build(BuildContext context) {
    return ListView(
      /*crossAxisAlignment: CrossAxisAlignment.stretch,
      mainAxisSize: MainAxisSize.min,*/
      padding: EdgeInsets.all(8.0),
      children: <Widget>[
        AdmHeaderTile(),
        GridView.count(
          physics: NeverScrollableScrollPhysics(),
          shrinkWrap: true,

          crossAxisCount: 2,
          crossAxisSpacing: 8.0,
          mainAxisSpacing: 8.0,
          // childAspectRatio: 0.89,
          scrollDirection: Axis.vertical,
          children: _getAdmNeuTiles(),
        ),
      ],
    );
  }

  List<Widget> _getAdmNeuTiles() {
    switch (_userBloc.admUserData["typeUser"]) {
      case "Administrador":
        return [
          admNeuTile(
            iconPer: ImageIcon(
              AssetImage("images/logo.png"),
              size: 80,
              color: Colors.green[800],
            ),
            title: "Feed de notícias",
            page: 0,
            ative: true,
          ),
          admNeuTile(
            icon: Icons.send,
            title: "Notificações",
            page: 1,
            ative: true,
          ),
          admNeuTile(
            icon: Icons.data_usage,
            title: "Controle de Receita",
            page: 2,
            ative: true,
          ),
          admNeuTile(
            icon: Icons.card_giftcard,
            title: "Cupons",
            page: 3,
            ative: true,
          ),
          admNeuTile(
            icon: Icons.card_membership,
            title: "Associações",
            page: 4,
            ative: true,
          ),
          admNeuTile(
            icon: Icons.person,
            title: "Usuários",
            page: 5,
            ative: true,
          ),
          admNeuTile(
            icon: Icons.shopping_cart,
            title: "Pedidos",
            page: 6,
            ative: true,
          ),
          admNeuTile(
            icon: Icons.list,
            title: "Produtos",
            page: 7,
            ative: true,
          ),
          admNeuTile(
            icon: FontAwesome5Solid.handshake,
            title: "Parceria",
            page: 8,
            ative: true,
          ),
        ];
        break;
      case "Moderador":
        return [
          admNeuTile(
            iconPer: ImageIcon(
              AssetImage("images/logo.png"),
              size: 80,
              color: Colors.green[800].withOpacity(0.70),
            ),
            title: "Feed de notícias",
            page: 0,
            ative: false,
          ),
          admNeuTile(
            icon: Icons.send,
            title: "Notificações",
            page: 1,
            ative: false,
          ),
          admNeuTile(
            icon: Icons.data_usage,
            title: "Controle de Receita",
            page: 2,
            ative: false,
          ),
          admNeuTile(
            icon: Icons.card_giftcard,
            title: "Cupons",
            page: 3,
            ative: false,
          ),
          admNeuTile(
            icon: Icons.card_membership,
            title: "Associações",
            page: 4,
            ative: false,
          ),
          admNeuTile(
            icon: Icons.person,
            title: "Usuários",
            page: 5,
            ative: true,
          ),
          admNeuTile(
            icon: Icons.shopping_cart,
            title: "Pedidos",
            page: 6,
            ative: false,
          ),
          admNeuTile(
            icon: Icons.list,
            title: "Produtos",
            page: 7,
            ative: true,
          ),
          admNeuTile(
            icon: FontAwesome5Solid.handshake,
            title: "Parceria",
            page: 8,
            ative: false,
          ),
        ];
        break;
      case "Parceiro":
        return [
          admNeuTile(
            iconPer: ImageIcon(
              AssetImage("images/logo.png"),
              size: 80,
              color: Colors.green[800].withOpacity(0.70),
            ),
            title: "Feed de notícias",
            page: 0,
            ative: false,
          ),
          admNeuTile(
            icon: Icons.send,
            title: "Notificações",
            page: 1,
            ative: false,
          ),
          admNeuTile(
            icon: Icons.data_usage,
            title: "Controle de Receita",
            page: 2,
            ative: false,
          ),
          admNeuTile(
            icon: Icons.card_giftcard,
            title: "Cupons",
            page: 3,
            ative: false,
          ),
          admNeuTile(
            icon: Icons.card_membership,
            title: "Associações",
            page: 4,
            ative: false,
          ),
          admNeuTile(
            icon: Icons.person,
            title: "Usuários",
            page: 5,
            ative: true,
          ),
          admNeuTile(
            icon: Icons.shopping_cart,
            title: "Pedidos",
            page: 6,
            ative: false,
          ),
          admNeuTile(
            icon: Icons.list,
            title: "Produtos",
            page: 7,
            ative: false,
          ),
          admNeuTile(
            icon: FontAwesome5Solid.handshake,
            title: "Parceria",
            page: 8,
            ative: false,
          ),
        ];
        break;
      default:
        return [Container()];
        break;
    }
  }

  Widget admNeuTile(
      {IconData icon,
      ImageIcon iconPer,
      @required String title,
      @required bool ative,
      @required int page}) {
    return NeumorphicButton(
      // margin: EdgeInsets.all(5.0),
      drawSurfaceAboveChild: true,
      style: NeumorphicStyle(
        depth: 15.0,
        intensity: 0.20,
        surfaceIntensity: 0.15,
        shape: NeumorphicShape.concave,
        lightSource: DateTime.now().hour % 2 == 0
            ? LightSource.topLeft
            : LightSource.topRight,
        /* page % 2 == 0
            ? LightSource.lerp(LightSource.left, LightSource.left, 1.0)
            : LightSource.lerp(LightSource.right, LightSource.right, 0.5),*/
        color: ative
            ? Colors.black.withOpacity(0.9)
            : Colors.black.withOpacity(0.7),
      ),
      onClick: ative
          ? () {
              if (page <= 3) {
                setState(() {
                  Navigator.of(context).push(
                      MaterialPageRoute(builder: (context) => _pages[page]));
                  /*this.page = page;
            changePage = true;*/
                });
              } else if (page == 4) {
                showDialog(
                    context: context,
                    builder: (context) => AssociationDialog());
                /* } else if (page == 2) {
          Navigator.of(context).push(
              MaterialPageRoute(builder: (context) => RevenueControlScreen()));
          //showAlertDialog(context);
        } else if (page == 3) {
          Navigator.of(context)
              .push(MaterialPageRoute(builder: (context) => CouponsScreen()));*/
              } else {
                page = page - 4;
                widget.controller.jumpToPage(page);
                /* widget.controller.animateToPage(page,
                    duration: Duration(milliseconds: 500),
                    curve: Curves.ease);*/
              }
            }
          : () {
              showAlertDialog(context, title);
            },
      // shape: BoxShape.rectangle,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.stretch,
        mainAxisAlignment: MainAxisAlignment.start,
        mainAxisSize: MainAxisSize.min,
        children: <Widget>[
          Padding(
              padding: const EdgeInsets.fromLTRB(8, 10, 5, 15),
              child: icon != null
                  ? Icon(
                      icon,
                      color: ative
                          ? Colors.green[800]
                          : Colors.green[800].withOpacity(0.70),
                      size: 80.0,
                    )
                  : iconPer),
          //Divider(color: Colors.grey[600]),
          Padding(
            padding: const EdgeInsets.all(2.0),
            child: Text(title,
                textAlign: TextAlign.center,
                style: TextStyle(
                    fontSize: 16.0,
                    fontWeight: FontWeight.bold,
                    color: ative ? Colors.white : Colors.white70)),
          ),
        ],
      ),
    );
  }

  Widget admTile(
      {IconData icon,
      ImageIcon iconPer,
      @required String title,
      @required int page}) {
    return Card(
      elevation: 8.0,
      color: Colors.black,
      margin: EdgeInsets.symmetric(horizontal: 8, vertical: 5),
      child: InkWell(
        onTap: () {
          if (page < 2) {
            setState(() {
              Navigator.of(context)
                  .push(MaterialPageRoute(builder: (context) => _pages[page]));
              /*this.page = page;
              changePage = true;*/
            });
          } else if (page == 2 || page == 3) {
            //  showAlertDialog(context);
          } else {
            page = page - 3;
            widget.controller.jumpToPage(page);
            /* widget.controller.animateToPage(page,
                duration: Duration(milliseconds: 500), curve: Curves.ease);*/
          }
        },
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.stretch,
          mainAxisAlignment: MainAxisAlignment.start,
          mainAxisSize: MainAxisSize.min,
          children: <Widget>[
            Padding(
                padding: const EdgeInsets.fromLTRB(8, 10, 5, 8),
                child: icon != null
                    ? Icon(
                        icon,
                        color: Colors.green[900],
                        size: 90.0,
                      )
                    : iconPer),
            Padding(
              padding: const EdgeInsets.only(top: 5.0, bottom: 5.0),
              child: Text(title,
                  textAlign: TextAlign.center,
                  style: TextStyle(
                      fontSize: 18.0,
                      fontWeight: FontWeight.bold,
                      color: Colors.white)),
            ),
            Divider(color: Colors.grey[600]),
          ],
        ),
      ),
    );
  }

  showAlertDialog(BuildContext context, String titleTab) {
    Widget botaoContinuar = FlatButton(
      color: Colors.transparent,
      child: Text(
        "Tudo bem!",
        style: TextStyle(color: Theme.of(context).primaryColor),
      ),
      onPressed: () {
        Navigator.pop(context);
      },
    );
    showDialog(
      context: context,
      builder: (BuildContext context) {
        return AlertDialog(
            elevation: 2.0,
            shape:
                OutlineInputBorder(borderRadius: BorderRadius.circular(12.5)),
            title: Text("Você não tem permissão para acessar $titleTab !",
                textAlign: TextAlign.center),
            content: Text(
                "Você não possuí os privilégios necessários para acessar essa tela !",
                textAlign: TextAlign.justify),
            actions: [botaoContinuar]);
      },
    );
  }
}
