import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:faen_app_admin/widgets/tiles/partner_tile.dart';
import 'package:flutter/material.dart';

class PartnerTab extends StatefulWidget {
  @override
  _PartnerTabState createState() => _PartnerTabState();
}

class _PartnerTabState extends State<PartnerTab> {
  @override
  Widget build(BuildContext context) {
    return FutureBuilder<QuerySnapshot>(
      future: Firestore.instance.collection("partners").getDocuments(),
      builder: (context, snapshot) {
        if (!snapshot.hasData)
          return Center(
            child: CircularProgressIndicator(
              valueColor: AlwaysStoppedAnimation(Colors.white),
            ),
          );
        return RefreshIndicator(
          backgroundColor: Colors.white.withOpacity(0.9),
          color: Colors.greenAccent[700],
          onRefresh: () async {
            await Future.delayed(Duration(seconds: 1), () {
              setState(() {});
            });
          },
          child: GridView.builder(
            gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
              crossAxisCount: 2,
              crossAxisSpacing: 4.0,
              mainAxisSpacing: 4.0,
              childAspectRatio: 0.75,
            ),
            itemCount: snapshot.data.documents.length,
            itemBuilder: (context, index) {
              return PartnerTile(snapshot.data.documents[index]);
            },
          ),
        );
      },
    );
  }
}
