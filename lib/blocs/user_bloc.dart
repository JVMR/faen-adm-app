import 'package:bloc_pattern/bloc_pattern.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:rxdart/rxdart.dart';

class UserBloc extends BlocBase {
  final _usersController = BehaviorSubject<List>();

  Stream<List> get outUsers => _usersController.stream;

  Map<String, Map<String, dynamic>> _users = {};
  Map<String, dynamic> admUserData = {};

  Firestore _firestore = Firestore.instance;

  UserBloc() {
    _addUsersListener();
  }

  void onChangedSearch(String search) {
    if (search.trim().isEmpty) {
      getNoFilter();
    } else {
      _usersController.add(_filter(search.trim()));
    }
  }

  void getNoFilter() {
    _usersController.add(_users.values.toList());
  }

  void getFilterMembersOnly() {
    _usersController.add(_filterMembersOnly());
  }

  void getFilterNoMembersOnly() {
    _usersController.add(_filterNoMembersOnly());
  }

  List<Map<String, dynamic>> _filter(String search) {
    List<Map<String, dynamic>> filteredUsers =
        List.from(_users.values.toList());
    filteredUsers.retainWhere((user) {
      return user["name"].toUpperCase().contains(search.toUpperCase());
    });
    return filteredUsers;
  }

  List<Map<String, dynamic>> _filterMembersOnly() {
    List<Map<String, dynamic>> filteredUsers =
        List.from(_users.values.toList());
    filteredUsers.retainWhere((user) {
      return user["typeUser"] == "Associado";
    });
    return filteredUsers;
  }

  List<Map<String, dynamic>> _filterNoMembersOnly() {
    List<Map<String, dynamic>> filteredUsers =
        List.from(_users.values.toList());
    filteredUsers.retainWhere((user) {
      return user["typeUser"] == "Não Associado";
    });
    return filteredUsers;
  }

  void _addUsersListener() {
    _firestore
        .collection("users")
        .orderBy("name")
        .snapshots()
        .listen((snapshot) {
      snapshot.documentChanges.forEach((change) {
        String uid = change.document.documentID;

        switch (change.type) {
          case DocumentChangeType.added:
            _users[uid] = change.document.data;
            _users[uid]["uid"] = uid;
            _subscribeToOrders(uid);
            break;
          case DocumentChangeType.modified:
            _users[uid].addAll(change.document.data);
            _usersController.add(_users.values.toList());
            break;
          case DocumentChangeType.removed:
            _users.remove(uid);
            _unsubscribeToOrders(uid);
            _usersController.add(_users.values.toList());
            break;
        }
      });
    });
  }

  void _subscribeToOrders(String uid) {
    _users[uid]["subscription"] = _firestore
        .collection("users")
        .document(uid)
        .collection("orders")
        .snapshots()
        .listen((orders) async {
      int numOrders = orders.documents.length;

      double money = 0.0;

      for (DocumentSnapshot d in orders.documents) {
        DocumentSnapshot order =
            await _firestore.collection("orders").document(d.documentID).get();

        if (order.data == null) continue;

        money += order.data["totalPrice"];
      }

      _users[uid].addAll({"money": money, "orders": numOrders});

      _usersController.add(_users.values.toList());
    });
  }

  Map<String, dynamic> getUser(String uid) {
    return _users[uid];
  }

  Map<String, Map<String, dynamic>> getUsers() {
    return _users;
  }

  void _unsubscribeToOrders(String uid) {
    _users[uid]["subscription"].cancel();
  }

  Future<bool> updateUserData(
      String uid, Map<String, dynamic> newUserData) async {
    try {
      await _firestore
          .collection("users")
          .document(uid)
          .updateData(newUserData);
      return true;
    } catch (error) {
      print("Erro na atualização do Usuário: " + error.toString());
      return false;
    }
  }

  Future<void> deleteUser(String uid) async {
    await Firestore.instance
        .collection("users")
        .document(uid)
        .delete()
        .catchError((error) {
      print(error.toString());
    });
  }

  void setAdmUserData(String admUID) {
    admUserData = _users[admUID];
    print("Dados do administrador: $admUserData");
  }

  @override
  void dispose() {
    super.dispose();
    _usersController.close();
  }
}
