import 'dart:io';
import 'package:bloc_pattern/bloc_pattern.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_storage/firebase_storage.dart';
import 'package:intl/intl.dart';
import 'package:rxdart/rxdart.dart';

class PostFeedBloc extends BlocBase {
  final _dataController = BehaviorSubject<Map>();
  final _loadingController = BehaviorSubject<bool>();
  final _createdController = BehaviorSubject<bool>();
  final _markdownController = BehaviorSubject<String>();

  Stream<Map> get outData => _dataController.stream;
  Stream<bool> get outLoading => _loadingController.stream;
  Stream<bool> get outCreated => _createdController.stream;
  Stream<String> get outMarkdownData => _markdownController.stream;

  String categoryId;
  DocumentSnapshot postFeed;

  Map<String, dynamic> unsavedData;

  PostFeedBloc({this.postFeed}) {
    if (postFeed != null) {
      unsavedData = Map.of(postFeed.data);
      _markdownController.add(unsavedData["description"]);

      //print(unsavedData["image"]);

      _createdController.add(true);
    } else {
      var now = DateTime.now();
      String date = DateFormat("dd/MM/yyyy").format(now);
      unsavedData = {
        "title": null,
        "subTitle": null,
        "description": null,
        "image": null,
        "date": date,
        "createdAt": FieldValue.serverTimestamp(),
        "likes": 0
        //"style": Estilo do post, "Novo Evento", "Novos Produtos", "Nova Parceria"
      };

      _createdController.add(false);
    }

    _dataController.add(unsavedData);
  }

  void saveTitle(String title) {
    unsavedData["title"] = title;
  }

  void saveSubTitle(String subTitle) {
    unsavedData["subTitle"] = subTitle;
  }

  void saveDescription(String description) {
    unsavedData["description"] = description;
  }

  void saveImage(File image) {
    unsavedData["image"] = image;
  }

  void updateMarkdownData(String newData) {
    _markdownController.add(newData);
  }

  Future<bool> savePostFeed() async {
    _loadingController.add(true);

    try {
      if (postFeed != null) {
        await _uploadImages(postFeed.documentID);
        await postFeed.reference.updateData(unsavedData);
      } else {
        DocumentReference dr = await Firestore.instance
            .collection("feed")
            .add(Map.from(unsavedData)..remove("image"));
        await _uploadImages(dr.documentID);
        await dr.updateData(unsavedData);
      }

      _createdController.add(true);
      _loadingController.add(false);
      return true;
    } catch (e) {
      print("Erro ao salvar: " + e.toString());
      _loadingController.add(false);
      return false;
    }
  }

  Future _uploadImages(String postId) async {
    /*for (int i = 0; i < unsavedData["images"].length; i++) {
      if (unsavedData["images"][i] is String) continue;*/

    try {
      if (unsavedData["image"] is File) {
        StorageUploadTask uploadTask = FirebaseStorage.instance
            .ref()
            .child("PostsFeed")
            .child(postId)
            .putFile(unsavedData["image"]);

        StorageTaskSnapshot s = await uploadTask.onComplete;
        String downloadUrl = await s.ref.getDownloadURL();

        unsavedData["image"] = downloadUrl;
      }
    } catch (e) {
      print("${e.toString()}");
    }
    //}
  }

  void deletePost() {
    postFeed.reference.delete();
  }

  @override
  void dispose() {
    super.dispose();
    _dataController.close();
    _loadingController.close();
    _createdController.close();
    _markdownController.close();
  }
}
