import 'package:bloc_pattern/bloc_pattern.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:rxdart/rxdart.dart';

class RevenueControlBloc extends BlocBase {
  final _dataController = BehaviorSubject<List<DocumentSnapshot>>();
  final _countOrdersController = BehaviorSubject<int>();
  final _countUsersMapController = BehaviorSubject<Map<String, int>>();

  Stream<List<DocumentSnapshot>> get outData => _dataController.stream;
  Stream<int> get outCountOrders => _countOrdersController.stream;
  Stream<Map<String, int>> get outCountUsersMap =>
      _countUsersMapController.stream;

  Firestore _firestore = Firestore.instance;

  List<DocumentSnapshot> _dataRevenue = [];
  int countOrders;
  Map<String, int> countUserMap = {};

  List<DocumentSnapshot> getDataRevenue() => _dataRevenue;
  int getCountOrders() => countOrders;
  Map<String, int> getCountUsersMap() => countUserMap;

  RevenueControlBloc() {
    /*String currentDate = "${DateTime.now().month}.${DateTime.now().year}";
    createNewMonthDocument(currentDate);*/
    checkCurrentMonth().then((a) async {
      await loadData();
    });
    loadCountOrders();
    loadCountUsersMap();

    //loadData();
  }

  Future<void> loadData() async {
    await _firestore.collection("revenue").getDocuments().then((datas) {
      datas.documents.forEach((dataSnap) {
        _dataRevenue.add(dataSnap);
      });
    });
    _dataController.add(_dataRevenue);
  }

  Future<void> checkCurrentMonth() async {
    String currentDate = "${DateTime.now().month}.${DateTime.now().year}";
    try {
      print(currentDate);
      var docMonth =
          await _firestore.collection("revenue").document(currentDate).get();
      print("Documento: " + "${docMonth.exists}");
      //  print("entro no if");
      if (docMonth == null || !docMonth.exists) {
        print("entro no if");
        await createNewMonthDocument(currentDate);
      }
    } catch (e) {
      print("===" + e.toString() + "===");
    }
  }

  Future<void> createNewMonthDocument(String currentDate) async {
    List<String> titlesProducts = [];
    Map<String, dynamic> productsSold = {};
    print("Aloha");
    var categories = await _firestore.collection('products').getDocuments();
    var products = categories.documents.map((category) async {
      return await _firestore
          .collection('products')
          .document(category.documentID)
          .collection("items")
          .getDocuments();
    });

    for (var productsQuery in products) {
      print("Entrou no For Each");
      // var aloha = await doc;
      //print(doc.toString());
      await productsQuery.then((productsSnap) {
        productsSnap.documents.forEach((product) {
          print(product.data["title"]);
          titlesProducts.add(product.data["title"]);
          productsSold[product.data["title"]] = 0;
        });
      });
      /*await aloha.then((a) {
        a.documents.map((product) {
          print(product.data["title"]);
          titlesProducts.add(product.data["title"]);
          productsSold[product.data["title"]] = 0;
        });
      });*/
    }
    /*await _firestore
          .collection('products')
          .document(category.documentID)
          .collection("items").getDocuments();*/

    //products.documents.map(())
    /*titlesProducts.add(product.data["title"]);
          productsSold[product.data["title"]] = 0;*/

    print(titlesProducts);
    print(productsSold);

    Map<String, dynamic> monthInitialData = {
      'totalOrder': 0,
      'totalOrderCash': 0.0,
      'titlesProducts': titlesProducts,
      'productsSold': productsSold,
    };
    await _firestore
        .collection('revenue')
        .document(currentDate)
        .setData(monthInitialData);
    //await loadData();
  }

  Future<void> addCash(String cash) async {
    print("Função order cash:");
    double newCash = double.tryParse(cash);
    DocumentReference refRevenue = Firestore.instance
        .collection("revenue")
        .document("${DateTime.now().month}.${DateTime.now().year}");
    DocumentSnapshot docRevenue = await refRevenue.get();
    newCash += docRevenue.data["totalOrderCash"];
    await refRevenue.updateData({"totalOrderCash": newCash});
  }

  Future<void> loadCountOrders() async {
    var docs = await Firestore.instance.collection("orders").getDocuments();
    countOrders = docs.documents.length;
    _countOrdersController.add(countOrders);
  }

  Future<void> loadCountUsersMap() async {
    countUserMap["countTotalUsers"] = 0;
    countUserMap["countMembers"] = 0;
    countUserMap["countNonMembers"] = 0;
    countUserMap["countAdms"] = 0;

    var users = await Firestore.instance.collection("users").getDocuments();
    users.documents.forEach((user) {
      countUserMap["countTotalUsers"]++;
      switch (user.data["typeUser"]) {
        case "Associado":
          countUserMap["countMembers"]++;
          break;
        case "Não Associado":
          countUserMap["countNonMembers"]++;
          break;
        case "Administrador":
          countUserMap["countAdms"]++;
          break;
      }
    });
    print(countUserMap);
    _countUsersMapController.add(countUserMap);
  }

  Map<String, dynamic> getDataToPrint() {
    return {
      "dataRevenue": _dataRevenue,
      "countOrders": countOrders,
      "countUserMap": countUserMap
    };
  }

  @override
  void dispose() {
    super.dispose();
    _dataController.close();
    _countOrdersController.close();
    _countUsersMapController.close();
  }
}
