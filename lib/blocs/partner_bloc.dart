import 'dart:io';

import 'package:bloc_pattern/bloc_pattern.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_storage/firebase_storage.dart';
import 'package:rxdart/rxdart.dart';

class PartnerBloc extends BlocBase {
  final _dataController = BehaviorSubject<Map>();
  final _loadingController = BehaviorSubject<bool>();
  final _createdController = BehaviorSubject<bool>();

  Stream<Map> get outData => _dataController.stream;
  Stream<bool> get outLoading => _loadingController.stream;
  Stream<bool> get outCreated => _createdController.stream;

  String categoryId;
  DocumentSnapshot partner;

  Map<String, dynamic> unsavedData;

  PartnerBloc({this.partner}) {
    if (partner != null) {
      unsavedData = Map.of(partner.data);
      print(unsavedData["image"]);
      //unsavedData["images"] = List.of(partner.data["images"]);
      //unsavedData["sizes"] = List.of(partner.data["sizes"]);

      _createdController.add(true);
    } else {
      unsavedData = {
        "title": null,
        "subTitle": null,
        "phone": null,
        "address": null,
        "description": null,
        "image": null,
      };

      _createdController.add(false);
    }

    _dataController.add(unsavedData);
  }

  void saveTitle(String title) {
    unsavedData["title"] = title;
  }

  void saveSubTitle(String subTitle) {
    unsavedData["subTitle"] = subTitle;
  }

  void savePhone(String phone) {
    unsavedData["phone"] = phone;
  }

  void saveAddress(String address) {
    unsavedData["address"] = address;
  }

  void saveDescription(String description) {
    unsavedData["description"] = description;
  }

  void saveImage(File image) {
    unsavedData["image"] = image;
  }

  Future<bool> savePartner() async {
    _loadingController.add(true);

    try {
      if (partner != null) {
        await _uploadImages(partner.documentID);
        await partner.reference.updateData(unsavedData);
      } else {
        DocumentReference dr = await Firestore.instance
            .collection("partners")
            .add(Map.from(unsavedData)..remove("image"));
        await _uploadImages(dr.documentID);
        await dr.updateData(unsavedData);
      }

      _createdController.add(true);
      _loadingController.add(false);
      return true;
    } catch (e) {
      print("Erro ao salvar: " + e.toString());
      _loadingController.add(false);
      return false;
    }
  }

  Future _uploadImages(String partnerId) async {
    /*for (int i = 0; i < unsavedData["images"].length; i++) {
      if (unsavedData["images"][i] is String) continue;*/

    try {
      if (unsavedData["image"] != null && unsavedData["image"] is File) {
        StorageUploadTask uploadTask = FirebaseStorage.instance
            .ref()
            .child("Partners")
            .child(partnerId)
            .putFile(unsavedData["image"]);

        StorageTaskSnapshot s = await uploadTask.onComplete;
        String downloadUrl = await s.ref.getDownloadURL();

        unsavedData["image"] = downloadUrl;
      }
    } catch (e) {
      print("${e.toString()}");
    }
    //}
  }

  void deletePartner() {
    partner.reference.delete();
  }

  @override
  void dispose() {
    super.dispose();
    _dataController.close();
    _loadingController.close();
    _createdController.close();
  }
}
