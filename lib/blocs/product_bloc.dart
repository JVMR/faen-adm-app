import 'package:bloc_pattern/bloc_pattern.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:faen_app_admin/models/notification_sender.dart';
import 'package:firebase_storage/firebase_storage.dart';
import 'package:rxdart/rxdart.dart';

class ProductBloc extends BlocBase {
  final _dataController = BehaviorSubject<Map>();
  final _loadingController = BehaviorSubject<bool>();
  final _createdController = BehaviorSubject<bool>();

  Stream<Map> get outData => _dataController.stream;
  Stream<bool> get outLoading => _loadingController.stream;
  Stream<bool> get outCreated => _createdController.stream;

  String categoryId;
  DocumentSnapshot product;
  NotificationSender _notificationSender = NotificationSender();

  Map<String, dynamic> unsavedData;

  ProductBloc({this.categoryId, this.product}) {
    if (product != null) {
      unsavedData = Map.of(product.data);
      unsavedData["images"] = List.of(product.data["images"]);
      unsavedData["sizes"] = List.of(product.data["sizes"]);

      _createdController.add(true);
    } else {
      unsavedData = {
        "title": null,
        "description": null,
        "price": null, // Preço para não associados
        "priceMember": null, // Preço para associados
        "images": [],
        "hasSize": false,
        "sizes": [],
        "status": "Disponível",
      };

      _createdController.add(false);
    }

    _dataController.add(unsavedData);
  }

  void saveTitle(String title) {
    unsavedData["title"] = title;
  }

  void saveDescription(String description) {
    unsavedData["description"] = description;
  }

  void savePriceMember(String price) {
    unsavedData["priceMember"] = double.parse(price);
  }

  void savePrice(String price) {
    unsavedData["price"] = double.parse(price);
  }

  void saveImages(List images) {
    unsavedData["images"] = images;
  }

  void saveHasSize(bool hasSize) {
    unsavedData["hasSize"] = hasSize;
  }

  void saveSizes(List sizes) {
    unsavedData["sizes"] = sizes;
  }

  void updateStatus(String newValue) {
    unsavedData["status"] = newValue;
  }

  Future<bool> saveProduct() async {
    _loadingController.add(true);

    try {
      if (product != null) {
        await _uploadImages(product.documentID);
        await product.reference.updateData(unsavedData);
      } else {
        DocumentReference dr = await Firestore.instance
            .collection("products")
            .document(categoryId)
            .collection("items")
            .add(Map.from(unsavedData)..remove("images"));
        await _uploadImages(dr.documentID);
        await dr.updateData(unsavedData);
        bool deuCerto =
            await _notificationSender.sendNoticationToTopicWithImage(
                title: "Novo produto disponível !",
                body: unsavedData["title"] +
                    " está disponível na loja. Confira no app agora : ) ",
                topic: "test",
                urlImage: unsavedData["images"][0]);
        DocumentReference refRevenue = Firestore.instance
            .collection("revenue")
            .document("${DateTime.now().month}.${DateTime.now().year}");
        DocumentSnapshot docRevenue = await refRevenue.get();
        docRevenue.data["titleProducts"].add(unsavedData["title"]);
        docRevenue.data["productsSold"][unsavedData["title"]] = 0;
        await refRevenue.updateData(docRevenue.data);
        print("Resultado da notificação - $deuCerto");
      }

      _createdController.add(true);
      _loadingController.add(false);
      return true;
    } catch (e) {
      print(e.toString());
      _loadingController.add(false);
      return false;
    }
  }

  Future _uploadImages(String productId) async {
    for (int i = 0; i < unsavedData["images"].length; i++) {
      if (unsavedData["images"][i] is String) continue;

      StorageUploadTask uploadTask = FirebaseStorage.instance
          .ref()
          .child(categoryId)
          .child(productId)
          .child(DateTime.now().millisecondsSinceEpoch.toString())
          .putFile(unsavedData["images"][i]);

      StorageTaskSnapshot s = await uploadTask.onComplete;
      String downloadUrl = await s.ref.getDownloadURL();

      unsavedData["images"][i] = downloadUrl;
    }
  }

  void deleteProduct() {
    product.reference.delete();
  }

  @override
  void dispose() {
    super.dispose();
    _dataController.close();
    _loadingController.close();
    _createdController.close();
  }
}
