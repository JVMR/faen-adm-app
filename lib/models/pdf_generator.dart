//import 'package:flutter/material.dart';
import 'dart:io';

import 'package:bloc_pattern/bloc_pattern.dart';
import 'package:faen_app_admin/blocs/revenue_control_bloc.dart';
import 'package:path_provider/path_provider.dart';
import 'package:pdf/widgets.dart' as pw;
import 'package:pdf/pdf.dart';
import 'package:printing/printing.dart';

class PdfGenerator {
  static pw.Document pdf;
  static Future<void> generateListUserDocument(
      PdfPageFormat format, Map<String, dynamic> content) async {
    String dateNow =
        "${DateTime.now().day}/${DateTime.now().month}/${DateTime.now().year}";
    pdf = pw.Document(
        title: 'Relatório de Usuários - $dateNow', author: 'FAEN Admin App');
    List<pw.Widget> contentInParagraph = [];
    content.forEach((uid, infoUser) {
      /*pw.Paragraph paragraph = pw.Paragraph(
        text:
            "${infoUser["name"]} - ${infoUser["typeUser"]} - ${infoUser["course"]}\n",
        textAlign: pw.TextAlign.left,
        margin: pw.EdgeInsets.only(bottom: 5.0),
      );*/
      final paragraph = pw.Padding(
          padding: pw.EdgeInsets.only(bottom: 5.0),
          child: pw.RichText(
            text: pw.TextSpan(
              text: "Nome: ",
              children: <pw.TextSpan>[
                pw.TextSpan(
                  text: "${infoUser["name"]}.\n",
                  style: pw.TextStyle(fontWeight: pw.FontWeight.bold),
                ),
                pw.TextSpan(
                  text: "CPF: ",
                  style: pw.TextStyle(fontWeight: pw.FontWeight.normal),
                ),
                pw.TextSpan(
                  text: "${infoUser["cpf"]}.\n",
                  style: pw.TextStyle(fontWeight: pw.FontWeight.bold),
                ),
                pw.TextSpan(
                  text: "Tipo: ",
                  style: pw.TextStyle(fontWeight: pw.FontWeight.normal),
                ),
                pw.TextSpan(
                  text: "${infoUser["typeUser"]}.\n",
                  style: pw.TextStyle(fontWeight: pw.FontWeight.bold),
                ),
                pw.TextSpan(
                  text: "Curso: ",
                  style: pw.TextStyle(fontWeight: pw.FontWeight.normal),
                ),
                pw.TextSpan(
                  text: "${infoUser["course"]}.\n",
                  style: pw.TextStyle(fontWeight: pw.FontWeight.bold),
                ),
              ],
            ),
          ));
      contentInParagraph.add(paragraph);
    });
    // print(contentInParagraph);
    pdf.addPage(pw.MultiPage(
        crossAxisAlignment: pw.CrossAxisAlignment.start,
        header: (pw.Context context) {
          //Cabeçalho
          if (context.pageNumber == 1) {
            return null;
          }
          return pw.Container(
            alignment: pw.Alignment.centerRight,
            margin: pw.EdgeInsets.only(bottom: 10.0),
            decoration: const pw.BoxDecoration(
                border: pw.BoxBorder(
                    bottom: true, width: 0.5, color: PdfColors.grey)),
            child: pw.Text('Relatório de Usuários - $dateNow',
                textAlign: pw.TextAlign.center),
          );
        },
        footer: (pw.Context context) {
          //Rodapé das páginas
          return pw.Container(
              alignment: pw.Alignment.centerRight,
              child: pw.Text(
                  'Página ${context.pageNumber} de ${context.pagesCount}',
                  style: pw.Theme.of(context)
                      .defaultTextStyle
                      .copyWith(color: PdfColors.grey)));
        },
        build: (pw.Context context) => <pw.Widget>[
              //Conteúdo do Pdf
              pw.Container(
                alignment: pw.Alignment.center,
                margin: pw.EdgeInsets.only(bottom: 10.0),
                decoration: const pw.BoxDecoration(
                    border: pw.BoxBorder(
                        bottom: true, width: 0.5, color: PdfColors.grey)),
                child: pw.Text('Relatório de Usuários FAEN App',
                    textScaleFactor: 2, textAlign: pw.TextAlign.center),
              ),
              pw.Column(
                children: contentInParagraph,
              ),
            ]));
    //return pdf;
  }

  static Future<void> generateRevenueControlDocument(
      PdfPageFormat format) async {
    final _revenueControlBloc = BlocProvider.getBloc<RevenueControlBloc>();
    final countUsersMap = _revenueControlBloc.getCountUsersMap();
    String dateNow =
        "${DateTime.now().day}/${DateTime.now().month}/${DateTime.now().year}";
    pdf = pw.Document(
        title: 'Relatório de Controle de Receitas - $dateNow',
        author: 'FAEN Admin App');
    List<pw.Widget> contentDataRevenueParagraph =
        _revenueControlBloc.getDataRevenue().map((doc) {
      String productsSold = doc.data["productsSold"].toString().substring(
          1,
          doc.data["productsSold"].toString().length -
              1); //Utilizado para remover o parênteses que retornam do toString()
      return pw.RichText(
        text: pw.TextSpan(
          text: "Dados de ${doc.documentID}:\n",
          style: pw.TextStyle(
            fontWeight: pw.FontWeight.bold,
            height: 2.0,
            fontSize: 14.0,
          ),
          children: <pw.TextSpan>[
            pw.TextSpan(
              text: "Produtos vendidos - $productsSold.\n",
              style: pw.TextStyle(
                  fontSize: 12.0, fontWeight: pw.FontWeight.normal),
            ),
            pw.TextSpan(
              text: "Total de pedidos: ",
              style: pw.TextStyle(
                  fontSize: 12.0, fontWeight: pw.FontWeight.normal),
            ),
            pw.TextSpan(
              text: "${doc.data["totalOrder"]}.\n",
              style: pw.TextStyle(
                fontSize: 12.0,
                //fontWeight: pw.FontWeight.normal
              ),
            ),
            pw.TextSpan(
              text: "Total em Reais: ",
              style: pw.TextStyle(
                  fontSize: 12.0, fontWeight: pw.FontWeight.normal),
            ),
            pw.TextSpan(
              text: "R\$${doc.data["totalOrderCash"].toStringAsFixed(2)}.\n",
              style: pw.TextStyle(
                fontSize: 12.0,
                //fontWeight: pw.FontWeight.normal
              ),
            ),
          ],
        ),
        softWrap: true,
      );
      /* return pw.Paragraph(text: '''
        Dados de ${doc.documentID}:  
          $productsSold
          Total de pedido: ${doc.data["totalOrder"]}
          Total em Reais: R\$${doc.data["totalOrderCash"].toStringAsFixed(2)}
          ''');*/
    }).toList();
    /*  for (int i = 0; content["dataRevenue"].length; i++) {}
    content["dataRevenue"].forEach((doc) {
      pw.Paragraph paragraph = pw.Paragraph(
          text: doc["name"] - doc["typeUser"] + "\n",
          textAlign: pw.TextAlign.justify);
      contentDataRevenueParagraph.add(paragraph);
    });*/
    Future.delayed(Duration(seconds: 5), () {});
    print(contentDataRevenueParagraph);
    pdf.addPage(pw.MultiPage(
        crossAxisAlignment: pw.CrossAxisAlignment.start,
        maxPages: 100,
        header: (pw.Context context) {
          //Cabeçalho
          if (context.pageNumber == 1) {
            return null;
          }
          return pw.Container(
            alignment: pw.Alignment.center,
            decoration: const pw.BoxDecoration(
                border: pw.BoxBorder(
                    bottom: true, width: 0.5, color: PdfColors.grey)),
            child: pw.Text('Relatório de Controle de Receitas - $dateNow',
                textAlign: pw.TextAlign.center),
          );
        },
        footer: (pw.Context context) {
          //Rodapé das páginas
          //return pw.Container();
          return pw.Container(
              alignment: pw.Alignment.centerRight,
              child: pw.Text(
                  "Página ${context.pageNumber} de ${context.pagesCount}\n"
                  "Relatório emitido em $dateNow.",
                  style: pw.Theme.of(context)
                      .defaultTextStyle
                      .copyWith(color: PdfColors.grey)));
          /* return pw.Row(
              mainAxisAlignment: pw.MainAxisAlignment.spaceBetween,
              crossAxisAlignment: pw.CrossAxisAlignment.stretch,
              children: [
                pw.Container(
                    alignment: pw.Alignment.centerLeft,
                    child: pw.Text('Relatório emitido em $dateNow',
                        style: pw.Theme.of(context)
                            .defaultTextStyle
                            .copyWith(color: PdfColors.grey))),
                pw.Container(
                    alignment: pw.Alignment.centerRight,
                    child: pw.Text(
                        'Página ${context.pageNumber} de ${context.pagesCount}',
                        style: pw.Theme.of(context)
                            .defaultTextStyle
                            .copyWith(color: PdfColors.grey))),
              ]);*/
        },
        build: (pw.Context context) => <pw.Widget>[
              //Conteúdo do Pdf
              pw.Header(
                  level: 0,
                  child: pw.Text(
                    'Relatório de Controle de Receitas',
                    textScaleFactor: 2,
                    textAlign: pw.TextAlign.center,
                  )),
              pw.Column(
                  // alignment: pw.WrapAlignment.spaceBetween,
                  children: contentDataRevenueParagraph),
              _buildLineRichTextWithData(
                  "Pedidos Atuais: ", "${_revenueControlBloc.countOrders}"),
              pw.Paragraph(
                text: "Contagem de Usuários: ",
                style: pw.TextStyle(
                    fontSize: 14.0, fontWeight: pw.FontWeight.bold),
                margin: pw.EdgeInsets.only(bottom: 5.0, top: 5.0),
              ),
              _buildBullet(
                  "${countUsersMap["countMembers"]} usuários Associados."),
              _buildBullet(
                  "${countUsersMap["countNonMembers"]} usuários Não Associados."),
              _buildBullet(
                  "${countUsersMap["countAdms"]} usuários Administradores."),
              _buildBullet(
                  "${countUsersMap["countTotalUsers"]} usuários totais."),
              /*pw.SizedBox(
                height: 15.0,
              ),
              pw.Paragraph(
                text: "Relatório emitido em $dateNow.",
                style: pw.TextStyle(fontStyle: pw.FontStyle.italic),
              )*/
              /*_buildLineRichTextWithData("Usuários Associados: ",
                  countUsersMap["countMembers"].toString()),
              _buildLineRichTextWithData("Usuários não Associados: ",
                  countUsersMap["countNonMembers"].toString()),
              _buildLineRichTextWithData("Usuários Administradores: ",
                  countUsersMap["countAdms"].toString()),
              _buildLineRichTextWithData("Usuários Totais: ",
                  countUsersMap["countTotalUsers"].toString()),*/

              //pw.ListView(children: contentInParagraph),
              /* content.map((uid, info){
                  return pw.Paragraph(
                    text: info.toString()
                  );
                }).toList<Widget>(),*/
            ]));
    //return pdf;
  }

  static pw.Widget _buildBullet(String data) {
    return pw.Bullet(
      text: data,
    );
  }

  static pw.Widget _buildLineRichTextWithData(String legend, String data) {
    return pw.RichText(
        text: pw.TextSpan(
      text: legend,
      children: <pw.TextSpan>[
        pw.TextSpan(
            text: data + ".\n",
            style: pw.TextStyle(
              fontWeight: pw.FontWeight.bold,
            ))
      ],
    ));
  }

  static Future<void> printPdf() async {
    print('Print ...');
    await Printing.layoutPdf(
        onLayout: (PdfPageFormat format) async => pdf.save());
    //  (await generateDocument(format)).save());
  }

  static Future sharePdf() async {
    print('click');
    await Printing.sharePdf(bytes: pdf.save(), filename: 'Relatório.pdf');
  }

  static savePdf() async {
    Directory appDocDir = await getTemporaryDirectory();

    final file = File("${appDocDir.path}/relatorio.pdf");
    file.writeAsBytesSync(pdf.save());
    /*final output = await getDownloadsDirectory();
    final file = File("${output.path}/example.pdf");
    await file.writeAsBytes(pdf.save());*/
  }
}
