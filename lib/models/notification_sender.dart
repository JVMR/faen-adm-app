import 'dart:convert';
import 'package:flutter/material.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:http/http.dart' as http;

class NotificationSender {
  String serverKey =
      "AAAAjNqyZC8:APA91bH6U53o2Gl5glD6qo-yE5L6pYN2Fw3IEK2KQQ5r_5Wqg3MdgBNT2kVKvQYkkDpZg5i57SqlQ7B89q6KYoSuttlaJ2ULYeYHzyF-GwaSOlE-yEpoG0shi094JyyoZ9Q7z_5M0iDs";

  Future<bool> sendNotificationToUser(
      {@required String title,
      @required String body,
      @required String uid}) async {
    // String userToken = await Firestore.instance.collection("users").document(uid).collection("tokens").getDocuments().then((doc) => doc.documents.);
    var querySnapshot = await Firestore.instance
        .collection('users')
        .document(uid)
        .collection("tokens")
        .orderBy("createdAt")
        .getDocuments(); //Ordem crescente
    var tokens = querySnapshot.documents.map((snap) => snap.documentID).last;
    print(tokens);
    //  .then((snap) => snap.documents.map((snap) => snap.documentID));
    try {
      http.Response response = await http.post(
        'https://fcm.googleapis.com/fcm/send',
        headers: <String, String>{
          'Content-Type': 'application/json',
          'Authorization': 'key=$serverKey',
        },
        body: jsonEncode(
          <String, dynamic>{
            'notification': <String, dynamic>{
              'title': '$title',
              'body': '$body',
            },
            "content_available": true,
            "mutable_content": true,
            'priority': 'high',
            'data': <String, dynamic>{
              'click_action': 'FLUTTER_NOTIFICATION_CLICK',
              'id': '2',
              'status': 'done'
            },
            'to': tokens,
          },
        ),
      );
      print(response.statusCode);
      print(response.body);
    } catch (e) {
      print(e.toString());
      return false;
    }
    return true;
  }

  Future<bool> sendNoticationToTopic(
      {@required String title,
      @required String body,
      @required String topic}) async {
    try {
      http.Response response = await http.post(
        'https://fcm.googleapis.com/fcm/send',
        headers: <String, String>{
          'Content-Type': 'application/json',
          'Authorization': 'key=$serverKey',
        },
        body: jsonEncode(
          <String, dynamic>{
            'notification': <String, dynamic>{
              'title': '$title',
              'body': '$body',
            },
            "content_available": true,
            "mutable_content": true,
            'priority': 'high',
            'data': <String, dynamic>{
              'click_action': 'FLUTTER_NOTIFICATION_CLICK',
              'id': '1',
              'status': 'done'
            },
            'to': "/topics/$topic",
          },
        ),
      );
      print(response.statusCode);
      print(response.body);
    } catch (e) {
      print(e.toString());
      return false;
    }
    return true;
  }

  Future<bool> sendNoticationToTopicWithImage(
      {@required String title,
      @required String body,
      @required String topic,
      @required String urlImage}) async {
    try {
      await http.post(
        'https://fcm.googleapis.com/fcm/send',
        headers: <String, String>{
          'Content-Type': 'application/json',
          'Authorization': 'key=$serverKey',
        },
        body: jsonEncode(
          <String, dynamic>{
            'notification': <String, dynamic>{
              'title': '$title',
              'body': '$body',
              'image': "$urlImage"
            },
            "content_available": true,
            "mutable_content": true,
            'priority': 'high',
            'data': <String, dynamic>{
              'click_action': 'FLUTTER_NOTIFICATION_CLICK',
              'id': '1',
              'status': 'done'
            },
            'to': "/topics/$topic",
          },
        ),
      );
    } catch (e) {
      print(e.toString());
      return false;
    }
    return true;
  }
}
