import 'dart:io';

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:firebase_storage/firebase_storage.dart';
import 'package:flutter/material.dart';

class AddUserClass {
  FirebaseAuth _auth = FirebaseAuth.instance;

  FirebaseUser firebaseUser;
  Map<String, dynamic> userData = Map();

  bool isLoading = false;

  void dispose() {
    signOut();
  }

  Future<bool> verifyPrivileges(FirebaseUser user) async {
    return await Firestore.instance
        .collection("admins")
        .document(user.uid)
        .get()
        .then((doc) {
      if (doc.data != null) {
        return true;
      } else {
        return false;
      }
    }).catchError((e) {
      print("Método de verificação de privilégios: " + e.toString());
      return false;
    });
  }

  void addUser(
      {@required Map<String, String> admData,
      @required Map<String, dynamic> userData,
      @required String pass,
      @required VoidCallback onSuccess,
      @required VoidCallback cancel,
      @required Function onFail}) async {
    isLoading = true;
    print("Entrou no signup");
    bool isAdm;
    // await _auth.setLanguageCode("Portuguese");
    //EmailAuthProvider.getCredential();
    //FirebaseUser backUp = await _auth.currentUser();
    //backUp.reauthenticateWithCredential(credential)

    //_auth.signInWithCustomToken(token: await backUp.getIdToken());
    await _auth
        .signInWithEmailAndPassword(
            email: admData["email"], password: admData["password"])
        .then((user) async {
      print(user.user.toString());
      isAdm = await verifyPrivileges(user.user);
    }).catchError((error) {
      onFail(error.toString());
      cancel();
      return;
    });
    print(isAdm);
    if (isAdm == false || isAdm == null) {
      print("Entrou no if de Adm");
      onFail("Esse usuário não tem permissão de administrador !");
      _auth.signOut();
      cancel();
      return;
    }

    await _auth
        .createUserWithEmailAndPassword(
            email: userData["email"], password: pass)
        .then((user) async {
      firebaseUser = user.user;
      await _saveUserData(userData);

      /*_auth.signInWithEmailAndPassword(
          email: admData["email"], password: admData["password"]);*/
      // onSuccess();
      //isLoading = false;
    }).catchError((error) {
      onFail(error.toString());
      isLoading = false;
    });
    _auth
        .signInWithEmailAndPassword(
            email: admData["email"], password: admData["password"])
        .then((user) {
      onSuccess();
      isLoading = false;
    }).catchError((error) {
      onFail(error.toString());
      return;
    });
    /* _auth.signInWithEmailAndPassword(
        email: admData["email"], password: admData["password"]);*/
  }

  void signOut() async {
    await _auth.signOut();

    userData = Map();
    firebaseUser = null;
  }

  void recoverPass(String email) {
    _auth.sendPasswordResetEmail(email: email);
  }

  Future<Null> _saveUserData(Map<String, dynamic> userData) async {
    this.userData = userData;
    await _uploadImage();
    await Firestore.instance
        .collection("users")
        .document(firebaseUser.uid)
        .setData(userData);
  }

  Future<bool> updateUserData(Map<String, dynamic> userData) async {
    this.userData = userData;
    isLoading = true;
    try {
      await _uploadImage();
      await Firestore.instance
          .collection("users")
          .document(firebaseUser.uid)
          .updateData(userData);
      _loadCurrentUser();
      isLoading = false;
      return true;
    } catch (e) {
      print(e.toString());
      isLoading = false;
      return false;
    }
  }

  Future _uploadImage() async {
    if (userData["photo"] is File) {
      StorageUploadTask uploadTask = FirebaseStorage.instance
          .ref()
          .child("Users Photos")
          .child(userData["name"])
          .putFile(userData["photo"]);

      StorageTaskSnapshot s = await uploadTask.onComplete;
      String downloadUrl = await s.ref.getDownloadURL();

      userData["photo"] = downloadUrl;
    }
  }

  Future<Null> _loadCurrentUser() async {
    if (firebaseUser == null) firebaseUser = await _auth.currentUser();
    if (firebaseUser != null) {
      if (userData["name"] == null) {
        DocumentSnapshot docUser = await Firestore.instance
            .collection("users")
            .document(firebaseUser.uid)
            .get();
        userData = docUser.data;
      }
    }
  }
}
