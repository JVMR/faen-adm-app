import 'dart:io';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:faen_app_admin/blocs/partner_bloc.dart';
import 'package:faen_app_admin/validators/partner_validator.dart';
import 'package:faen_app_admin/widgets/image_source_sheet.dart';
import 'package:flutter/material.dart';

class EditPartnerScreen extends StatefulWidget {
  final DocumentSnapshot partner;

  EditPartnerScreen({this.partner});

  @override
  _EditPartnerScreenState createState() => _EditPartnerScreenState(partner);
}

class _EditPartnerScreenState extends State<EditPartnerScreen>
    with PartnerValidator {
  final PartnerBloc _partnerBloc;

  final _formKey = GlobalKey<FormState>();
  final _scaffoldKey = GlobalKey<ScaffoldState>();

  _EditPartnerScreenState(DocumentSnapshot partner)
      : _partnerBloc = PartnerBloc(partner: partner);

  @override
  Widget build(BuildContext context) {
    InputDecoration _buildDecoration(String label) {
      return InputDecoration(
          filled: true,
          fillColor: Colors.white10,
          //isDense: true,
          enabledBorder: OutlineInputBorder(
              borderSide: BorderSide(
                color: Colors.white54,
              ),
              borderRadius: BorderRadius.circular(15.0)),
          focusedBorder: OutlineInputBorder(
              borderSide: BorderSide(
                color: Colors.green[900],
              ),
              borderRadius: BorderRadius.circular(15.0)),
          focusedErrorBorder: OutlineInputBorder(
              borderSide: BorderSide(
                color: Colors.red[900],
              ),
              borderRadius: BorderRadius.circular(15.0)),
          errorBorder: OutlineInputBorder(
              borderSide: BorderSide(
                color: Colors.red[900],
              ),
              borderRadius: BorderRadius.circular(15.0)),
          labelText: label,
          labelStyle:
              TextStyle(fontFamily: 'BebasNeue', color: Colors.white70));
    }

    final _fieldStyle =
        TextStyle(fontFamily: 'Lato', color: Colors.white, fontSize: 16);

    return Container(
      decoration: const BoxDecoration(
          image: DecorationImage(
              image: ExactAssetImage("images/Fundo2.png"), fit: BoxFit.cover)),
      child: Scaffold(
        key: _scaffoldKey,
        // backgroundColor: Colors.grey[850],
        appBar: AppBar(
          elevation: 0,
          centerTitle: true,
          title: StreamBuilder<bool>(
              stream: _partnerBloc.outCreated,
              initialData: false,
              builder: (context, snapshot) {
                return Text(
                    snapshot.data ? "Editar Parceria" : "Criar Nova Parceria");
              }),
          actions: <Widget>[
            StreamBuilder<bool>(
              stream: _partnerBloc.outCreated,
              initialData: false,
              builder: (context, snapshot) {
                if (snapshot.data)
                  return StreamBuilder<bool>(
                      stream: _partnerBloc.outLoading,
                      initialData: false,
                      builder: (context, snapshot) {
                        return IconButton(
                          icon: Icon(Icons.delete_outline),
                          onPressed: snapshot.data
                              ? null
                              : () {
                                  _partnerBloc.deletePartner();
                                  Navigator.of(context).pop();
                                },
                        );
                      });
                else
                  return Container();
              },
            ),
            StreamBuilder<bool>(
                stream: _partnerBloc.outLoading,
                initialData: false,
                builder: (context, snapshot) {
                  return IconButton(
                    icon: Icon(Icons.save),
                    onPressed: snapshot.data ? null : savePartner,
                  );
                }),
          ],
        ),
        body: Stack(
          children: <Widget>[
            Form(
              key: _formKey,
              child: StreamBuilder<Map>(
                  stream: _partnerBloc.outData,
                  builder: (context, snapshot) {
                    if (!snapshot.hasData) return Container();
                    return ListView(
                      padding: EdgeInsets.all(16),
                      children: <Widget>[
                        Center(
                            child: InkWell(
                          child: snapshot.data["image"] != null
                              ? SizedBox(
                                  width:
                                      MediaQuery.of(context).size.width * 0.55,
                                  height:
                                      MediaQuery.of(context).size.width * 0.55,
                                  child: Card(
                                    elevation: 8.0,
                                    clipBehavior: Clip.antiAlias,
                                    shape: RoundedRectangleBorder(
                                      borderRadius: BorderRadius.all(
                                          Radius.circular(15.0)),
                                    ),
                                    child: Hero(
                                      tag: snapshot.data["image"],
                                      child: CircleAvatar(
                                        child: snapshot.data["image"] is File
                                            ? Image.file(
                                                snapshot.data["image"],
                                                fit: BoxFit.cover,
                                              )
                                            : Image.network(
                                                snapshot.data["image"],
                                                fit: BoxFit.cover,
                                              ),
                                        backgroundColor: Colors.transparent,
                                      ),
                                    ),
                                  ),
                                )
                              : SizedBox(
                                  width:
                                      MediaQuery.of(context).size.width * 0.55,
                                  height:
                                      MediaQuery.of(context).size.width * 0.55,
                                  child: Card(
                                    elevation: 8.0,
                                    color: Colors.black38,
                                    clipBehavior: Clip.antiAlias,
                                    shape: RoundedRectangleBorder(
                                      borderRadius: BorderRadius.all(
                                          Radius.circular(15.0)),
                                    ),
                                    child: Icon(Icons.add_a_photo,
                                        size: 100.0, color: Colors.white),
                                  ),
                                ),
                          onTap: () {
                            showModalBottomSheet(
                                context: context,
                                builder: (context) => ImageSourceSheet(
                                      onImageSelected: (image) {
                                        setState(() {
                                          _partnerBloc.saveImage(image);
                                        });
                                        Navigator.of(context).pop();
                                      },
                                    ));
                          },
                        )),
                        SizedBox(height: 10.0),
                        TextFormField(
                          initialValue: snapshot.data["title"],
                          style: _fieldStyle,
                          decoration: _buildDecoration("Nome da Empresa"),
                          onSaved: _partnerBloc.saveTitle,
                          validator: validateTitle,
                        ),
                        SizedBox(height: 10.0),
                        TextFormField(
                          initialValue: snapshot.data["subTitle"],
                          style: _fieldStyle,
                          decoration: _buildDecoration("Tipo da Empresa"),
                          onSaved: _partnerBloc.saveSubTitle,
                          validator: validateSubTitle,
                        ),
                        SizedBox(height: 10.0),
                        TextFormField(
                          initialValue: snapshot.data["phone"],
                          style: _fieldStyle,
                          decoration: _buildDecoration("Telefone"),
                          onSaved: _partnerBloc.savePhone,
                          validator: validatePhone,
                        ),
                        SizedBox(height: 10.0),
                        TextFormField(
                          initialValue: snapshot.data["address"],
                          style: _fieldStyle,
                          decoration: _buildDecoration("Endereço"),
                          maxLines: 3,
                          onSaved: _partnerBloc.saveAddress,
                          validator: validateAddress,
                        ),
                        SizedBox(height: 10.0),
                        TextFormField(
                          initialValue: snapshot.data["description"],
                          style: _fieldStyle,
                          maxLines: 5,
                          decoration: _buildDecoration("Descrição da parceria"),
                          onSaved: _partnerBloc.saveDescription,
                          validator: validateDescription,
                        ),
                        SizedBox(
                          height: 16,
                        ),
                      ],
                    );
                  }),
            ),
            StreamBuilder<bool>(
                stream: _partnerBloc.outLoading,
                initialData: false,
                builder: (context, snapshot) {
                  return IgnorePointer(
                    ignoring: !snapshot.data,
                    child: Container(
                      color:
                          snapshot.data ? Colors.black54 : Colors.transparent,
                    ),
                  );
                }),
          ],
        ),
      ),
    );
  }

  void savePartner() async {
    if (_formKey.currentState.validate()) {
      _formKey.currentState.save();

      _scaffoldKey.currentState.showSnackBar(SnackBar(
        content: Text(
          "Salvando informações da parceria...",
          style: TextStyle(color: Colors.white),
        ),
        duration: Duration(minutes: 1),
        backgroundColor: Colors.green,
      ));

      bool success = await _partnerBloc.savePartner();

      _scaffoldKey.currentState.removeCurrentSnackBar();

      _scaffoldKey.currentState.showSnackBar(SnackBar(
        content: Text(
          success
              ? "As informações da parceria foram salvas!"
              : "Erro ao salvar as informações da parceria!",
          style: TextStyle(color: Colors.white),
        ),
        backgroundColor: success ? Colors.green : Colors.red,
      ));
    }
  }
}
