import 'dart:io';

import 'package:faen_app_admin/widgets/dialogs/confirm_adm_dialog.dart';
import 'package:faen_app_admin/widgets/image_source_sheet.dart';
import 'package:flutter/material.dart';
import 'package:flutter_masked_text/flutter_masked_text.dart';
import 'package:flutter_neumorphic/flutter_neumorphic.dart';

class AddUserScreen extends StatefulWidget {
  @override
  _AddUserScreenState createState() => _AddUserScreenState();
}

class _AddUserScreenState extends State<AddUserScreen> {
  // AddUserClass _addUserClass = AddUserClass();
  //Map<String, String> admData = {"email": "", "password": ""};
  Map<String, dynamic> userData = Map();

  final _nameController = TextEditingController();
  final _dateController = MaskedTextController(mask: "00/00/0000");
  final _phoneController = MaskedTextController(mask: "(00)0 0000-0000");
  final _cpfController = MaskedTextController(mask: "000.000.000-00");
  final _rgController = TextEditingController();
  final _rgOEController = TextEditingController();
  //final _courseController = TextEditingController();

  String _course;
  int _semester;
  String _typePay;
  String _typeUser;
  File image;

  final _semesterController = TextEditingController();
  final _emailController = TextEditingController();
  final _passController = TextEditingController();
  final _passConfirmController = TextEditingController();

  bool isObscure = true;
  bool _hasError = false;
  bool isAutoValid = false;
  bool _imageValidator = true;
  bool _allDocumentReceived = false;
  bool isLoading = false;

  final _formKey = GlobalKey<FormState>();
  final _scaffoldKey = GlobalKey<ScaffoldState>();

  final _nameFocus = FocusNode();
  final _dateFocus = FocusNode();
  final _cpfFocus = FocusNode();
  final _rgFocus = FocusNode();
  final _rgOEFocus = FocusNode();
  final _phoneFocus = FocusNode();
  final _emailFocus = FocusNode();
  final _passFocus = FocusNode();
  final _passConfirmFocus = FocusNode();

  static const courseOptions = <String>[
    'Agronomia',
    'Engenharia Agrícola',
    'Engenharia de Aquicultura',
    'Engenharia de Computação',
    'Engenharia de Energia',
    'Engenharia Civil',
    'Engenharia de Alimentos',
    'Engenharia Mecânica',
    'Engenharia de Produção',
    'Outros'
  ];

  static const payOptions = <String>[
    'Dinheiro',
    'Cartão',
    'Transferência',
  ];

  static const semesterOptions = <int>[1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12];

  static const typeUsersOptions = <String>[
    "Associado",
    "Não Associado",
    "Administrador"
  ];

  final List<DropdownMenuItem<String>> _dropDownMenuItems = courseOptions
      .map((String value) => DropdownMenuItem(
            value: value,
            child: Text(value, textAlign: TextAlign.center),
          ))
      .toList();

  final List<DropdownMenuItem<int>> _dropDownMenuItems2 = semesterOptions
      .map((int value) => DropdownMenuItem(
            value: value,
            child: Text("$value"),
          ))
      .toList();

  final List<DropdownMenuItem<String>> _dropDownMenuItems3 = payOptions
      .map((String value) => DropdownMenuItem(
            value: value,
            child: Text(value),
          ))
      .toList();

  final List<DropdownMenuItem<String>> _dropDownMenuItems4 = typeUsersOptions
      .map((String value) => DropdownMenuItem(
            value: value,
            child: Text(
              value, /*style: TextStyle(color: Theme.of(context).primaryColor)*/
            ),
          ))
      .toList();

  @override
  void initState() {
    super.initState();
    /*WidgetsBinding.instance
        .addPostFrameCallback((_) => showAlertDialog(context));*/
  }

  @override
  void dispose() {
    super.dispose();
    //_addUserClass.dispose();
    _emailController.dispose();
    _nameController.dispose();
    _dateController.dispose();
    _cpfController.dispose();
    //_courseController.dispose();
    _semesterController.dispose();
    _phoneController.dispose();
    _passController.dispose();
    _passConfirmController.dispose();
    _rgController.dispose();
    _rgOEController.dispose();
    _emailFocus.dispose();
    _passFocus.dispose();
    _passConfirmFocus.dispose();
    _nameFocus.dispose();
    _dateFocus.dispose();
    _phoneFocus.dispose();
    _cpfFocus.dispose();
    _rgFocus.dispose();
    _rgOEFocus.dispose();
  }

  @override
  Widget build(BuildContext context) {
    //final _userBloc = BlocProvider.of<UserBloc>(context);
    return Container(
      decoration: BoxDecoration(
          image: DecorationImage(
              image: ExactAssetImage("images/Fundo2.png"), fit: BoxFit.cover)),
      child: Scaffold(
          key: _scaffoldKey,
          appBar: AppBar(
            backgroundColor: Theme.of(context).primaryColor.withOpacity(0.50),
            title: Text("Adicionar Novo Usuário"),
            centerTitle: true,
          ),
          // backgroundColor: Colors.grey[850],
          body: isLoading
              ? Center(
                  child: Container(
                  //height: 200,
                  //width: 200,
                  child: CircularProgressIndicator(
                    valueColor: AlwaysStoppedAnimation(Colors.white),
                  ),
                  //child: FlareActor("animations/Carregar.flr", animation: "infinito"),
                ))
              : Form(
                  key: _formKey,
                  child: ListView(
                    //padding: EdgeInsets.all(16.0),
                    children: <Widget>[
                      Card(
                        elevation: 7.0,
                        shape: RoundedRectangleBorder(
                          side: BorderSide(color: Colors.grey[800]),
                          borderRadius: BorderRadius.all(Radius.circular(15.0)),
                        ),
                        margin: EdgeInsets.symmetric(
                            horizontal: 15.0, vertical: 20.0),
                        color: Colors.black12,
                        child: Padding(
                          padding: const EdgeInsets.all(16.0),
                          child: Column(
                            //crossAxisAlignment: CrossAxisAlignment.center,
                            mainAxisSize: MainAxisSize.min,
                            children: <Widget>[
                              Center(
                                  child: InkWell(
                                child: Container(
                                  width: 140.0,
                                  height: 140.0,
                                  decoration: BoxDecoration(
                                    color: Theme.of(context)
                                        .primaryColor
                                        .withOpacity(0.50),
                                    shape: BoxShape.circle,
                                    image: image != null
                                        ? DecorationImage(
                                            image: FileImage(image),
                                            fit: BoxFit.cover)
                                        : null,
                                  ),
                                  child: image == null
                                      ? Icon(
                                          Icons.add_a_photo,
                                          color: _imageValidator
                                              ? Colors.white70
                                              : Colors.red[900],
                                          size: 88.0,
                                        )
                                      : null,
                                ),
                                //AssetImage("images/person.png")
                                onTap: () {
                                  showModalBottomSheet(
                                      context: context,
                                      builder: (context) => ImageSourceSheet(
                                            onImageSelected: (image) {
                                              setState(() {
                                                this.image = image;
                                                _imageValidator = true;
                                              });
                                              Navigator.of(context).pop();
                                            },
                                          ));
                                },
                              )),
                              Text(
                                image == null
                                    ? "Insira a foto do usuário"
                                    : "Perfeito !",
                                textAlign: TextAlign.center,
                                style: TextStyle(
                                    color: Colors.white70, fontSize: 16.0),
                              ),
                              SizedBox(height: 16.0),
                              _inputFormField(
                                  label: "Nome Completo",
                                  controller: _nameController,
                                  textValidator: "Insira o nome do usuário !",
                                  focus: _nameFocus,
                                  showCursor: true,
                                  typeInput: TextInputType.text),

                              SizedBox(height: 16.0),
                              _inputFormField(
                                  label: "Data de Nascimento",
                                  controller: _dateController,
                                  focus: _dateFocus,
                                  showCursor: false,
                                  textValidator:
                                      "Insira a data de nascimento !",
                                  hintText: "23/09/2000",
                                  typeInput: TextInputType.datetime),
                              SizedBox(height: 16.0),
                              //Text("Digite seu CPF"),
                              _inputFormField(
                                  label: "CPF",
                                  controller: _cpfController,
                                  hintText: "123.456.789-00",
                                  textValidator: "Insira o CPF !",
                                  typeInput: TextInputType.number,
                                  focus: _cpfFocus,
                                  showCursor: false),

                              SizedBox(height: 16.0),
                              _inputFormField(
                                  label: "Telefone",
                                  controller: _phoneController,
                                  hintText: "(67)9 9999-9999",
                                  textValidator:
                                      "Insira o número de telefone !",
                                  typeInput: TextInputType.phone,
                                  focus: _phoneFocus,
                                  showCursor: false),
                              SizedBox(height: 16.0),
                              Row(
                                mainAxisAlignment:
                                    MainAxisAlignment.spaceEvenly,
                                children: <Widget>[
                                  SizedBox(
                                    //height: 150,
                                    width: MediaQuery.of(context).size.width *
                                        0.45,
                                    child: _inputFormField(
                                        label: "RG",
                                        controller: _rgController,
                                        focus: _rgFocus,
                                        textValidator: "Insira o número do RG",
                                        typeInput:
                                            TextInputType.numberWithOptions(),
                                        showCursor: true),
                                  ),
                                  SizedBox(
                                    width: 5,
                                  ),
                                  SizedBox(
                                    width: MediaQuery.of(context).size.width *
                                        0.30,
                                    child: _inputFormField(
                                        label: "Orgão\nExpedidor",
                                        controller: _rgOEController,
                                        focus: _rgOEFocus,
                                        showCursor: true,
                                        textValidator:
                                            "É necessário inserir o Orgão Expedidor"),
                                  ),
                                ],
                              ),
                              SizedBox(height: 16.0),
                              Card(
                                margin: EdgeInsets.zero,
                                shape: OutlineInputBorder(
                                    borderSide: BorderSide(
                                      color: Colors.white54,
                                    ),
                                    borderRadius: BorderRadius.circular(12.5)),
                                color: Colors.white38,
                                child: ListTile(
                                  //dense: true,
                                  contentPadding: EdgeInsets.zero,
                                  title: Padding(
                                    padding: const EdgeInsets.only(left: 16.0),
                                    child: Text("Tipo de usuário: ",
                                        style: TextStyle(
                                            color: Colors.white70,
                                            fontSize: 16.0)),
                                  ),
                                  trailing: Padding(
                                    padding: const EdgeInsets.only(
                                        right: 2.0, left: 2.0),
                                    child: DropdownButton(
                                      value: _typeUser,
                                      // style: TextStyle(color: Colors.green[700]),
                                      items: _dropDownMenuItems4,
                                      onChanged: ((newValue) {
                                        setState(() {
                                          _typeUser = newValue;
                                        });
                                      }),
                                    ),
                                  ),
                                ),
                              ),
                              SizedBox(height: 16.0),
                              Card(
                                margin: EdgeInsets.zero,
                                shape: OutlineInputBorder(
                                    borderSide: BorderSide(
                                      color: Colors.white54,
                                    ),
                                    borderRadius: BorderRadius.circular(12.5)),
                                color: Colors.white38,
                                child: ListTile(
                                  //dense: true,
                                  //contentPadding: EdgeInsets.zero,
                                  contentPadding:
                                      EdgeInsets.symmetric(horizontal: 2.0),
                                  title: Padding(
                                    padding: const EdgeInsets.only(left: 16.0),
                                    child: Text(
                                      "Defina seu curso:",
                                      style: TextStyle(color: Colors.white70),
                                      softWrap: true,
                                    ),
                                  ),
                                  trailing: Padding(
                                    padding: const EdgeInsets.only(
                                        right: 2.0, left: 2.0),
                                    child: DropdownButton(
                                      value: _course,
                                      hint: Text("Escolha seu curso"),
                                      /*style: TextStyle(
                                    color: Theme.of(context).primaryColor,
                                  ),*/
                                      items: _dropDownMenuItems,
                                      onChanged: ((newValue) {
                                        setState(() {
                                          _course = newValue;
                                        });
                                      }),
                                    ),
                                  ),
                                ),
                              ),
                              SizedBox(height: 16.0),
                              Card(
                                margin: EdgeInsets.zero,
                                shape: OutlineInputBorder(
                                    borderSide: BorderSide(
                                      color: Colors.white54,
                                    ),
                                    borderRadius: BorderRadius.circular(12.5)),
                                color: Colors.white38,
                                child: ListTile(
                                  contentPadding: EdgeInsets.zero,
                                  title: Padding(
                                    padding: const EdgeInsets.only(left: 16.0),
                                    child: Text(
                                      "Escolha seu semestre atual:",
                                      style: TextStyle(color: Colors.white70),
                                      softWrap: true,
                                    ),
                                  ),
                                  trailing: Padding(
                                    padding: const EdgeInsets.only(
                                        right: 2.0, left: 2.0),
                                    child: DropdownButton(
                                      value: _semester,
                                      //hint: Text("Escolha seu curso"),
                                      items: _dropDownMenuItems2,
                                      onChanged: ((newValue) {
                                        setState(() {
                                          _semester = newValue;
                                        });
                                      }),
                                    ),
                                  ),
                                ),
                              ),
                              SizedBox(height: 16.0),
                              Card(
                                margin: EdgeInsets.zero,
                                shape: OutlineInputBorder(
                                    borderSide: BorderSide(
                                      color: Colors.white54,
                                    ),
                                    borderRadius: BorderRadius.circular(12.5)),
                                color: Colors.white38,
                                child: ListTile(
                                  contentPadding: EdgeInsets.zero,
                                  title: Padding(
                                    padding: const EdgeInsets.only(left: 16.0),
                                    child: Text(
                                      "Escolha uma forma de pagamento:",
                                      softWrap: true,
                                      style: TextStyle(color: Colors.white70),
                                    ),
                                  ),
                                  trailing: Padding(
                                    padding: const EdgeInsets.only(
                                        right: 2.0, left: 2.0),
                                    child: DropdownButton(
                                      value: _typePay,
                                      //hint: Text("Escolha seu curso"),
                                      items: _dropDownMenuItems3,
                                      onChanged: ((newValue) {
                                        setState(() {
                                          _typePay = newValue;
                                        });
                                      }),
                                    ),
                                  ),
                                ),
                              ),
                              SizedBox(height: 16.0),
                              Card(
                                margin: EdgeInsets.zero,
                                shape: OutlineInputBorder(
                                    borderSide: BorderSide(
                                      color: Colors.white54,
                                    ),
                                    borderRadius: BorderRadius.circular(12.5)),
                                color: Colors.white38,
                                child: ListTile(
                                  //dense: true,
                                  contentPadding: EdgeInsets.zero,
                                  title: Padding(
                                    padding: const EdgeInsets.only(left: 16.0),
                                    child: Text("Documentos entregues",
                                        textAlign: TextAlign.start,
                                        style: TextStyle(
                                            color: Colors.white70,
                                            fontSize: 16.0)),
                                  ),
                                  trailing: NeumorphicCheckbox(
                                      margin: EdgeInsets.all(8.0),
                                      padding: EdgeInsets.all(8.0),
                                      style: NeumorphicCheckboxStyle(
                                        selectedColor: Colors.green[900],
                                        disabledColor: Colors.black,
                                      ),
                                      value: _allDocumentReceived,
                                      onChanged: (value) {
                                        setState(() {
                                          _allDocumentReceived =
                                              !_allDocumentReceived;
                                        });
                                      }),
                                  /*Checkbox(
                                    checkColor: Colors.white,
                                    activeColor: Colors.green[700],
                                    value: _allDocumentReceived,
                                    onChanged: (_) {
                                      setState(() {
                                        _allDocumentReceived =
                                            !_allDocumentReceived;
                                      });
                                    },
                                  ),*/
                                ),
                              ),
                              SizedBox(height: 16.0),
                              TextFormField(
                                controller: _emailController,
                                focusNode: _emailFocus,
                                decoration: InputDecoration(
                                  filled: true,
                                  fillColor: Colors.white38,
                                  labelText: "Email",
                                  enabledBorder: OutlineInputBorder(
                                      borderSide: BorderSide(
                                        color: Colors.white60,
                                      ),
                                      borderRadius:
                                          BorderRadius.circular(12.5)),
                                  focusedBorder: OutlineInputBorder(
                                      borderSide: BorderSide(
                                        color: Colors.green[900],
                                      ),
                                      borderRadius:
                                          BorderRadius.circular(12.5)),
                                  errorBorder: OutlineInputBorder(
                                      borderSide: BorderSide(
                                        color: Colors.red[900],
                                      ),
                                      borderRadius:
                                          BorderRadius.circular(12.5)),
                                  focusedErrorBorder: OutlineInputBorder(
                                      borderSide: BorderSide(
                                        color: Colors.red[900],
                                      ),
                                      borderRadius:
                                          BorderRadius.circular(12.5)),
                                ),
                                keyboardType: TextInputType.emailAddress,
                                onFieldSubmitted: (text) {
                                  return FocusScope.of(context)
                                      .requestFocus(_passFocus);
                                },
                                validator: (text) {
                                  if (text.isEmpty || !text.contains("@")) {
                                    FocusScope.of(context)
                                        .requestFocus(_emailFocus);
                                    return "Email inválido !";
                                  } else
                                    return null;
                                },
                              ),
                              SizedBox(height: 16.0),
                              TextFormField(
                                controller: _passController,
                                focusNode: _passFocus,
                                decoration: InputDecoration(
                                  filled: true,
                                  fillColor: Colors.white38,
                                  suffixIcon: IconButton(
                                    icon: isObscure
                                        ? Icon(Icons.visibility_off)
                                        : Icon(Icons.visibility),
                                    onPressed: () {
                                      setState(() {
                                        isObscure = !isObscure;
                                      });
                                    },
                                  ),
                                  labelText: "Senha",
                                  enabledBorder: OutlineInputBorder(
                                      borderSide: BorderSide(
                                        color: Colors.white60,
                                      ),
                                      borderRadius:
                                          BorderRadius.circular(12.5)),
                                  focusedBorder: OutlineInputBorder(
                                      borderSide: BorderSide(
                                        color: Colors.green[900],
                                      ),
                                      borderRadius:
                                          BorderRadius.circular(12.5)),
                                  errorBorder: OutlineInputBorder(
                                      borderSide: BorderSide(
                                        color: Colors.red[900],
                                      ),
                                      borderRadius:
                                          BorderRadius.circular(12.5)),
                                  focusedErrorBorder: OutlineInputBorder(
                                      borderSide: BorderSide(
                                        color: Colors.red[900],
                                      ),
                                      borderRadius:
                                          BorderRadius.circular(12.5)),
                                ),
                                obscureText: isObscure,
                                onFieldSubmitted: (text) {
                                  setState(() {
                                    isAutoValid = true;
                                  });
                                  return FocusScope.of(context)
                                      .requestFocus(_passConfirmFocus);
                                },
                                validator: (text) {
                                  if (text.isEmpty || text.length < 6) {
                                    FocusScope.of(context)
                                        .requestFocus(_passFocus);
                                    return "Senha inválida !";
                                  } else
                                    return null;
                                },
                              ),
                              SizedBox(height: 16.0),
                              TextFormField(
                                controller: _passConfirmController,
                                focusNode: _passConfirmFocus,
                                autovalidate: isAutoValid,
                                decoration: InputDecoration(
                                  filled: true,
                                  fillColor: Colors.white38,
                                  suffixIcon: IconButton(
                                    icon: isObscure
                                        ? Icon(Icons.visibility_off)
                                        : Icon(Icons.visibility),
                                    onPressed: () {
                                      setState(() {
                                        isObscure = !isObscure;
                                      });
                                    },
                                  ),
                                  labelText: "Confirme sua senha",
                                  enabledBorder: OutlineInputBorder(
                                      borderSide: BorderSide(
                                        color: Colors.white54,
                                      ),
                                      borderRadius:
                                          BorderRadius.circular(12.5)),
                                  focusedBorder: OutlineInputBorder(
                                      borderSide: BorderSide(
                                        color: Colors.green[900],
                                      ),
                                      borderRadius:
                                          BorderRadius.circular(12.5)),
                                  errorBorder: OutlineInputBorder(
                                      borderSide: BorderSide(
                                        color: Colors.red[900],
                                      ),
                                      borderRadius:
                                          BorderRadius.circular(12.5)),
                                  focusedErrorBorder: OutlineInputBorder(
                                      borderSide: BorderSide(
                                        color: Colors.red[900],
                                      ),
                                      borderRadius:
                                          BorderRadius.circular(12.5)),
                                ),
                                obscureText: isObscure,
                                validator: (text) {
                                  if (_passController.text != text) {
                                    FocusScope.of(context)
                                        .requestFocus(_passConfirmFocus);
                                    return "O valor digitado não é igual ao anterior";
                                  } else
                                    return null;
                                },
                              ),
                              SizedBox(height: 16.0),
                              Visibility(
                                visible: !_imageValidator,
                                child: Text(
                                  "Por favor, volte ao início do formulário e adicione uma foto de perfil !",
                                  textAlign: TextAlign.center,
                                  style: TextStyle(
                                    color: Colors.red[900],
                                  ),
                                  softWrap: true,
                                ),
                              ),
                              SizedBox(height: 16.0),
                              SizedBox(
                                height: 44.0,
                                width: MediaQuery.of(context).size.width * 0.5,
                                child: RaisedButton(
                                  padding: EdgeInsets.zero,
                                  shape: OutlineInputBorder(
                                    borderSide: BorderSide(
                                        color: !_hasError
                                            ? Color.fromARGB(122, 4, 64, 0)
                                            : Colors.red[900]),
                                    borderRadius:
                                        BorderRadius.all(Radius.circular(12.5)),
                                  ),
                                  child: Text(
                                    "Criar Conta",
                                    style: TextStyle(fontSize: 18.0),
                                  ),
                                  textColor: Colors.white,
                                  color: Theme.of(context).primaryColor,
                                  onPressed: () {
                                    if (_formKey.currentState.validate() &&
                                        image != null) {
                                      userData["name"] = _nameController.text;
                                      userData["photo"] = image;
                                      userData["cpf"] = _cpfController.text;
                                      userData["rg"] = _rgController.text;
                                      userData["rgOE"] = _rgOEController.text;
                                      userData["email"] = _emailController.text;
                                      userData["date"] = _dateController.text;
                                      userData["phone"] = _phoneController.text;
                                      userData["course"] = _course;
                                      userData["semester"] = _semester;
                                      userData["typeUser"] = _typeUser;
                                      userData["typePay"] = _typePay;
                                      userData["allDocumentReceived"] =
                                          _allDocumentReceived;

                                      //adicionar outras informações necessárias
                                      /*setState(() {
                                        isLoading = true;
                                        _onLoading();
                                      });*/
                                      showDialog(
                                          context: context,
                                          builder: (context) =>
                                              ConfirmAdmDialog(userData,
                                                  _passController.text));
                                      /* _addUserClass.signUp(
                                        admData: admData,
                                          userData: userData,
                                          pass: _passController.text,
                                          onSuccess: _onSuccess,
                                          onFail: _onFail);*/
                                    } else {
                                      _imageValidator =
                                          image == null ? false : true;
                                      _hasError = true;
                                      setState(() {
                                        isLoading = false;
                                      });
                                    }
                                  },
                                ),
                              ),
                              //_errorCard(),
                            ],
                          ),
                        ),
                      ),
                    ],
                  ),
                )),
    );
  }

  Widget _inputFormField(
      {@required String label,
      @required TextEditingController controller,
      String textValidator,
      String hintText,
      @required bool showCursor,
      TextInputType typeInput,
      @required FocusNode focus}) {
    return TextFormField(
      enableSuggestions: true,
      showCursor: showCursor,
      focusNode: focus,
      keyboardType: typeInput,
      controller: controller,
      decoration: InputDecoration(
        filled: true,
        fillColor: Colors.white38,
        labelText: label,
        hintText: hintText ?? "",
        enabledBorder: OutlineInputBorder(
            borderSide: BorderSide(
              color: Colors.white60,
            ),
            borderRadius: BorderRadius.circular(12.5)),
        focusedBorder: OutlineInputBorder(
            borderSide: BorderSide(
              color: Colors.green[900],
            ),
            borderRadius: BorderRadius.circular(12.5)),
        focusedErrorBorder: OutlineInputBorder(
            borderSide: BorderSide(
              color: Colors.red[900],
            ),
            borderRadius: BorderRadius.circular(12.5)),
        errorBorder: OutlineInputBorder(
            borderSide: BorderSide(
              color: Colors.red[900],
            ),
            borderRadius: BorderRadius.circular(12.5)),
      ),
      validator: (text) {
        if (text.isEmpty) {
          FocusScope.of(context).requestFocus(focus);
          return textValidator;
        } else
          return null;
      },
    );
  }

  /*Widget showConfirmAdmDialog() {
    return Dialog(
        child: Card(
      child: Padding(
        padding: const EdgeInsets.all(8.0),
        child: Column(
          mainAxisSize: MainAxisSize.min,
          children: <Widget>[
            inputDialogField(Icons.person, "email", false),
            inputDialogField(Icons.lock, "senha", true),
            Row(
              mainAxisSize: MainAxisSize.min,
              mainAxisAlignment: MainAxisAlignment.end,
              children: <Widget>[
                FlatButton(
                  child: Text("Cancelar"),
                  onPressed: () {
                    Navigator.of(context).pop();
                  },
                ),
                FlatButton(
                  child: Text("Confirmar"),
                  onPressed: () {
                    setState(() {
                      isLoading = true;
                      _onLoading();
                    });
                    _addUserClass.signUp(
                        admData: admData,
                        userData: userData,
                        pass: _passController.text,
                        onSuccess: _onSuccess,
                        onFail: _onFail);
                  },
                ),
              ],
            ),
          ],
        ),
      ),
    ));
  }*/

  /*void _onLoading() {
    _scaffoldKey.currentState.showSnackBar(SnackBar(
      content: Text(
        "Criando usuário...",
        style: TextStyle(color: Colors.white),
      ),
      duration: Duration(minutes: 1),
      backgroundColor: Theme.of(context).primaryColor,
    ));
  }*/

  /*void _onSuccess() {
    setState(() {
      isLoading = false;
    });
    _scaffoldKey.currentState.removeCurrentSnackBar();
    _scaffoldKey.currentState.showSnackBar(
      SnackBar(
        content: Text("Usuário criado com sucesso !"),
        backgroundColor: Colors.green,
        duration: Duration(seconds: 2),
      ),
    );
    Future.delayed(Duration(seconds: 2)).then((_) {
      Navigator.of(context).pop();
      Navigator.of(context).pushReplacement(
          MaterialPageRoute(builder: (context) => HomeScreen()));
      Navigator.of(context)
          .push(MaterialPageRoute(builder: (context) => LoginScreen()));
    });
  }

  void _onFail() {
    _scaffoldKey.currentState.removeCurrentSnackBar();
    _scaffoldKey.currentState.showSnackBar(SnackBar(
      content: Text("Falha ao criar o usuário !"),
      backgroundColor: Colors.redAccent,
      duration: Duration(seconds: 2),
    ));
  }*/

  showAlertDialog(BuildContext context) {
    Widget botaoContinuar = FlatButton(
      color: Colors.transparent,
      child: Text(
        "Entendi!",
        style: TextStyle(color: Theme.of(context).primaryColor),
      ),
      onPressed: () {
        Navigator.pop(context);
      },
    );
    showDialog(
      context: context,
      builder: (BuildContext context) {
        return AlertDialog(
            elevation: 2.0,
            shape:
                OutlineInputBorder(borderRadius: BorderRadius.circular(12.5)),
            title: Text(
              "Aviso importante!",
              textAlign: TextAlign.center,
            ),
            content: Text(
              "Após a adição de novo(s) usuário(s) será necessário a revalidação dos seus privilégios, para continuar com o pleno funcionamento de todas as funcionalidades do app.",
              textAlign: TextAlign.justify,
            ),
            actions: [botaoContinuar]);
      },
    );
  }

  Widget inputDialogField(
    IconData icon,
    String label,
    bool obscure,
  ) {
    return TextField(
      onChanged: (text) {},
      decoration: InputDecoration(
        prefixIcon: Icon(
          icon,
          color: Colors.white70,
        ),
        labelText: label,
        labelStyle: TextStyle(color: Colors.white70),
        enabledBorder: OutlineInputBorder(
            borderSide: BorderSide(
              color: Colors.white60,
            ),
            borderRadius: BorderRadius.circular(12.5)),
        focusedBorder: OutlineInputBorder(
            borderSide: BorderSide(
              color: Theme.of(context).primaryColor,
            ),
            borderRadius: BorderRadius.circular(12.5)),
        focusedErrorBorder: OutlineInputBorder(
            borderSide: BorderSide(
              color: Colors.red[900],
            ),
            borderRadius: BorderRadius.circular(12.5)),
        errorBorder: OutlineInputBorder(
            borderSide: BorderSide(
              color: Colors.red[900],
            ),
            borderRadius: BorderRadius.circular(12.5)),
      ),
      style: TextStyle(color: Colors.white),
      obscureText: obscure,
    );
  }
}
