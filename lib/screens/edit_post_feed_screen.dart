import 'dart:io';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:faen_app_admin/blocs/post_feed_bloc.dart';
import 'package:faen_app_admin/validators/post_feed_validator.dart';
import 'package:faen_app_admin/widgets/image_source_sheet.dart';
import 'package:flutter/material.dart';
import 'package:flutter_markdown/flutter_markdown.dart';
import 'package:flutter_neumorphic/flutter_neumorphic.dart';
import 'package:image_cropper/image_cropper.dart';

class EditPostFeedScreen extends StatefulWidget {
  final DocumentSnapshot postFeed;

  EditPostFeedScreen({this.postFeed});

  @override
  _EditPostFeedScreenState createState() => _EditPostFeedScreenState(postFeed);
}

class _EditPostFeedScreenState extends State<EditPostFeedScreen>
    with PostFeedValidator {
  final PostFeedBloc _postFeedBloc;

  final _formKey = GlobalKey<FormState>();
  final _scaffoldKey = GlobalKey<ScaffoldState>();

  final controller = ScrollController();

  _EditPostFeedScreenState(DocumentSnapshot postFeed)
      : _postFeedBloc = PostFeedBloc(postFeed: postFeed);

  @override
  Widget build(BuildContext context) {
    InputDecoration _buildDecoration(String label) {
      return InputDecoration(
          filled: true,
          fillColor: Colors.white10,
          border: OutlineInputBorder(borderRadius: BorderRadius.circular(15.0)),
          enabledBorder: OutlineInputBorder(
              borderSide: BorderSide(
                color: Colors.white70,
              ),
              borderRadius: BorderRadius.circular(15.0)),
          focusedBorder: OutlineInputBorder(
              borderSide: BorderSide(
                color: Colors.green[900],
              ),
              borderRadius: BorderRadius.circular(15.0)),
          focusedErrorBorder: OutlineInputBorder(
              borderSide: BorderSide(
                color: Colors.red[900],
              ),
              borderRadius: BorderRadius.circular(15.0)),
          errorBorder: OutlineInputBorder(
              borderSide: BorderSide(
                color: Colors.red[900],
              ),
              borderRadius: BorderRadius.circular(15.0)),
          labelText: label,
          labelStyle:
              TextStyle(fontFamily: 'BebasNeue', color: Colors.white70));
    }

    final _fieldStyle =
        TextStyle(fontFamily: 'Lato', color: Colors.white, fontSize: 16);

    return Container(
      decoration: BoxDecoration(
          image: DecorationImage(
              image: ExactAssetImage("images/Fundo2.png"), fit: BoxFit.cover)),
      child: Scaffold(
        key: _scaffoldKey,
        // backgroundColor: Colors.grey[850],
        appBar: AppBar(
          elevation: 0,
          centerTitle: true,
          title: StreamBuilder<bool>(
              stream: _postFeedBloc.outCreated,
              initialData: false,
              builder: (context, snapshot) {
                return Text(snapshot.data ? "Editar Post" : "Criar Novo Post");
              }),
          actions: <Widget>[
            StreamBuilder<bool>(
              stream: _postFeedBloc.outCreated,
              initialData: false,
              builder: (context, snapshot) {
                if (snapshot.data)
                  return StreamBuilder<bool>(
                      stream: _postFeedBloc.outLoading,
                      initialData: false,
                      builder: (context, snapshot) {
                        return IconButton(
                          icon: Icon(Icons.delete_outline),
                          onPressed: snapshot.data
                              ? null
                              : () {
                                  _postFeedBloc.deletePost();
                                  Navigator.of(context).pop();
                                },
                        );
                      });
                else
                  return Container();
              },
            ),
            StreamBuilder<bool>(
                stream: _postFeedBloc.outLoading,
                initialData: false,
                builder: (context, snapshot) {
                  return IconButton(
                    icon: Icon(Icons.save),
                    onPressed: snapshot.data ? null : savePostFeed,
                  );
                }),
          ],
        ),
        body: Stack(
          children: <Widget>[
            Form(
              key: _formKey,
              child: StreamBuilder<Map>(
                  stream: _postFeedBloc.outData,
                  builder: (context, snapshot) {
                    if (!snapshot.hasData) return Container();
                    return ListView(
                      padding: EdgeInsets.all(16),
                      children: <Widget>[
                        Center(
                            child: InkWell(
                          child: snapshot.data["image"] != null
                              ? SizedBox(
                                  height: (MediaQuery.of(context).size.width *
                                          0.98) /
                                      2,
                                  width:
                                      MediaQuery.of(context).size.width * 0.98,
                                  child: Neumorphic(
                                    margin: EdgeInsets.zero,
                                    padding: const EdgeInsets.only(
                                        right: 5.0,
                                        left: 5.0,
                                        top: 5.0,
                                        bottom: 10),
                                    drawSurfaceAboveChild: true,
                                    boxShape: NeumorphicBoxShape.roundRect(
                                      BorderRadius.circular(12.0),
                                    ),
                                    style: NeumorphicStyle(
                                      depth: -10.0,
                                      intensity: 0.3,
                                      lightSource: DateTime.now().hour % 2 == 0
                                          ? LightSource.topLeft
                                          : LightSource.topRight,
                                      color: Colors.black87,
                                    ),
                                    child: CircleAvatar(
                                      child: snapshot.data["image"] is File
                                          ? Image.file(
                                              snapshot.data["image"],
                                              fit: BoxFit.cover,
                                            )
                                          : Image.network(
                                              snapshot.data["image"],
                                              fit: BoxFit.cover,
                                            ),
                                      //backgroundColor: Colors.transparent,
                                    ),
                                  ),
                                )
                              : Container(
                                  width: 200.0,
                                  height: 200.0,
                                  decoration: BoxDecoration(
                                    shape: BoxShape.circle,
                                  ),
                                  child: Icon(Icons.add_a_photo,
                                      size: 100.0, color: Colors.white30),
                                ),
                          onTap: () {
                            showModalBottomSheet(
                                context: context,
                                builder: (context) => ImageSourceSheet(
                                      cropAspectRatio: CropAspectRatio(
                                          ratioX: 2.0, ratioY: 1.0),
                                      onImageSelected: (image) {
                                        setState(() {
                                          _postFeedBloc.saveImage(image);
                                        });
                                        Navigator.of(context).pop();
                                      },
                                    ));
                          },
                        )),
                        SizedBox(height: 10.0),
                        TextFormField(
                          initialValue: snapshot.data["title"],
                          style: _fieldStyle,
                          decoration: _buildDecoration("Título"),
                          onSaved: _postFeedBloc.saveTitle,
                          validator: validateTitle,
                        ),
                        SizedBox(height: 10.0),
                        TextFormField(
                          initialValue: snapshot.data["subTitle"],
                          style: _fieldStyle,
                          decoration: _buildDecoration("Subtítulo"),
                          onSaved: _postFeedBloc.saveSubTitle,
                          validator: validateSubTitle,
                        ),
                        SizedBox(height: 10.0),
                        TextFormField(
                          initialValue: snapshot.data["description"],
                          style: _fieldStyle,
                          maxLines: 5,
                          decoration: _buildDecoration("Descrição do post"),
                          onSaved: _postFeedBloc.saveDescription,
                          validator: validateDescription,
                          onChanged: _postFeedBloc.updateMarkdownData,
                        ),
                        SizedBox(height: 10.0),
                        Text(
                          "Visualização com Markdown: ",
                          style: TextStyle(
                              fontFamily: 'BebasNeue',
                              color: Colors.white70,
                              //  fontWeight: FontWeight.bold,
                              fontSize: 16.0),
                        ),
                        SizedBox(height: 4.0),
                        StreamBuilder<String>(
                            stream: _postFeedBloc.outMarkdownData,
                            builder: (context, snapshot) {
                              return _buildMarkdown(snapshot.data ?? "");
                            }),
                        SizedBox(
                          height: 16,
                        ),
                      ],
                    );
                  }),
            ),
            StreamBuilder<bool>(
                stream: _postFeedBloc.outLoading,
                initialData: false,
                builder: (context, snapshot) {
                  return IgnorePointer(
                    ignoring: !snapshot.data,
                    child: Container(
                      color:
                          snapshot.data ? Colors.black54 : Colors.transparent,
                    ),
                  );
                }),
          ],
        ),
      ),
    );
  }

  Widget _buildMarkdown(String data) {
    return Neumorphic(
      margin: EdgeInsets.zero,
      padding:
          const EdgeInsets.only(right: 5.0, left: 5.0, top: 5.0, bottom: 10),
      drawSurfaceAboveChild: true,
      boxShape: NeumorphicBoxShape.roundRect(
        BorderRadius.circular(12.0),
      ),
      style: NeumorphicStyle(
        depth: -10.0,
        intensity: 0.3,
        lightSource: DateTime.now().hour % 2 == 0
            ? LightSource.topLeft
            : LightSource.topRight,
        color: Colors.white10,
      ),
      child: Theme(
        data: Theme.of(context).copyWith(
            canvasColor: Color.fromARGB(255, 4, 64, 0),
            primaryColor: Colors.white,
            textTheme: Theme.of(context).textTheme.copyWith(
                bodyText1: TextStyle(
                    color: Colors.white, fontFamily: 'Lato', fontSize: 16),
                bodyText2: TextStyle(
                    color: Colors.white, fontFamily: 'Lato', fontSize: 16),
                headline2: TextStyle(
                    color: Colors.white, fontFamily: 'Lato', fontSize: 16),
                headline3: TextStyle(
                    color: Colors.white, fontFamily: 'Lato', fontSize: 16),
                headline4: TextStyle(
                    color: Colors.white, fontFamily: 'Lato', fontSize: 16),
                headline5: TextStyle(
                    color: Colors.white, fontFamily: 'Lato', fontSize: 16),
                headline6: TextStyle(
                    color: Colors.white, fontFamily: 'Lato', fontSize: 16),
                overline: TextStyle(
                    color: Colors.white, fontFamily: 'Lato', fontSize: 16),
                subtitle2: TextStyle(
                    color: Colors.white, fontFamily: 'Lato', fontSize: 16),
                caption: TextStyle(color: Colors.white54))),
        child: Markdown(
          controller: controller,
          shrinkWrap: true,
          selectable: true,
          data: data,
          styleSheetTheme: MarkdownStyleSheetBaseTheme.material,
          /*imageDirectory:
                                    'https://raw.githubusercontent.com',*/
        ),
      ),
    );
  }

  void savePostFeed() async {
    if (_formKey.currentState.validate()) {
      _formKey.currentState.save();

      _scaffoldKey.currentState.showSnackBar(SnackBar(
        content: Text(
          "Salvando informações do post ...",
          style: TextStyle(color: Colors.white),
        ),
        duration: Duration(minutes: 1),
        backgroundColor: Colors.green,
      ));

      bool success = await _postFeedBloc.savePostFeed();

      _scaffoldKey.currentState.removeCurrentSnackBar();

      _scaffoldKey.currentState.showSnackBar(SnackBar(
        content: Text(
          success
              ? "O post foi publicado com sucesso !"
              : "Erro ao publicar o Post !",
          style: TextStyle(color: Colors.white),
        ),
        backgroundColor: success ? Colors.green : Colors.red,
      ));
    }
  }
}
