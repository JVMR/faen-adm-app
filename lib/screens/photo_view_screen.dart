import 'package:flutter/material.dart';
import 'package:photo_view/photo_view.dart';

class PhotoViewScreen extends StatefulWidget {
  final String urlImage;
  final String userName;

  PhotoViewScreen(this.urlImage, this.userName);

  @override
  _PhotoViewScreenState createState() => _PhotoViewScreenState();
}

class _PhotoViewScreenState extends State<PhotoViewScreen> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        automaticallyImplyLeading: true,
        title: Text(widget.userName),
        centerTitle: true,
      ),
      body: PhotoView(
          enableRotation: false,
          heroAttributes: PhotoViewHeroAttributes(tag: widget.urlImage),
          loadingBuilder: (context, _) {
            return CircularProgressIndicator(
              valueColor: AlwaysStoppedAnimation(Colors.white),
            );
          },
          imageProvider: NetworkImage(widget.urlImage)),
    );
  }
}
