import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:faen_app_admin/widgets/dialogs/add_coupon_dialog.dart';
import 'package:faen_app_admin/widgets/tiles/coupons_tile.dart';
import 'package:flutter/material.dart';

class CouponsScreen extends StatefulWidget {
  @override
  _CouponsScreenState createState() => _CouponsScreenState();
}

class _CouponsScreenState extends State<CouponsScreen> {
  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: const BoxDecoration(
          image: DecorationImage(
              image: ExactAssetImage("images/Fundo2.png"), fit: BoxFit.cover)),
      child: Scaffold(
          //backgroundColor: Colors.grey[850],
          appBar: AppBar(
            title: Text("Cupons Disponíveis"),
            centerTitle: true,
          ),
          floatingActionButton: FloatingActionButton(
            backgroundColor: Colors.green[900].withOpacity(0.87),
            child: Icon(Icons.add),
            onPressed: () async {
              await showDialog(context: context, child: AddCouponDialog());
              update();
            },
          ),
          body: FutureBuilder<QuerySnapshot>(
              future: Firestore.instance.collection("coupons").getDocuments(),
              builder: (context, snapshot) {
                if (!snapshot.hasData)
                  return Center(
                    child: CircularProgressIndicator(
                      valueColor: AlwaysStoppedAnimation(Colors.white),
                    ),
                  );
                return RefreshIndicator(
                  backgroundColor: Colors.white.withOpacity(0.9),
                  color: Colors.greenAccent[700],
                  onRefresh: () async {
                    await Future.delayed(Duration(seconds: 1), () {
                      setState(() {});
                    });
                  },
                  child: ListView.separated(
                    itemCount: snapshot.data.documents.length,
                    itemBuilder: (context, index) {
                      return CouponsTile(
                          snapshot.data.documents[index], update);
                    },
                    separatorBuilder: (context, index) {
                      return Divider(
                        height: 2.0,
                        color: Colors.white12,
                      );
                    },
                  ),
                );
              })),
    );
  }

  VoidCallback update() {
    setState(() {});
    return null;
  }
}
