import 'dart:io';
import 'package:bloc_pattern/bloc_pattern.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:faen_app_admin/blocs/user_bloc.dart';
import 'package:faen_app_admin/widgets/dialogs/send_notification_dialog.dart';
import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_masked_text/flutter_masked_text.dart';
import 'package:flutter_neumorphic/flutter_neumorphic.dart';
import 'package:transparent_image/transparent_image.dart';

import 'home_screen.dart';

class EditUserScreen extends StatefulWidget {
  final Map<String, dynamic> userData;
  EditUserScreen(this.userData);
  @override
  _EditUserScreenState createState() => _EditUserScreenState(userData);
}

class _EditUserScreenState extends State<EditUserScreen> {
  Map<String, dynamic> userData;

  final _formKey = GlobalKey<FormState>();
  final _scaffoldKey = GlobalKey<ScaffoldState>();

  _EditUserScreenState(this.userData);

  final _nameController = TextEditingController();
  final _dateController = MaskedTextController(mask: "00/00/0000");
  final _phoneController = MaskedTextController(mask: "(00)0 0000-0000");
  final _cpfController = MaskedTextController(mask: "000.000.000-00");
  final _rgController = TextEditingController();
  final _rgOEController = TextEditingController();

  String _course;
  int _semester;
  String _typePay;
  String _typeUser;
  File image;

  final _semesterController = TextEditingController();
  final _emailController = TextEditingController();
  final _passController = TextEditingController();
  final _passConfirmController = TextEditingController();

  bool isObscure = true;
  bool isAutoValid = false;
  bool _allDocumentReceived;

  final _nameFocus = FocusNode();
  final _dateFocus = FocusNode();
  final _cpfFocus = FocusNode();
  final _rgFocus = FocusNode();
  final _rgOEFocus = FocusNode();
  final _phoneFocus = FocusNode();
  final _emailFocus = FocusNode();
  final _passFocus = FocusNode();
  final _passConfirmFocus = FocusNode();

  final _userBloc = BlocProvider.getBloc<UserBloc>();

  static const courseOptions = <String>[
    'Agronomia',
    'Engenharia Agrícola',
    'Engenharia de Aquicultura',
    'Engenharia de Computação',
    'Engenharia de Energia',
    'Engenharia Civil',
    'Engenharia de Alimentos',
    'Engenharia Mecânica',
    'Engenharia de Produção',
    'Outros'
  ];

  static const payOptions = <String>[
    'Dinheiro',
    'Cartão',
    'Transferência',
  ];

  static const semesterOptions = <int>[1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12];

  static const typeUsersOptions = <String>[
    "Associado",
    "Não Associado",
    "Administrador",
    "Moderador",
    "Parceiro",
  ];

  final List<DropdownMenuItem<String>> _dropDownMenuItems = courseOptions
      .map((String value) => DropdownMenuItem(
            value: value,
            child: Text(value, textAlign: TextAlign.center),
          ))
      .toList();

  final List<DropdownMenuItem<int>> _dropDownMenuItems2 = semesterOptions
      .map((int value) => DropdownMenuItem(
            value: value,
            child: Text("$value"),
          ))
      .toList();

  final List<DropdownMenuItem<String>> _dropDownMenuItems3 = payOptions
      .map((String value) => DropdownMenuItem(
            value: value,
            child: Text(value),
          ))
      .toList();

  final List<DropdownMenuItem<String>> _dropDownMenuItems4 = typeUsersOptions
      .map((String value) => DropdownMenuItem(
            value: value,
            child: Text(
              value, /*style: TextStyle(color: Theme.of(context).primaryColor)*/
            ),
          ))
      .toList();

  static const menuItems = <String>[
    'Apagar',
    'Enviar notificação',
  ];

  final List<PopupMenuItem<String>> _popUpMenuItems = menuItems
      .map(
        (String value) => PopupMenuItem<String>(
          value: value,
          child: Text(value),
        ),
      )
      .toList();

  @override
  void initState() {
    super.initState();

    _nameController.text = userData["name"];
    _cpfController.text = userData["cpf"];
    _rgController.text = userData["rg"] ?? "";
    _rgOEController.text = userData["rgOE"] ?? "";
    _emailController.text = userData["email"];
    _dateController.text = userData["date"];
    _phoneController.text = userData["phone"];
    _course = userData["course"];
    _typeUser = userData["typeUser"];
    _semester = userData["semester"];
    _typePay = userData["typePay"];
    _allDocumentReceived = userData["allDocumentReceived"];
  }

  @override
  void dispose() {
    super.dispose();
    _emailController.dispose();
    _nameController.dispose();
    _dateController.dispose();
    _cpfController.dispose();
    _semesterController.dispose();
    _phoneController.dispose();
    _passController.dispose();
    _passConfirmController.dispose();
    _rgController.dispose();
    _rgOEController.dispose();
    _emailFocus.dispose();
    _passFocus.dispose();
    _passConfirmFocus.dispose();
    _nameFocus.dispose();
    _dateFocus.dispose();
    _phoneFocus.dispose();
    _cpfFocus.dispose();
    _rgFocus.dispose();
    _rgOEFocus.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(
          image: DecorationImage(
              image: ExactAssetImage("images/Fundo2.png"), fit: BoxFit.cover)),
      child: Scaffold(
        key: _scaffoldKey,
        appBar: AppBar(
          title: Text("Edição de Usuário"),
          centerTitle: true,
          actions: <Widget>[
            PopupMenuButton<String>(
              color: Colors.white.withOpacity(0.9),
              onSelected: (newValue) {
                switch (newValue) {
                  case "Apagar":
                    showAlertDialog(context);
                    break;
                  case 'Enviar notificação':
                    showDialog(
                        context: context,
                        builder: (context) => SendNotificationDialog(
                            userData["uid"], userData["name"]));
                    /* showSendNotificationDialog(
                    context, orderData.data["clientId"]);*/
                    break;
                }
              },
              itemBuilder: (context) => _popUpMenuItems,
            ),
          ],
        ),
        floatingActionButton: FloatingActionButton(
          child: Icon(Icons.save),
          backgroundColor: Colors.green[900].withOpacity(0.87),
          onPressed: () async {
            if (_formKey.currentState.validate())
              try {
                userData["rg"] != null
                    ? _userBloc.updateUserData(userData["uid"], _mapToUpdate())
                    : await Firestore.instance
                        .collection("users")
                        .document(userData["uid"])
                        .setData(_mapToUpdate());
                _buildDialog(context, true);
              } catch (e) {
                print("${e.toString()}");
                _buildDialog(context, false);
              }
          },
        ),
        body: Form(
          key: _formKey,
          child: SingleChildScrollView(
            padding: EdgeInsets.symmetric(vertical: 8.0, horizontal: 10.0),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Center(
                  child: SizedBox(
                    height: MediaQuery.of(context).size.height * 0.30,
                    width: MediaQuery.of(context).size.height * 0.30,
                    child: Card(
                        elevation: 8.0,
                        clipBehavior: Clip.antiAlias,
                        shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.all(Radius.circular(15.0)),
                        ),
                        child:
                            userData["photo"] != null || userData["photo"] != ""
                                ? Hero(
                                    tag: userData["photo"],
                                    child: FadeInImage.memoryNetwork(
                                      placeholder: kTransparentImage,
                                      image: userData["photo"] ?? "",
                                      fit: BoxFit.cover,
                                    ),
                                  )
                                : Center(
                                    child: Icon(
                                      Icons.broken_image,
                                      color: Colors.white70,
                                      size: 50.0,
                                    ),
                                  )),
                  ),
                ),
                SizedBox(
                  height: 10.0,
                ),
                _inputFormField(
                    label: "Nome Completo",
                    controller: _nameController,
                    textValidator: "Insira o nome !",
                    focus: _nameFocus,
                    showCursor: true,
                    typeInput: TextInputType.text),
                SizedBox(height: 16.0),
                _inputFormField(
                    label: "Data de Nascimento",
                    controller: _dateController,
                    focus: _dateFocus,
                    showCursor: false,
                    textValidator: "Insira a data de nascimento !",
                    hintText: "23/09/2000",
                    typeInput: TextInputType.datetime),
                SizedBox(height: 16.0),
                _inputFormField(
                    label: "CPF",
                    controller: _cpfController,
                    hintText: "123.456.789-00",
                    textValidator: "Insira o CPF !",
                    typeInput: TextInputType.number,
                    focus: _cpfFocus,
                    showCursor: false),
                SizedBox(height: 16.0),
                _inputFormField(
                    label: "Telefone",
                    controller: _phoneController,
                    hintText: "(67)9 9999-9999",
                    textValidator: "Insira o número de telefone !",
                    typeInput: TextInputType.phone,
                    focus: _phoneFocus,
                    showCursor: false),
                SizedBox(height: 16.0),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  children: <Widget>[
                    SizedBox(
                      //height: 150,
                      width: MediaQuery.of(context).size.width * 0.45,
                      child: _inputFormField(
                          label: "RG",
                          controller: _rgController,
                          focus: _rgFocus,
                          textValidator: "Insira o número do seu RG",
                          typeInput: TextInputType.numberWithOptions(),
                          showCursor: true),
                    ),
                    SizedBox(
                      width: 5,
                    ),
                    SizedBox(
                      width: MediaQuery.of(context).size.width * 0.30,
                      child: _inputFormField(
                          label: "Orgão\nExpedidor",
                          controller: _rgOEController,
                          focus: _rgOEFocus,
                          showCursor: true,
                          textValidator:
                              "É necessário inserir o Orgão Expedidor"),
                    ),
                  ],
                ),
                SizedBox(height: 16.0),
                Card(
                  margin: EdgeInsets.zero,
                  shape: OutlineInputBorder(
                      borderSide: BorderSide(
                        color: Colors.white54,
                      ),
                      borderRadius: BorderRadius.circular(12.5)),
                  color: Colors.white10,
                  child: ListTile(
                    //dense: true,
                    contentPadding: EdgeInsets.zero,
                    title: Padding(
                      padding: const EdgeInsets.only(left: 16.0),
                      child: Text("Tipo de usuário: ",
                          style:
                              TextStyle(color: Colors.white70, fontSize: 16.0)),
                    ),
                    trailing: Padding(
                      padding: const EdgeInsets.only(right: 2.0, left: 2.0),
                      child: DropdownButton(
                        value: _typeUser,
                        style: TextStyle(
                            fontFamily: 'BebasNeue', color: Colors.green[700]),
                        items: _dropDownMenuItems4,
                        onChanged: ((newValue) {
                          setState(() {
                            _typeUser = newValue;
                          });
                        }),
                      ),
                    ),
                  ),
                ),
                SizedBox(height: 16.0),
                Card(
                  margin: EdgeInsets.zero,
                  shape: OutlineInputBorder(
                      borderSide: BorderSide(
                        color: Colors.white54,
                      ),
                      borderRadius: BorderRadius.circular(12.5)),
                  color: Colors.white10,
                  child: ListTile(
                    contentPadding: EdgeInsets.symmetric(horizontal: 2.0),
                    title: Padding(
                      padding: const EdgeInsets.only(left: 16.0),
                      child: Text(
                        "Curso:",
                        style: TextStyle(color: Colors.white70),
                        softWrap: true,
                      ),
                    ),
                    trailing: Padding(
                      padding: const EdgeInsets.only(right: 2.0, left: 2.0),
                      child: DropdownButton(
                        value: _course,
                        style: TextStyle(
                            fontFamily: 'BebasNeue', color: Colors.green[700]),
                        items: _dropDownMenuItems,
                        onChanged: ((newValue) {
                          setState(() {
                            _course = newValue;
                          });
                        }),
                      ),
                    ),
                  ),
                ),
                SizedBox(height: 16.0),
                Card(
                  margin: EdgeInsets.zero,
                  shape: OutlineInputBorder(
                      borderSide: BorderSide(
                        color: Colors.white54,
                      ),
                      borderRadius: BorderRadius.circular(12.5)),
                  color: Colors.white10,
                  child: ListTile(
                    contentPadding: EdgeInsets.zero,
                    title: Padding(
                      padding: const EdgeInsets.only(left: 16.0),
                      child: Text(
                        "Semestre atual:",
                        style: TextStyle(color: Colors.white70),
                        softWrap: true,
                      ),
                    ),
                    trailing: Padding(
                      padding: const EdgeInsets.only(right: 2.0, left: 2.0),
                      child: DropdownButton(
                        value: _semester,
                        style: TextStyle(
                            fontFamily: 'BebasNeue', color: Colors.green[700]),
                        items: _dropDownMenuItems2,
                        onChanged: ((newValue) {
                          setState(() {
                            _semester = newValue;
                          });
                        }),
                      ),
                    ),
                  ),
                ),
                SizedBox(height: 16.0),
                Card(
                  margin: EdgeInsets.zero,
                  shape: OutlineInputBorder(
                      borderSide: BorderSide(
                        color: Colors.white54,
                      ),
                      borderRadius: BorderRadius.circular(12.5)),
                  color: Colors.white10,
                  child: ListTile(
                    contentPadding: EdgeInsets.zero,
                    title: Padding(
                      padding: const EdgeInsets.only(left: 16.0),
                      child: Text(
                        "Forma de pagamento:",
                        softWrap: true,
                        style: TextStyle(color: Colors.white70),
                      ),
                    ),
                    trailing: Padding(
                      padding: const EdgeInsets.only(right: 2.0, left: 2.0),
                      child: DropdownButton(
                        value: _typePay,
                        items: _dropDownMenuItems3,
                        style: TextStyle(
                            fontFamily: 'BebasNeue', color: Colors.green[700]),
                        onChanged: ((newValue) {
                          setState(() {
                            _typePay = newValue;
                          });
                        }),
                      ),
                    ),
                  ),
                ),
                SizedBox(height: 16.0),
                Card(
                  margin: EdgeInsets.zero,
                  shape: OutlineInputBorder(
                      borderSide: BorderSide(
                        color: Colors.white54,
                      ),
                      borderRadius: BorderRadius.circular(12.5)),
                  color: Colors.white10,
                  child: ListTile(
                    //dense: true,
                    contentPadding: EdgeInsets.zero,
                    title: Padding(
                      padding: const EdgeInsets.only(left: 16.0),
                      child: Text("Documentos entregues",
                          textAlign: TextAlign.start,
                          style:
                              TextStyle(color: Colors.white70, fontSize: 16.0)),
                    ),
                    trailing: NeumorphicCheckbox(
                        margin: EdgeInsets.all(8.0),
                        padding: EdgeInsets.all(8.0),
                        style: NeumorphicCheckboxStyle(
                          selectedColor: Colors.green[900],
                          disabledColor: Colors.black,
                        ),
                        value: _allDocumentReceived,
                        onChanged: (value) {
                          setState(() {
                            _allDocumentReceived = !_allDocumentReceived;
                          });
                        }),
                  ),
                ),
                SizedBox(height: 16.0),
                FlatButton.icon(
                  color: Colors.green[900],
                  icon: Icon(
                    Icons.content_copy,
                    color: Colors.white,
                  ),
                  label: Text("Copiar dados",
                      style: TextStyle(color: Colors.white)),
                  onPressed: () {
                    Clipboard.setData(new ClipboardData(text: _copyUserData()));
                    _showSnackBar(
                        "As informações do usuário foram copiadas com sucesso !");
                  },
                )
              ],
            ),
          ),
        ),
      ),
    );
  }

  Widget lineText(String title, String userData) {
    return Padding(
      padding: EdgeInsets.only(top: 5.0, bottom: 5.0),
      child: RichText(
        text: TextSpan(
            text: title,
            style: TextStyle(
              fontSize: 18.0,
            ),
            children: <TextSpan>[
              TextSpan(
                text: userData,
                style: TextStyle(fontWeight: FontWeight.bold, fontSize: 16.0),
                recognizer: TapGestureRecognizer()
                  ..onTap = () {
                    Clipboard.setData(new ClipboardData(text: userData));
                    _showSnackBar(
                        "A informação \"$userData\" foi adicionada à área de tranferência !");
                  },
              )
            ]),
      ),
    );
  }

  Widget _inputFormField(
      {@required String label,
      @required TextEditingController controller,
      String textValidator,
      String hintText,
      @required bool showCursor,
      TextInputType typeInput,
      @required FocusNode focus}) {
    return TextFormField(
      enableSuggestions: true,
      showCursor: showCursor,
      focusNode: focus,
      keyboardType: typeInput,
      controller: controller,
      style: TextStyle(fontFamily: 'Lato', color: Colors.white70),
      decoration: InputDecoration(
        filled: true,
        labelStyle: TextStyle(fontFamily: 'BebasNeue', color: Colors.white70),
        fillColor: Colors.white10,
        labelText: label,
        hintText: hintText ?? "",
        enabledBorder: OutlineInputBorder(
            borderSide: BorderSide(
              color: Colors.white54,
            ),
            borderRadius: BorderRadius.circular(15.0)),
        focusedBorder: OutlineInputBorder(
            borderSide: BorderSide(
              color: Colors.green[900],
            ),
            borderRadius: BorderRadius.circular(15.0)),
        focusedErrorBorder: OutlineInputBorder(
            borderSide: BorderSide(
              color: Colors.red[900],
            ),
            borderRadius: BorderRadius.circular(15.0)),
        errorBorder: OutlineInputBorder(
            borderSide: BorderSide(
              color: Colors.red[900],
            ),
            borderRadius: BorderRadius.circular(15.0)),
      ),
      validator: (text) {
        if (text.isEmpty) {
          FocusScope.of(context).requestFocus(focus);
          return textValidator;
        } else
          return null;
      },
    );
  }

  _showSnackBar(String message) async {
    _scaffoldKey.currentState.showSnackBar(SnackBar(
      content: Text(
        message,
        style: TextStyle(color: Colors.white),
      ),
      duration: Duration(seconds: 3),
      backgroundColor: Color.fromARGB(255, 4, 64, 0),
    ));
    Future.delayed(Duration(seconds: 3)).then((_) {
      _scaffoldKey.currentState.removeCurrentSnackBar();
    });
  }

  _buildDialog(BuildContext context, bool result) {
    String message = result
        ? "As informações do usuário foram salvas com sucesso !"
        : "Não foi possível atualizar as informações do usuário !";
    Widget button = FlatButton(
      child: Text("Continuar"),
      onPressed: () {
        result
            ? Navigator.of(context).pushReplacement(
                MaterialPageRoute(builder: (context) => HomeScreen(pageX: 0)))
            : Navigator.of(context).pop();
      },
    );
    showDialog(
        context: context,
        builder: (context) => AlertDialog(
              shape:
                  OutlineInputBorder(borderRadius: BorderRadius.circular(12.5)),
              title: Text(message),
              actions: <Widget>[button],
            ));
  }

  showAlertDialog(context) {
    Widget botaoDeletar = FlatButton(
      color: Colors.transparent,
      child: Text(
        "Deletar",
        style: TextStyle(color: Colors.red[900]),
      ),
      onPressed: () async {
        await _userBloc.deleteUser(userData["uid"]);
        Navigator.pop(context);
        Navigator.of(context).pushReplacement(
            MaterialPageRoute(builder: (context) => HomeScreen(pageX: 1)));
      },
    );
    Widget botaoCancelar = FlatButton(
      color: Colors.transparent,
      child: Text(
        "Cancelar",
        style: TextStyle(color: Colors.green[900]),
      ),
      onPressed: () {
        Navigator.pop(context);
      },
    );
    showDialog(
      context: context,
      builder: (BuildContext context) {
        return AlertDialog(
            elevation: 2.0,
            shape:
                OutlineInputBorder(borderRadius: BorderRadius.circular(12.5)),
            title: Text(
              "Tem certeza que deseja deletar o usuário ${userData["name"]} ?",
              textAlign: TextAlign.center,
            ),
            content: Text(
              "Depois de deletado não será possível recuperar as informações do usuário !",
              textAlign: TextAlign.justify,
            ),
            actions: [botaoDeletar, botaoCancelar]);
      },
    );
  }

  Map<String, dynamic> _mapToUpdate() {
    Map<String, dynamic> toUpdateMap = {
      "name": _nameController.text,
      "photo": userData["photo"],
      "cpf": _cpfController.text,
      "rg": _rgController.text,
      "rgOE": _rgOEController.text,
      "email": _emailController.text,
      "date": _dateController.text,
      "phone": _phoneController.text,
      "course": _course,
      "semester": _semester,
      "typeUser": _typeUser,
      "typePay": _typePay,
      "allDocumentReceived": _allDocumentReceived,
    };
    return toUpdateMap;
  }

  String _copyUserData() {
    String copiedData = "Nome: " + userData["name"] + "\n";
    copiedData += "CPF: " + userData["cpf"] + "\n";
    copiedData += "Data de nascimento: " + userData["date"] + "\n";
    copiedData += "Email: " + userData["email"] + "\n";
    copiedData += "Telefone: " + userData["phone"] + "\n";
    copiedData += "Curso: " + userData["course"] + "\n";
    copiedData += "Semestre: " + "${userData["semester"]}" + "\n";
    copiedData += "Tipo de pagamento: " + userData["typePay"] + "\n";
    copiedData += "Tipo de Usuário: " + userData["typeUser"] + "\n";
    copiedData += "Documentos Entregues: ";
    copiedData += userData["allDocumentReceived"] ? "Sim" : "Não";

    return copiedData;
  }
}
