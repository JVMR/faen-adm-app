import 'package:faen_app_admin/blocs/login_bloc.dart';
import 'package:faen_app_admin/widgets/input_field.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

import 'home_screen.dart';

class LoginScreen extends StatefulWidget {
  @override
  _LoginScreenState createState() => _LoginScreenState();
}

class _LoginScreenState extends State<LoginScreen> {
  final _loginBloc = LoginBloc();

  @override
  void initState() {
    super.initState();

    _loginBloc.outState.listen((state) {
      switch (state) {
        case LoginState.SUCCESS:
          Navigator.of(context).pushReplacement(
              MaterialPageRoute(builder: (context) => HomeScreen()));
          break;
        case LoginState.FAIL:
          break;
        case LoginState.FAILP:
          showDialog(
              context: context,
              builder: (context) => AlertDialog(
                    title: Text("Erro"),
                    content: Text("Você não possui os privilégios necessários"),
                  ));
          break;
        case LoginState.LOADING:
        case LoginState.IDLE:
      }
    });
  }

  @override
  void dispose() {
    _loginBloc.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    SystemChrome.setSystemUIOverlayStyle(SystemUiOverlayStyle(
      systemNavigationBarColor: Colors.black, // navigation bar color
      systemNavigationBarDividerColor: Colors.black87,
      systemNavigationBarIconBrightness: Brightness.light,
      statusBarColor:
          Theme.of(context).primaryColor.withOpacity(0.5), // status bar color
    ));
    return Container(
      decoration: const BoxDecoration(
          image: DecorationImage(
              image: ExactAssetImage("images/Fundo2.png"), fit: BoxFit.cover)),
      child: Scaffold(
        //  backgroundColor: Colors.grey[850],
        body: StreamBuilder<LoginState>(
            stream: _loginBloc.outState,
            initialData: LoginState.LOADING,
            builder: (context, snapshot) {
              switch (snapshot.data) {
                case LoginState.LOADING:
                  return Center(
                    child: Container(
                      decoration: BoxDecoration(
                          color: Colors.black87, shape: BoxShape.circle),
                      padding: EdgeInsets.all(5.0),
                      child: CircularProgressIndicator(
                        valueColor: AlwaysStoppedAnimation(Colors.white),
                      ),
                    ),
                  );
                case LoginState.SUCCESS:
                  return Container();
                  break;
                case LoginState.FAIL:
                case LoginState.FAILP:
                case LoginState.IDLE:
                  return ListView(
                    padding: EdgeInsets.zero,
                    children: <Widget>[
                      Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: <Widget>[
                          Center(
                            child: Card(
                              shape: RoundedRectangleBorder(
                                borderRadius:
                                    BorderRadius.all(Radius.circular(15.0)),
                              ),
                              margin: EdgeInsets.symmetric(
                                  horizontal: 20.0, vertical: 60.0),
                              color: Colors.black87,
                              child: Padding(
                                padding: const EdgeInsets.all(16.0),
                                child: Column(
                                  crossAxisAlignment: CrossAxisAlignment.center,
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  mainAxisSize: MainAxisSize.min,
                                  children: <Widget>[
                                    Container(
                                      width: 140.0,
                                      height: 140.0,
                                      decoration: BoxDecoration(
                                        image: DecorationImage(
                                            image:
                                                AssetImage("images/logo.png"),
                                            fit: BoxFit.cover),
                                      ),
                                    ),
                                    SizedBox(height: 16.0),
                                    InputField(
                                      icon: Icons.person_outline,
                                      label: "Usuário",
                                      obscure: false,
                                      stream: _loginBloc.outEmail,
                                      onChanged: _loginBloc.changeEmail,
                                    ),
                                    SizedBox(height: 16.0),
                                    InputField(
                                      icon: Icons.lock_outline,
                                      label: "Senha",
                                      obscure: true,
                                      stream: _loginBloc.outPassword,
                                      onChanged: _loginBloc.changePassword,
                                    ),
                                    SizedBox(height: 16.0),
                                    StreamBuilder<bool>(
                                        stream: _loginBloc.outSubmitValid,
                                        builder: (context, snapshot) {
                                          return SizedBox(
                                            height: 44.0,
                                            width: MediaQuery.of(context)
                                                    .size
                                                    .width *
                                                0.7,
                                            child: RaisedButton(
                                              color: Theme.of(context)
                                                  .primaryColor,
                                              child: Text("Entrar"),
                                              onPressed: snapshot.hasData
                                                  ? _loginBloc.submit
                                                  : null,
                                              textColor: Colors.white,
                                              disabledColor: Theme.of(context)
                                                  .primaryColor
                                                  .withAlpha(122),
                                              shape: OutlineInputBorder(
                                                borderSide: BorderSide(
                                                    color: Color.fromARGB(
                                                        122, 4, 64, 0)),
                                                borderRadius: BorderRadius.all(
                                                    Radius.circular(15.0)),
                                              ),
                                            ),
                                          );
                                        })
                                  ],
                                ),
                              ),
                            ),
                          ),
                        ],
                      ),
                    ],
                  );
                  break;
                default:
                  return Container();
              }
            }),
      ),
    );
  }
}
