import 'package:bloc_pattern/bloc_pattern.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:faen_app_admin/blocs/revenue_control_bloc.dart';
import 'package:faen_app_admin/blocs/user_bloc.dart';
import 'package:faen_app_admin/models/notification_sender.dart';
import 'package:faen_app_admin/widgets/dialogs/send_notification_dialog.dart';
import 'package:flutter/material.dart';
import 'package:flutter_neumorphic/flutter_neumorphic.dart';
import 'package:transparent_image/transparent_image.dart';

class OrderScreen extends StatefulWidget {
  final DocumentSnapshot orderData;

  OrderScreen(this.orderData);
  @override
  _OrderScreenState createState() => _OrderScreenState(orderData);
}

class _OrderScreenState extends State<OrderScreen> {
  final DocumentSnapshot orderData;
  final NotificationSender _notificationSender = NotificationSender();
  int status;

  _OrderScreenState(this.orderData);

  @override
  void initState() {
    super.initState();
    status = orderData["status"];
  }

  final states = [
    "",
    "Aguardando pagamento",
    "Pedido em preparação",
    "Pedido pronto",
    "Entregue"
  ];

  static const menuItems = <String>[
    'Apagar',
    'Enviar notificação',
  ];

  final List<PopupMenuItem<String>> _popUpMenuItems = menuItems
      .map(
        (String value) => PopupMenuItem<String>(
          value: value,
          child: Text(value, style: TextStyle(color: Colors.black)),
        ),
      )
      .toList();

  @override
  Widget build(BuildContext context) {
    String bitIdOrder =
        "#${orderData.documentID.substring(orderData.documentID.length - 7, orderData.documentID.length)}";
    final _userBloc = BlocProvider.getBloc<UserBloc>();
    final _revenueControlBloc = BlocProvider.getBloc<RevenueControlBloc>();
    final _user = _userBloc.getUser(orderData.data["clientId"]);

    final titleStyle = TextStyle(
        color: Colors.white70,
        fontSize: 16.0,
        fontWeight: FontWeight.w500,
        height: 1.2);
    final dataStyle = TextStyle(
        color: Colors.white,
        fontSize: 16.0,
        fontWeight: FontWeight.w700,
        height: 1.2);

    return Stack(
      children: <Widget>[
        Positioned.fill(
          child: Image.asset(
            "images/Fundo2.png",
            fit: BoxFit.cover,
            height: MediaQuery.of(context).size.height,
            width: MediaQuery.of(context).size.width,
          ),
        ),
        Positioned.fill(
          child: Scaffold(
            // backgroundColor: Colors.grey[850],
            appBar: AppBar(
              title: Text("Pedido $bitIdOrder"),
              centerTitle: true,
              actions: <Widget>[
                PopupMenuButton<String>(
                  color: Colors.white.withOpacity(0.9),
                  onSelected: (newValue) {
                    switch (newValue) {
                      case "Apagar":
                        showAlertDialog(context, bitIdOrder);
                        break;
                      case 'Enviar notificação':
                        showDialog(
                            context: context,
                            builder: (context) => SendNotificationDialog(
                                orderData.data["clientId"], _user["name"]));
                        /* showSendNotificationDialog(
                        context, orderData.data["clientId"]);*/
                        break;
                    }
                  },
                  itemBuilder: (context) => _popUpMenuItems,
                ),
              ],
            ),
            body: SingleChildScrollView(
                padding: EdgeInsets.all(8.0),
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.start,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    Center(
                      child: SizedBox(
                        height: MediaQuery.of(context).size.height * 0.30,
                        width: MediaQuery.of(context).size.height * 0.30,
                        child: Neumorphic(
                          /* elevation: 8.0,
                          clipBehavior: Clip.antiAlias,
                          shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.all(Radius.circular(15.0)),
                          ),*/
                          drawSurfaceAboveChild: true,
                          boxShape: NeumorphicBoxShape.roundRect(
                              BorderRadius.circular(15.0)),
                          margin: EdgeInsets.all(5.0),
                          style: NeumorphicStyle(
                            depth: -10.0,
                            intensity: 0.60,
                            surfaceIntensity: 0.30,
                            // shape: NeumorphicShape.convex,
                            lightSource: DateTime.now().hour % 2 == 0
                                ? LightSource.topLeft
                                : LightSource.topRight,
                            color: Colors.black38,
                          ),
                          child: FadeInImage.memoryNetwork(
                            placeholder: kTransparentImage,
                            image: _user["photo"],
                            fit: BoxFit.cover,
                          ),
                        ),
                      ),
                    ),
                    Center(
                      child: Text(
                        "Foto de perfil do usuário",
                        style: TextStyle(color: Colors.white70),
                        textAlign: TextAlign.center,
                      ),
                    ),
                    SizedBox(
                      height: 8.0,
                    ),
                    SelectableText.rich(
                      TextSpan(
                        text: "Informações do Usuário\n",
                        style: TextStyle(
                            height: 1.5,
                            color: Colors.white,
                            fontSize: 17.0,
                            fontWeight: FontWeight.w700),
                        children: <TextSpan>[
                          TextSpan(text: "Nome: ", style: titleStyle),
                          TextSpan(
                              text: "${_user["name"]}\n", style: dataStyle),
                          TextSpan(text: "Membro: ", style: titleStyle),
                          TextSpan(
                              text: "${_user["typeUser"]}\n", style: dataStyle),
                          TextSpan(
                            text: "Informações do Pedido\n",
                            style: TextStyle(
                                height: 2.0,
                                color: Colors.white,
                                fontSize: 17.0,
                                fontWeight: FontWeight.w700),
                          ),
                          TextSpan(text: "ID do produto: ", style: titleStyle),
                          TextSpan(text: bitIdOrder + "\n", style: dataStyle),
                          TextSpan(
                            text: "Pedido feito em ",
                            style: TextStyle(
                                height: 1.2,
                                color: Colors.white70,
                                fontSize: 16.0,
                                fontStyle: FontStyle.italic,
                                fontWeight: FontWeight.w600),
                          ),
                          TextSpan(
                            text: "${orderData.data["orderDate"]}\n",
                            style: TextStyle(
                                height: 1.2,
                                color: Colors.white,
                                fontSize: 16.0,
                                fontStyle: FontStyle.italic,
                                fontWeight: FontWeight.w700),
                          ),
                          TextSpan(
                              text: "Produtos\n",
                              style: TextStyle(
                                  color: Colors.white,
                                  fontSize: 17.0,
                                  fontWeight: FontWeight.w700),
                              children: orderData.data["products"]
                                  .map<TextSpan>((product) {
                                return TextSpan(
                                  text:
                                      "\t-> ${product["quantity"].toString()}x ${product["product"]["title"]} ",
                                  style: dataStyle,
                                  children: <TextSpan>[
                                    TextSpan(
                                        text: "${product["size"]}\n",
                                        style: TextStyle(
                                            color: Colors.white,
                                            fontSize: 16.0,
                                            fontWeight: FontWeight.w500)),
                                  ],
                                );
                              }).toList()),
                          TextSpan(
                              text: "Total sem descontos: ", style: titleStyle),
                          TextSpan(
                              text:
                                  "R\$${orderData.data["productsPrice"].toStringAsFixed(2)}\n",
                              style: dataStyle),
                          TextSpan(text: "Descontos: ", style: titleStyle),
                          TextSpan(
                              text:
                                  "R\$${orderData.data["discount"].toStringAsFixed(2)}\n",
                              style: dataStyle),
                          TextSpan(text: "Total: ", style: titleStyle),
                          TextSpan(
                              text:
                                  "R\$${orderData.data["totalPrice"].toStringAsFixed(2)}\n",
                              style: dataStyle),
                          TextSpan(
                              text: "Forma de Pagamento: ", style: titleStyle),
                          TextSpan(
                              text: "${orderData.data["typePay"]}\n",
                              style: dataStyle),
                          TextSpan(text: "Status: ", style: titleStyle),
                          TextSpan(
                              text: "${states[status]}\n", style: dataStyle),
                        ],
                      ),
                      textAlign: TextAlign.start,
                    ),
                    SizedBox(height: 8.0),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: <Widget>[
                        /* FlatButton(
                          onPressed: orderData.data["status"] > 1
                              ? () {
                                  orderData.reference.updateData(
                                      {"status": orderData.data["status"] - 1});
                                }
                              : null,
                          textColor: Colors.grey[850],
                          child: Text("Regredir"),
                        ),*/
                        Flexible(
                          flex: 1,
                          child: SizedBox(
                            height: MediaQuery.of(context).size.width * 0.18,
                            child: NeumorphicButton(
                              drawSurfaceAboveChild: true,
                              boxShape: NeumorphicBoxShape.roundRect(
                                  BorderRadius.circular(15.0)),
                              padding: EdgeInsets.symmetric(
                                  horizontal: 18, vertical: 5.0),
                              //  margin: EdgeInsets.all(5.0),
                              style: NeumorphicStyle(
                                depth: 10.0,
                                intensity: 0.20,
                                surfaceIntensity: 0.15,
                                shape: NeumorphicShape.concave,
                                lightSource: DateTime.now().hour % 2 == 0
                                    ? LightSource.bottomLeft
                                    : LightSource.bottomRight,
                                color: Colors.black38,
                              ),
                              child: ListTile(
                                contentPadding: EdgeInsets.zero,
                                leading:
                                    Icon(Icons.arrow_back, color: Colors.grey),
                                title: Text("Regredir",
                                    style: TextStyle(
                                        color: Colors.white30,
                                        fontWeight: FontWeight.w500)),
                              ),
                              onClick: orderData.data["status"] > 1
                                  ? () async {
                                      await orderData.reference.updateData({
                                        "status": orderData.data["status"] - 1
                                      });
                                      setState(() {
                                        status--;
                                      });
                                    }
                                  : null,
                            ),
                          ),
                        ),
                        SizedBox(width: 5.0),
                        Flexible(
                          flex: 1,
                          child: SizedBox(
                            height: MediaQuery.of(context).size.width * 0.18,
                            child: NeumorphicButton(
                              drawSurfaceAboveChild: true,
                              padding: EdgeInsets.symmetric(
                                  horizontal: 18, vertical: 5.0),
                              boxShape: NeumorphicBoxShape.roundRect(
                                  BorderRadius.circular(15.0)),
                              style: NeumorphicStyle(
                                depth: 10.0,
                                intensity: 0.20,
                                surfaceIntensity: 0.15,
                                shape: NeumorphicShape.concave,
                                lightSource: DateTime.now().hour % 2 == 0
                                    ? LightSource.bottomRight
                                    : LightSource.bottomLeft,
                                color: Colors.black38,
                              ),
                              child: ListTile(
                                contentPadding: EdgeInsets.zero,
                                trailing: Icon(Icons.arrow_forward,
                                    color: Colors.green),
                                title: Text("Avançar",
                                    style: TextStyle(
                                        color: Colors.green,
                                        fontWeight: FontWeight.w500)),
                              ),
                              onClick: orderData.data["status"] < 4
                                  ? () async {
                                      await orderData.reference.updateData({
                                        "status": orderData.data["status"] + 1
                                      });
                                      sendNotificationForStatusUpdate(
                                          orderData.data["clientId"],
                                          orderData.data["status"] + 1);
                                      if (orderData.data["status"] == 3) {
                                        _revenueControlBloc.addCash(orderData
                                            .data["totalPrice"]
                                            .toString());
                                      }
                                      setState(() {
                                        status++;
                                      });
                                    }
                                  : null,
                            ),
                          ),
                        ),
                        /*Neumorphic(
                          child: FlatButton.icon(
                            onPressed: orderData.data["status"] < 4
                                ? () async {
                                    await orderData.reference.updateData(
                                        {"status": orderData.data["status"] + 1});
                                    sendNotificationForStatusUpdate(
                                        orderData.data["clientId"],
                                        orderData.data["status"] + 1);
                                    if (orderData.data["status"] == 3) {
                                      _revenueControlBloc.addCash(
                                          orderData.data["totalPrice"].toString());
                                    }
                                  }
                                : null,
                            textColor: Colors.green,
                            icon: Icon(Icons.arrow_forward),
                            label: Text("Avançar"),
                            //title: Text("Avançar"),
                          ),
                        )*/
                      ],
                    )
                  ],
                )),
          ),
        ),
      ],
    );
  }

  showAlertDialog(context, String bitIdOrder) {
    Widget botaoDeletar = FlatButton(
      color: Colors.transparent,
      child: Text(
        "Deletar",
        style: TextStyle(color: Colors.red[900]),
      ),
      onPressed: () {
        //Navigator.pop(context);
        Firestore.instance
            .collection("users")
            .document(orderData["clientId"])
            .collection("orders")
            .document(orderData.documentID)
            .delete();
        orderData.reference.delete();
        Navigator.pop(context);
        Navigator.pop(context);
      },
    );
    Widget botaoCancelar = FlatButton(
      color: Colors.transparent,
      child: Text(
        "Cancelar",
        style: TextStyle(color: Colors.green[900]),
      ),
      onPressed: () {
        Navigator.pop(context);
      },
    );
    showDialog(
      context: context,
      builder: (BuildContext context) {
        return AlertDialog(
            elevation: 2.0,
            shape:
                OutlineInputBorder(borderRadius: BorderRadius.circular(12.5)),
            title: Text(
              "Tem certeza que deseja deletar o pedido $bitIdOrder ?",
              textAlign: TextAlign.center,
            ),
            content: Text(
              "Depois de deletado não será possível recuperar as informações desse pedido !",
              textAlign: TextAlign.justify,
            ),
            actions: [botaoDeletar, botaoCancelar]);
      },
    );
  }

  sendNotificationForStatusUpdate(String uid, int status) async {
    String bodyNotification = status == 3
        ? "O seu pedido está pronto para ser retirado : )"
        : "Status atual: ${states[status]}";
    bool sucesso = await _notificationSender.sendNotificationToUser(
        title: "O status do pedido foi atualizado !",
        body: bodyNotification,
        uid: uid);
    print("Retorno da notificação $sucesso");
  }
}
