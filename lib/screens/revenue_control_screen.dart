import 'package:bloc_pattern/bloc_pattern.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:faen_app_admin/blocs/revenue_control_bloc.dart';
import 'package:faen_app_admin/models/pdf_generator.dart';
import 'package:faen_app_admin/widgets/charts/horizontal_bar_chart.dart';
import 'package:faen_app_admin/widgets/charts/month_to_month_chart.dart';
import 'package:flutter/material.dart';
import 'package:flutter_neumorphic/flutter_neumorphic.dart';
import 'package:pdf/pdf.dart';
import 'package:shimmer/shimmer.dart';

class RevenueControlScreen extends StatefulWidget {
  @override
  _RevenueControlScreenState createState() => _RevenueControlScreenState();
}

class _RevenueControlScreenState extends State<RevenueControlScreen> {
  // RevenueControlBloc _revenueControlBloc;
  final _revenueControlBloc = BlocProvider.getBloc<RevenueControlBloc>();
  int index = 0;

  @override
  void initState() {
    super.initState();
    index = _revenueControlBloc.getDataRevenue().length > 0
        ? _revenueControlBloc.getDataRevenue().length - 1
        : 0;
    // _revenueControlBloc = RevenueControlBloc();
  }

  @override
  Widget build(BuildContext context) {
    return Stack(
      children: <Widget>[
        Positioned.fill(
          child: Image.asset(
            "images/Fundo2.png",
            fit: BoxFit.cover,
            height: MediaQuery.of(context).size.height,
            width: MediaQuery.of(context).size.width,
          ),
        ),
        Positioned.fill(
          child: Scaffold(
              //drawer: CustomDrawer(_controller),
              //backgroundColor: Colors.grey[850],
              appBar: AppBar(
                centerTitle: true,
                title: Text("Controle de Receitas"),
                actions: <Widget>[
                  IconButton(
                      icon: Icon(Icons.print),
                      onPressed: () async {
                        await printPdf();
                      }),
                ],
              ),
              body: SingleChildScrollView(
                padding: EdgeInsets.zero,
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.start,
                  crossAxisAlignment: CrossAxisAlignment.stretch,
                  mainAxisSize: MainAxisSize.min,
                  children: <Widget>[
                    SizedBox(height: 5.0),
                    Padding(
                      padding: const EdgeInsets.symmetric(horizontal: 8.0),
                      child: StreamBuilder<int>(
                          stream: _revenueControlBloc.outCountOrders,
                          builder: (context, snapshot) {
                            if (!snapshot.hasData)
                              return _buildShimmerContainer();
                            else
                              return _buildRichText(
                                snapshot.data.toString(),
                                " Pedidos Atuais.",
                              );
                          }),
                    ),
                    SizedBox(height: 5.0),
                    Padding(
                      padding: const EdgeInsets.symmetric(horizontal: 8.0),
                      child: StreamBuilder<Map<String, int>>(
                          stream: _revenueControlBloc.outCountUsersMap,
                          builder: (context, snapshot) {
                            if (!snapshot.hasData)
                              return _buildShimmerContainersColumn();
                            else
                              return Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: <Widget>[
                                  _buildRichText(
                                      snapshot.data["countMembers"].toString(),
                                      " Usuários Associados."),
                                  SizedBox(height: 5.0),
                                  _buildRichText(
                                      snapshot.data["countNonMembers"]
                                          .toString(),
                                      " Usuários Não Associados."),
                                  SizedBox(height: 5.0),
                                  _buildRichText(
                                      snapshot.data["countAdms"].toString(),
                                      " Usuários Administradores."),
                                  SizedBox(height: 5.0),
                                  _buildRichText(
                                      snapshot.data["countTotalUsers"]
                                          .toString(),
                                      " Usuários Totais."),
                                ],
                              );

                            /* _buildRichText("Usuários cadastrados: ",
                                        snapshot.data.documents.length.toString());*/
                          }),
                    ),

                    // SizedBox(height: 10.0),

                    StreamBuilder<List<DocumentSnapshot>>(
                      stream: _revenueControlBloc.outData,
                      builder: (context, snapshot) {
                        if (!snapshot.hasData) {
                          return Center(
                            child: CircularProgressIndicator(
                              valueColor: AlwaysStoppedAnimation(Colors.white),
                            ),
                          );
                        } else if (snapshot.data.length == 0) {
                          return Scaffold(
                            body: Center(
                              child: Text(
                                "Não há dados disponíveis para exibir !",
                                textAlign: TextAlign.center,
                                softWrap: true,
                                style: TextStyle(
                                    color: Colors.white, fontSize: 16),
                              ),
                            ),
                          );
                        } else {
                          return Column(
                            crossAxisAlignment: CrossAxisAlignment.stretch,
                            mainAxisSize: MainAxisSize.min,
                            children: <Widget>[
                              _buildNeuText("Rendimentos Totais\nMês a Mês"),
                              Padding(
                                padding: const EdgeInsets.all(8.0),
                                child: MonthToMonthChart(snapshot.data),
                              ),
                              _buildNeuText("Vendas por produto"),
                              SizedBox(height: 10.0),
                              _buildChartControl(snapshot.data),
                              SizedBox(height: 5.0),
                              Padding(
                                padding: const EdgeInsets.all(8.0),
                                child: SizedBox(
                                  height:
                                      MediaQuery.of(context).size.height * 0.5,
                                  child: HorizontalBarChart.withRealData(
                                      snapshot.data[index]),
                                ),
                              ),
                            ],
                          );
                        }
                      },
                    ),
                  ],
                ),
              )),
        ),
      ],
    );
  }

  Widget _buildNeuText(String text) {
    return Neumorphic(
      drawSurfaceAboveChild: true,
      margin: const EdgeInsets.only(top: 10, bottom: 12),
      padding: const EdgeInsets.only(top: 10, bottom: 10),
      style: const NeumorphicStyle(
          // oppositeShadowLightSource: true,
          //   lightSource: LightSource.lerp(LightSource.top, LightSource.bottom, 0),
          lightSource: LightSource.topLeft,
          depth: 0.0,
          color: Colors.black54,
          shape: NeumorphicShape.convex,
          surfaceIntensity: 0.05,
          intensity: 0.10),
      boxShape: NeumorphicBoxShape.roundRect(BorderRadius.horizontal()),
      child: Text(
        text,
        softWrap: true,
        textAlign: TextAlign.center,
        style: TextStyle(
            shadows: <Shadow>[Shadow(offset: Offset(1, 2), color: Colors.grey)],
            fontWeight: FontWeight.w700,
            color: Colors.white,
            fontSize: 15.5),
      ),
    );
  }

  Widget _buildRichText(String value, String legend) {
    return SelectableText.rich(
      TextSpan(
        text: value,
        style: TextStyle(
            fontWeight: FontWeight.w700, color: Colors.white, fontSize: 16.0),
        children: <TextSpan>[
          TextSpan(
            text: legend,
            style: TextStyle(
                // fontWeight: FontWeight.w700,
                color: Colors.white,
                fontSize: 15.5),
          )
        ],
      ),
    );
  }

  Widget _buildShimmerContainersColumn() {
    return Column(
      children: <Widget>[
        _buildShimmerContainer(),
        SizedBox(height: 5.0),
        _buildShimmerContainer(),
        SizedBox(height: 5.0),
        _buildShimmerContainer(),
        SizedBox(height: 5.0),
        _buildShimmerContainer(),
      ],
    );
  }

  Widget _buildShimmerContainer() {
    return SizedBox(
      width: MediaQuery.of(context).size.width,
      height: 20,
      child: Shimmer.fromColors(
          child: Container(
            color: Colors.white.withAlpha(50),
            margin: EdgeInsets.symmetric(vertical: 4),
          ),
          baseColor: Colors.white,
          highlightColor: Colors.grey),
    );
  }

  Widget _buildChartControl(List<DocumentSnapshot> documents) {
    return Neumorphic(
      drawSurfaceAboveChild: true,
      margin: const EdgeInsets.only(top: 10, bottom: 10),
      padding: const EdgeInsets.only(top: 5, bottom: 5),
      style: const NeumorphicStyle(
          lightSource: LightSource.topLeft,
          depth: 0.0,
          color: Colors.black54,
          shape: NeumorphicShape.convex,
          surfaceIntensity: 0.05,
          intensity: 0.10),
      boxShape: NeumorphicBoxShape.roundRect(BorderRadius.horizontal()),
      child: ListTile(
        trailing: IconButton(
            icon: Icon(Icons.arrow_forward_ios),
            color: Colors.green[900],
            disabledColor: Colors.grey[700],
            onPressed: index == documents.length - 1
                ? null
                : () {
                    setState(() {
                      index++;
                    });
                  }),
        leading: IconButton(
            icon: Icon(Icons.arrow_back_ios),
            color: Colors.green[900],
            disabledColor: Colors.grey[700],
            onPressed: index == 0
                ? null
                : () {
                    index--;
                    setState(() {});
                  }),
        title: Text(
          documents[index].documentID,
          style: TextStyle(
            color: Colors.white,
            fontWeight: FontWeight.w500,
            shadows: <Shadow>[Shadow(offset: Offset(1, 2), color: Colors.grey)],
          ),
          textAlign: TextAlign.center,
        ),
      ),
    );
  }

  Future<void> printPdf() async {
    PdfPageFormat format;
    await PdfGenerator.generateRevenueControlDocument(format);
    await PdfGenerator.savePdf();
    await PdfGenerator.printPdf();
  }
  /*
   SingleChildScrollView(
                            padding: EdgeInsets.zero,
                            scrollDirection: Axis.horizontal,
                            child: NeumorphicToggle(
                              width: snapshot.data.length <= 4
                                  ? MediaQuery.of(context).size.width
                                  : MediaQuery.of(context).size.width * 2,
                              style: NeumorphicToggleStyle(
                                  animateOpacity: true,
                                  borderRadius: BorderRadius.horizontal(),
                                  depth: -10.0

                                  //depth: 20.0,
                                  ),
                              selectedIndex: _page,
                              displayForegroundOnlyIfSelected: true,
                              children: snapshot.data.map((data) {
                                return ToggleElement(
                                  foreground: Card(
                                    margin: EdgeInsets.zero,
                                    color: Colors.green[900],
                                    child: Center(
                                        child: Text(
                                      data.documentID,
                                      style: TextStyle(
                                          fontWeight: FontWeight.w700),
                                    )),
                                  ),
                                  background: Container(
                                    color: Colors.black,
                                    child: Center(
                                        child: Text(data.documentID,
                                            style: TextStyle(
                                                fontWeight: FontWeight.w400))),
                                  ),
                                );
                              }).toList(),
                              thumb: Neumorphic(
                                //style: NeumorphicStyle(color: Colors.black87),
                                boxShape: NeumorphicBoxShape.roundRect(
                                    BorderRadius.all(Radius.circular(12.0))),
                              ),
                              onChanged: (value) {
                                setState(() {
                                  _page = value;
                                  _pageController.animateToPage(_page,
                                      duration: Duration(milliseconds: 500),
                                      curve: Curves.ease);
                                });
                              },
                            ),
                          ),
                          SizedBox(height: 5.0),
                          Padding(
                            padding: const EdgeInsets.all(8.0),
                            child: SizedBox(
                              height: MediaQuery.of(context).size.height * 0.5,
                              child: PageView(
                                controller: _pageController,
                                children: snapshot.data.map((data) {
                                  return HorizontalBarChart.withRealData(data);
                                  /* return data.documentID == "4.2020"
                                      ? HorizontalBarChart.withRealData(data)
                                      : HorizontalBarChart.withRandomData();*/
                                  //return SoldProductsChart(data);
                                }).toList(),
                              ),
                            ),
                          ),
  */
}
