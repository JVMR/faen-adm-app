import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:faen_app_admin/blocs/product_bloc.dart';
import 'package:faen_app_admin/validators/product_validator.dart';
import 'package:faen_app_admin/widgets/images_widget.dart';
import 'package:faen_app_admin/widgets/product_sizes.dart';
import 'package:flutter/material.dart';
import 'package:flutter_neumorphic/flutter_neumorphic.dart';

class ProductScreen extends StatefulWidget {
  final String categoryId;
  final DocumentSnapshot product;

  ProductScreen({this.categoryId, this.product});

  @override
  _ProductScreenState createState() => _ProductScreenState(categoryId, product);
}

class _ProductScreenState extends State<ProductScreen> with ProductValidator {
  final ProductBloc _productBloc;

  final _formKey = GlobalKey<FormState>();
  final _scaffoldKey = GlobalKey<ScaffoldState>();

  _ProductScreenState(String categoryId, DocumentSnapshot product)
      : _productBloc = ProductBloc(categoryId: categoryId, product: product);

  String status;

  static const statusOptions = <String>[
    'Disponível',
    'Esgotado',
  ];

  final List<DropdownMenuItem<String>> _dropDownMenuItems = statusOptions
      .map((String value) => DropdownMenuItem(
            value: value,
            child: Text(
              value,
              textAlign: TextAlign.center,
              style:
                  TextStyle(fontFamily: 'BebasNeue', color: Colors.green[700]),
            ),
          ))
      .toList();

  @override
  Widget build(BuildContext context) {
    InputDecoration _buildDecoration(String label, {String prefix}) {
      return InputDecoration(
        focusColor: Colors.green[900],
        filled: true,
        fillColor: Colors.white10,
        //isDense: true,
        prefixText: prefix,
        prefixStyle: TextStyle(
          fontFamily: 'Lato',
          color: Colors.white70, /*fontWeight: FontWeight.bold*/
        ),
        labelText: label,
        labelStyle: TextStyle(fontFamily: 'BebasNeue', color: Colors.white70),
        enabledBorder: OutlineInputBorder(
            borderSide: BorderSide(
              color: Colors.white60,
            ),
            borderRadius: BorderRadius.circular(15.0)),
        focusedBorder: OutlineInputBorder(
            borderSide: BorderSide(
              color: Colors.green[900],
            ),
            borderRadius: BorderRadius.circular(15.0)),
        focusedErrorBorder: OutlineInputBorder(
            borderSide: BorderSide(
              color: Colors.red[900],
            ),
            borderRadius: BorderRadius.circular(15.0)),
        errorBorder: OutlineInputBorder(
            borderSide: BorderSide(
              color: Colors.red[900],
            ),
            borderRadius: BorderRadius.circular(15.0)),
      );
    }

    final _fieldStyle =
        TextStyle(fontFamily: 'Lato', color: Colors.white, fontSize: 16);

    bool hasSize;

    return Container(
      decoration: BoxDecoration(
          image: DecorationImage(
              image: ExactAssetImage("images/Fundo2.png"), fit: BoxFit.cover)),
      child: Scaffold(
        key: _scaffoldKey,
        //  backgroundColor: Colors.grey[850],
        //resizeToAvoidBottomInset: true,
        appBar: AppBar(
          centerTitle: true,
          // elevation: 0,
          title: StreamBuilder<bool>(
              stream: _productBloc.outCreated,
              initialData: false,
              builder: (context, snapshot) {
                return Text(snapshot.data ? "Editar Produto" : "Criar Produto");
              }),
          actions: <Widget>[
            StreamBuilder<bool>(
              stream: _productBloc.outCreated,
              initialData: false,
              builder: (context, snapshot) {
                if (snapshot.data)
                  return StreamBuilder<bool>(
                      stream: _productBloc.outLoading,
                      initialData: false,
                      builder: (context, snapshot) {
                        return IconButton(
                          icon: Icon(Icons.delete_outline),
                          onPressed: snapshot.data
                              ? null
                              : () {
                                  _productBloc.deleteProduct();
                                  Navigator.of(context).pop();
                                },
                        );
                      });
                else
                  return Container();
              },
            ),
            StreamBuilder<bool>(
                stream: _productBloc.outLoading,
                initialData: false,
                builder: (context, snapshot) {
                  return IconButton(
                    icon: Icon(Icons.save),
                    onPressed: snapshot.data ? null : saveProduct,
                  );
                }),
          ],
        ),
        body: Stack(
          children: <Widget>[
            /*Image.asset(
              "images/Fundo2.png",
              fit: BoxFit.fill,
              height: MediaQuery.of(context).size.height,
              width: MediaQuery.of(context).size.width,
            ),*/
            Form(
              key: _formKey,
              child: StreamBuilder<Map>(
                  stream: _productBloc.outData,
                  builder: (context, snapshot) {
                    if (!snapshot.hasData) return Container();
                    hasSize = snapshot.data["hasSize"];
                    return ListView(
                      padding: EdgeInsets.zero,
                      shrinkWrap: true,
                      children: <Widget>[
                        Stack(
                          children: <Widget>[
                            /*Image.asset(
                              "images/Fundo2.png",
                              fit: BoxFit.fill,
                              height: MediaQuery.of(context).size.height,
                              width: MediaQuery.of(context).size.width,
                            ),*/
                            SingleChildScrollView(
                              padding: EdgeInsets.all(16),
                              child: Column(
                                children: <Widget>[
                                  Text(
                                    "Imagens",
                                    style: TextStyle(
                                        color: Colors.white70, fontSize: 14),
                                  ),
                                  ImagesWidget(
                                    context: context,
                                    initialValue: snapshot.data["images"],
                                    onSaved: _productBloc.saveImages,
                                    validator: validateImages,
                                  ),
                                  SizedBox(height: 10.0),
                                  TextFormField(
                                    initialValue: snapshot.data["title"],
                                    style: _fieldStyle,
                                    decoration: _buildDecoration("Título"),
                                    onSaved: _productBloc.saveTitle,
                                    validator: validateTitle,
                                  ),
                                  SizedBox(height: 10.0),
                                  TextFormField(
                                    initialValue: snapshot.data["description"],
                                    style: _fieldStyle,
                                    maxLines: 6,
                                    decoration: _buildDecoration("Descrição"),
                                    onSaved: _productBloc.saveDescription,
                                    validator: validateDescription,
                                  ),
                                  SizedBox(height: 10.0),
                                  TextFormField(
                                    initialValue: snapshot.data["priceMember"]
                                        ?.toStringAsFixed(2),
                                    style: _fieldStyle,
                                    decoration: _buildDecoration(
                                        "Preço para Sócios",
                                        prefix: "R\$ "),
                                    keyboardType:
                                        TextInputType.numberWithOptions(
                                            decimal: true),
                                    onSaved: _productBloc.savePriceMember,
                                    validator: validateMemberPrice,
                                  ),
                                  SizedBox(height: 10.0),
                                  TextFormField(
                                    initialValue: snapshot.data["price"]
                                        ?.toStringAsFixed(2),
                                    style: _fieldStyle,
                                    decoration: _buildDecoration(
                                        "Preço para Não Sócios",
                                        prefix: "R\$ "),
                                    keyboardType:
                                        TextInputType.numberWithOptions(
                                            decimal: true),
                                    onSaved: _productBloc.savePrice,
                                    validator: validateNoMemberPrice,
                                  ),
                                  SizedBox(height: 10.0),
                                  Card(
                                    margin: EdgeInsets.zero,
                                    shape: OutlineInputBorder(
                                        borderSide: BorderSide(
                                          color: Colors.white54,
                                        ),
                                        borderRadius:
                                            BorderRadius.circular(12.5)),
                                    color: Colors.white12,
                                    child: ListTile(
                                      // dense: true,
                                      //contentPadding: EdgeInsets.zero,
                                      contentPadding: EdgeInsets.zero,
                                      title: Padding(
                                        padding:
                                            const EdgeInsets.only(left: 16.0),
                                        child: Text(
                                          "Defina o Status:",
                                          style: TextStyle(
                                              color: Colors.white70,
                                              fontSize: 16.0),
                                          softWrap: true,
                                        ),
                                      ),
                                      trailing: Padding(
                                        padding: const EdgeInsets.only(
                                            right: 2.0, left: 2.0),
                                        child: DropdownButton(
                                          style: TextStyle(
                                              color: Colors.green[700]),
                                          /* value: status ?? snapshot.data["status"]
                                              .toString(),*/
                                          value: snapshot.data["status"]
                                              .toString(),
                                          // hint: Text("Escolha seu curso"),
                                          /*style: TextStyle(
                                        color: Theme.of(context).primaryColor,
                                      ),*/
                                          items: _dropDownMenuItems,
                                          onChanged: (newValue) {
                                            setState(() {
                                              _productBloc
                                                  .updateStatus(newValue);
                                            });
                                          },
                                        ),
                                      ),
                                    ),
                                  ),
                                  SizedBox(height: 10.0),
                                  Card(
                                      margin: EdgeInsets.zero,
                                      shape: OutlineInputBorder(
                                          borderSide: BorderSide(
                                            color: Colors.white54,
                                          ),
                                          borderRadius:
                                              BorderRadius.circular(12.5)),
                                      color: Colors.white12,
                                      child: ListTile(
                                        title: Text(
                                          "Esse produto possui tamanhos variados ?",
                                          style: TextStyle(
                                              color: Colors.white70,
                                              fontSize: 16.0),
                                        ),
                                        trailing: NeumorphicCheckbox(
                                            padding: EdgeInsets.all(8.0),
                                            style: NeumorphicCheckboxStyle(
                                              selectedColor: Colors.green[900],
                                              disabledColor: Colors.black,
                                            ),
                                            value: snapshot.data["hasSize"],
                                            onChanged: (value) {
                                              setState(() {
                                                _productBloc.saveHasSize(value);
                                                hasSize = value;
                                              });
                                            }),
                                      )),
                                  /* CheckboxListTile(
                                      title: Text(
                                        "Esse produto possui tamanhos variados ?",
                                        style: TextStyle(
                                            color: Colors.white70,
                                            fontSize: 16.0),
                                      ),
                                      activeColor: Colors.green[900],
                                      checkColor: Colors.white,
                                      value: snapshot.data["hasSize"],
                                      onChanged: (value) {
                                        setState(() {
                                          _productBloc.saveHasSize(value);
                                          hasSize = value;
                                        });
                                      }),*/
                                  SizedBox(
                                    height: 16,
                                  ),
                                  Visibility(
                                    visible: hasSize,
                                    child: Column(
                                      mainAxisAlignment:
                                          MainAxisAlignment.start,
                                      crossAxisAlignment:
                                          CrossAxisAlignment.start,
                                      mainAxisSize: MainAxisSize.min,
                                      children: <Widget>[
                                        Text(
                                          "Tamanhos",
                                          style: TextStyle(
                                              color: Colors.grey, fontSize: 12),
                                        ),
                                        ProductSizes(
                                          context: context,
                                          initialValue: snapshot.data["sizes"],
                                          onSaved: _productBloc.saveSizes,
                                          validator: (s) {
                                            if (s.isEmpty &&
                                                snapshot.data["hasSize"])
                                              return "";
                                            else
                                              return null;
                                          },
                                        ),
                                        SizedBox(height: 8.0),
                                      ],
                                    ),
                                  ),
                                ],
                              ),
                            )
                          ],
                        )
                      ],
                    );
                  }),
            ),
            StreamBuilder<bool>(
                stream: _productBloc.outLoading,
                initialData: false,
                builder: (context, snapshot) {
                  return IgnorePointer(
                    ignoring: !snapshot.data,
                    child: Container(
                      color:
                          snapshot.data ? Colors.black54 : Colors.transparent,
                    ),
                  );
                }),
          ],
        ),
      ),
    );
  }

  void saveProduct() async {
    if (_formKey.currentState.validate()) {
      _formKey.currentState.save();

      _scaffoldKey.currentState.showSnackBar(SnackBar(
        content: Text(
          "Salvando produto...",
          style: TextStyle(color: Colors.white),
        ),
        duration: Duration(minutes: 1),
        backgroundColor: Colors.green[900],
      ));

      bool success = await _productBloc.saveProduct();

      _scaffoldKey.currentState.removeCurrentSnackBar();

      _scaffoldKey.currentState.showSnackBar(SnackBar(
        content: Text(
          success ? "Produto salvo!" : "Erro ao salvar produto!",
          style: TextStyle(color: Colors.white),
        ),
        backgroundColor: success ? Colors.green : Colors.red[900],
      ));
    }
  }
}
