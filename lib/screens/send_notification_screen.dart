import 'package:faen_app_admin/models/notification_sender.dart';
import 'package:flutter/material.dart';
import 'package:flutter_neumorphic/flutter_neumorphic.dart';

class SendNotificationScreen extends StatefulWidget {
  @override
  _SendNotificationScreenState createState() => _SendNotificationScreenState();
}

class _SendNotificationScreenState extends State<SendNotificationScreen> {
  NotificationSender _notification = NotificationSender();
  String urlImage;
  final _titleController = TextEditingController();
  final _bodyController = TextEditingController();
  final _urlController = TextEditingController();

  Color borderColorDropdown = Colors.white70;

  String _topic = "Todos";
  String _topicUsers = "user";
  bool _withImage = false;
  bool _imageValidator = true;

  final _formKey = GlobalKey<FormState>();
  final _scaffoldKey = GlobalKey<ScaffoldState>();

  static const topicOptions = <String>[
    'Todos',
    'Associados',
    'Não Associados',
    'Teste'
  ];

  final List<DropdownMenuItem<String>> _dropDownMenuItemsX = topicOptions
      .map((String value) => DropdownMenuItem(
            value: value,
            child: Text(value, textAlign: TextAlign.center),
          ))
      .toList();

  @override
  Widget build(BuildContext context) {
    InputDecoration _buildDecoration(String label) {
      return InputDecoration(
          filled: true,
          fillColor: Colors.white10,
          enabledBorder: OutlineInputBorder(
              borderSide: BorderSide(
                color: Colors.white54,
              ),
              borderRadius: BorderRadius.circular(15.0)),
          focusedBorder: OutlineInputBorder(
              borderSide: BorderSide(
                color: Colors.green[900],
              ),
              borderRadius: BorderRadius.circular(15.0)),
          focusedErrorBorder: OutlineInputBorder(
              borderSide: BorderSide(
                color: Colors.red[900],
              ),
              borderRadius: BorderRadius.circular(15.0)),
          errorBorder: OutlineInputBorder(
              borderSide: BorderSide(
                color: Colors.red[900],
              ),
              borderRadius: BorderRadius.circular(15.0)),
          labelText: label,
          labelStyle:
              TextStyle(fontFamily: 'BebasNeue', color: Colors.white70));
    }

    final _fieldStyle =
        TextStyle(fontFamily: 'Lato', color: Colors.white, fontSize: 16);

    return Container(
      decoration: BoxDecoration(
          image: DecorationImage(
              image: ExactAssetImage("images/Fundo2.png"), fit: BoxFit.cover)),
      child: Form(
        key: _formKey,
        child: Scaffold(
          key: _scaffoldKey,
          //backgroundColor: Colors.grey[850],
          appBar: AppBar(
              elevation: 0,
              centerTitle: true,
              title: Text("Disparar Notificações")),
          floatingActionButtonLocation: FloatingActionButtonLocation.endFloat,
          floatingActionButton: FloatingActionButton(
            backgroundColor: Colors.green[900].withOpacity(0.87),
            child: Icon(Icons.send),
            onPressed: () {
              if (_formKey.currentState.validate() && _topic != "") {
                sendNotification();
              } else if (_topic == "") {
                borderColorDropdown = Colors.red[900];
              }
            },
          ),
          body: Stack(
            children: <Widget>[
              ListView(
                padding: EdgeInsets.all(16),
                children: <Widget>[
                  Visibility(
                    visible: _withImage,
                    child: Center(
                        child: Container(
                      width: MediaQuery.of(context).size.width * 0.55,
                      height: MediaQuery.of(context).size.width * 0.55,
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.all(Radius.circular(15.0)),
                        color: Theme.of(context).primaryColor.withOpacity(0.50),
                        // shape: BoxShape.circle,
                        image: _urlController.text == ""
                            ? null
                            : DecorationImage(
                                image: NetworkImage(_urlController.text),
                                fit: BoxFit.cover),
                      ),
                      child: _urlController.text == ""
                          ? Container(
                              width: MediaQuery.of(context).size.width * 0.55,
                              height: MediaQuery.of(context).size.width * 0.55,
                              child: Center(
                                child: Icon(
                                  Icons.add_a_photo,
                                  color: _imageValidator
                                      ? Colors.white70
                                      : Colors.red[900],
                                  size: 88.0,
                                ),
                              ),
                            )
                          : null,
                    )),
                  ),
                  SizedBox(height: 10.0),
                  TextFormField(
                    controller: _titleController,
                    style: _fieldStyle,
                    decoration: _buildDecoration("Título da notificação"),
                    validator: (text) {
                      if (text.isEmpty)
                        return "É necessário que a notificação possua um título !";
                      else
                        return null;
                    },
                  ),
                  SizedBox(height: 8.0),
                  TextFormField(
                    controller: _bodyController,
                    style: _fieldStyle,
                    maxLines: 5,
                    decoration: _buildDecoration("Corpo da notificação"),
                    validator: (text) {
                      if (text.isEmpty)
                        return "É necessário que a notificação possua um corpo !";
                      else
                        return null;
                    },
                  ),
                  SizedBox(height: 10.0),
                  Card(
                    margin: EdgeInsets.zero,
                    shape: OutlineInputBorder(
                        borderSide: BorderSide(color: Colors.white54),
                        borderRadius: BorderRadius.circular(12.5)),
                    color: Colors.white12,
                    child: ListTile(
                      contentPadding: EdgeInsets.zero,
                      title: Padding(
                        padding: const EdgeInsets.only(left: 16.0),
                        child: Text(
                          "Enviar para: ",
                          style: TextStyle(
                              fontFamily: 'BebasNeue',
                              color: Colors.white,
                              fontSize: 16.0),
                        ),
                      ),
                      trailing: Padding(
                        padding: const EdgeInsets.only(right: 2.0, left: 2.0),
                        child: DropdownButton(
                          value: _topic,
                          style: TextStyle(
                              fontFamily: 'BebasNeue',
                              color: Colors.green[700]),
                          items: _dropDownMenuItemsX,
                          onChanged: ((newValue) {
                            setState(() {
                              _topic = newValue;
                              switch (newValue) {
                                case "Todos":
                                  _topicUsers = "user";
                                  break;
                                case "Associados":
                                  _topicUsers = "members";
                                  break;
                                case "Teste":
                                  _topicUsers = "test";
                                  break;
                                default:
                                  _topicUsers = "non_members";
                                  break;
                              }
                              /*if (newValue == "Todos")
                                _topicUsers = "user";
                              else if (newValue == "Associados")
                                _topicUsers = "members";
                              else if (newValue == "Teste")
                                _topicUsers = "test";
                              else
                                _topicUsers = "non_members";*/
                            });
                          }),
                        ),
                      ),
                    ),
                  ),
                  SizedBox(height: 10.0),
                  Card(
                    margin: EdgeInsets.zero,
                    shape: OutlineInputBorder(
                        borderSide: BorderSide(
                          color: Colors.white54,
                        ),
                        borderRadius: BorderRadius.circular(12.5)),
                    color: Colors.white12,
                    child: ListTile(
                      title: Text("Deseja enviar uma imagem ?",
                          style: TextStyle(
                              fontFamily: 'BebasNeue',
                              color: Colors.white,
                              fontSize: 16.0)),
                      trailing: NeumorphicCheckbox(
                          padding: EdgeInsets.all(8.0),
                          style: NeumorphicCheckboxStyle(
                            selectedColor: Colors.green[900],
                            disabledColor: Colors.black,
                          ),
                          value: _withImage,
                          onChanged: (value) {
                            setState(() {
                              _withImage = value;
                            });
                          }),
                    ),
                    /*CheckboxListTile(
                        title: Text("Deseja enviar uma imagem ?",
                            style: TextStyle(
                                fontFamily: 'BebasNeue',
                                color: Colors.white,
                                fontSize: 16.0)),
                        value: _withImage,
                        onChanged: (value) {
                          setState(() {
                            _withImage = value;
                          });
                        }),*/
                  ),
                  SizedBox(height: 10.0),
                  Visibility(
                    visible: _withImage,
                    child: TextFormField(
                      controller: _urlController,
                      style: _fieldStyle,
                      //maxLines: 5,
                      decoration: InputDecoration(
                        filled: true,
                        fillColor: Colors.white10,
                        helperText:
                            "Após inserir a url, confira se ela foi carregada no topo da tela.",
                        helperStyle: TextStyle(color: Colors.white70),
                        enabledBorder: OutlineInputBorder(
                            borderSide: BorderSide(
                              color: Colors.white54,
                            ),
                            borderRadius: BorderRadius.circular(15.0)),
                        focusedBorder: OutlineInputBorder(
                            borderSide: BorderSide(
                              color: Colors.green[900],
                            ),
                            borderRadius: BorderRadius.circular(15.0)),
                        focusedErrorBorder: OutlineInputBorder(
                            borderSide: BorderSide(
                              color: Colors.red[900],
                            ),
                            borderRadius: BorderRadius.circular(15.0)),
                        errorBorder: OutlineInputBorder(
                            borderSide: BorderSide(
                              color: Colors.red[900],
                            ),
                            borderRadius: BorderRadius.circular(15.0)),
                        labelText: "Url da imagem",
                        labelStyle: TextStyle(
                            fontFamily: 'BebasNeue', color: Colors.white70),
                      ),

                      onChanged: (_) {
                        setState(() {});
                      },
                      validator: (text) {
                        if (text.isEmpty && _withImage)
                          return "Insira a url da imagem, ou desabilite a opção de enviar com imagem";
                        else
                          return null;
                      },
                    ),
                  ),
                  SizedBox(
                    height: 16,
                  ),
                ],
              ),
            ],
          ),
        ),
      ),
    );
  }

  void sendNotification() async {
    if (_formKey.currentState.validate()) {
      // _formKey.currentState.save();

      _scaffoldKey.currentState.showSnackBar(SnackBar(
        //behavior: SnackBarBehavior.floating,
        content: Text(
          "Preparando para envio...",
          style: TextStyle(color: Colors.white),
        ),
        duration: Duration(minutes: 1),
        backgroundColor: Colors.amber,
      ));

      bool success = _withImage
          ? await _notification.sendNoticationToTopicWithImage(
              title: _titleController.text,
              body: _bodyController.text,
              topic: _topicUsers,
              urlImage: _urlController.text)
          : await _notification.sendNoticationToTopic(
              title: _titleController.text,
              body: _bodyController.text,
              topic: _topicUsers);

      _scaffoldKey.currentState.removeCurrentSnackBar();

      _scaffoldKey.currentState.showSnackBar(SnackBar(
        content: Text(
          success
              ? "Notificação enviada com sucesso !"
              : "Erro ao enviar a notificação !",
          style: TextStyle(color: Colors.white),
        ),
        behavior: SnackBarBehavior.floating,
        elevation: 6.0,
        backgroundColor: success ? Colors.green : Colors.red,
      ));
    }
  }
}
