import 'package:bloc_pattern/bloc_pattern.dart';
import 'package:faen_app_admin/blocs/orders_bloc.dart';
import 'package:faen_app_admin/blocs/user_bloc.dart';
import 'package:faen_app_admin/models/pdf_generator.dart';
import 'package:faen_app_admin/screens/add_user_screen.dart';
import 'package:faen_app_admin/tabs/home_tab.dart';
import 'package:faen_app_admin/tabs/orders_tab.dart';
import 'package:faen_app_admin/tabs/partner_tab.dart';
import 'package:faen_app_admin/tabs/products_tab.dart';
import 'package:faen_app_admin/tabs/users_tab.dart';
import 'package:faen_app_admin/widgets/dialogs/edit_category_dialog.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_icons/flutter_icons.dart';
import 'package:flutter_speed_dial/flutter_speed_dial.dart';
import 'package:pdf/pdf.dart';

import 'edit_partner_screen.dart';

class HomeScreen extends StatefulWidget {
  final int pageX;

  HomeScreen({this.pageX});

  @override
  _HomeScreenState createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> {
  PageController _pageController;
  int _page;
  final _userBloc = BlocProvider.getBloc<UserBloc>();
  final _ordersBloc = BlocProvider.getBloc<OrdersBloc>();

  /*UserBloc _userBloc;
  OrdersBloc _ordersBloc;
  RevenueControlBloc _revenueControlBloc;*/

  @override
  void initState() {
    super.initState();
    _page = widget.pageX ?? 0;
    _pageController = PageController(initialPage: _page);

    /* _userBloc = UserBloc();
    _ordersBloc = OrdersBloc();
    _revenueControlBloc = RevenueControlBloc();*/
  }

  @override
  void dispose() {
    _pageController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: () {
        if (_pageController.page > 0 || _page > 0) {
          _pageController.jumpToPage(0);
          return Future.value(true);
        } else {
          return SystemChannels.platform
                  .invokeMethod<void>('SystemNavigator.pop', true) ??
              false;
        }
      },
      child: Container(
        decoration: const BoxDecoration(
            image: DecorationImage(
                image: ExactAssetImage("images/Fundo2.png"),
                fit: BoxFit.cover)),
        child: Scaffold(
          // drawer: CustomDrawer(_pageController),
          appBar: AppBar(
            centerTitle: true,
            automaticallyImplyLeading: false, //_page != 0 ? true : false,
            leading: _page != 0
                ? IconButton(
                    icon: Icon(
                      Theme.of(context).platform == TargetPlatform.iOS
                          ? Icons.arrow_back_ios
                          : Icons.arrow_back,
                    ),
                    onPressed: () {
                      _pageController.jumpToPage(0);
                    },
                  )
                : null,
            title: Padding(
              padding: const EdgeInsets.all(35.0),
              child: Hero(
                tag: "appBar",
                child:
                    Image.asset("images/AppBarImage.png", fit: BoxFit.contain),
              ),
            ),
          ),
          backgroundColor: Colors.transparent,
          /* bottomNavigationBar: Theme(
            data: Theme.of(context).copyWith(
                canvasColor: Colors.black.withOpacity(0.7),
                primaryColor: Colors.white,
                textTheme: Theme.of(context)
                    .textTheme
                    .copyWith(caption: TextStyle(color: Colors.white70))),
            child: BottomNavigationBar(
                currentIndex: _page,
                onTap: (p) {
                  _pageController.animateToPage(p,
                      duration: Duration(milliseconds: 500),
                      curve: Curves.ease);
                },
                items: [
                  BottomNavigationBarItem(
                      icon: Icon(FontAwesome5Solid.home),
                      title: Text("Home")),
                  BottomNavigationBarItem(
                      icon: Icon(Icons.person), title: Text("Clientes")),
                  BottomNavigationBarItem(
                      icon: Icon(Icons.shopping_cart),
                      title: Text("Pedidos")),
                  BottomNavigationBarItem(
                      icon: Icon(Icons.list), title: Text("Produtos")),
                  BottomNavigationBarItem(
                      activeIcon: Icon(
                        FontAwesome5Solid.handshake,
                      ),
                      icon: Icon(
                        FontAwesome.handshake_o,
                      ),
                      title: Text("Parcerias"))
                ]),
          ),*/
          body: SafeArea(
              child: PageView(
            controller: _pageController,
            physics: NeverScrollableScrollPhysics(),
            onPageChanged: (p) {
              setState(() {
                _page = p;
              });
            },
            children: <Widget>[
              HomeTab(_pageController),
              //FeedTab(),
              //SendNotificationScreen(),
              UsersTab(),
              OrdersTab(),
              ProductsTab(),
              PartnerTab()
            ],
          )),
          floatingActionButton: _buildFloating(),
        ),
      ),
    );
  }

  Widget _buildFloating() {
    switch (_page) {
      case 0:
        return null;
      /* return FloatingActionButton(
          child: Icon(Icons.add),
          backgroundColor: Theme.of(context).primaryColor,
          onPressed: () {
            Navigator.of(context).push(
                MaterialPageRoute(builder: (context) => EditPostFeedScreen()));
          },
        );*/
      case 1:
        return SpeedDial(
          child: Icon(Icons.sort),
          backgroundColor: Colors.green[900].withOpacity(0.87),
          overlayOpacity: 0.4,
          overlayColor: Colors.black,
          children: _buildSpeedDialChilds(),
        );
      case 2:
        return SpeedDial(
          child: Icon(Icons.sort),
          backgroundColor: Colors.green[900].withOpacity(0.87),
          overlayOpacity: 0.4,
          overlayColor: Colors.black,
          children: [
            SpeedDialChild(
                child: Icon(
                  Icons.arrow_downward,
                  color: Colors.green,
                  //color: Theme.of(context).primaryColor,
                ),
                backgroundColor: Colors.white,
                label: "Concluídos Abaixo",
                labelStyle: TextStyle(color: Colors.black, fontSize: 14),
                onTap: () {
                  _ordersBloc.setOrderCriteria(SortCriteria.READY_LAST);
                }),
            SpeedDialChild(
                child: Icon(
                  Icons.arrow_upward,
                  color: Colors.green,
                  //color: Theme.of(context).primaryColor,
                ),
                backgroundColor: Colors.white,
                label: "Concluídos Acima",
                labelStyle: TextStyle(color: Colors.black, fontSize: 14),
                onTap: () {
                  _ordersBloc.setOrderCriteria(SortCriteria.READY_FIRST);
                })
          ],
        );
      case 3:
        return FloatingActionButton(
          child: Icon(Icons.add),
          backgroundColor: Colors.green[900].withOpacity(0.87),
          onPressed: () {
            showDialog(
                context: context, builder: (context) => EditCategoryDialog());
          },
        );
      case 4:
        return FloatingActionButton(
          child: Icon(Icons.add),
          backgroundColor: Colors.green[900].withOpacity(0.87),
          onPressed: () {
            Navigator.of(context).push(
                MaterialPageRoute(builder: (context) => EditPartnerScreen()));
          },
        );
      default:
        return null;
    }
  }

  List<SpeedDialChild> _buildSpeedDialChilds() {
    if (_userBloc.admUserData["typeUser"] == "Administrador" ||
        _userBloc.admUserData["typeUser"] == "Moderador") {
      return [
        SpeedDialChild(
            child: Icon(
              Icons.people,
              color: Colors.green,
              //color: Theme.of(context).primaryColor,
            ),
            backgroundColor: Colors.white,
            label: "Exibir todos",
            labelStyle: TextStyle(color: Colors.black, fontSize: 14),
            onTap: () {
              _userBloc.getNoFilter();
            }),
        SpeedDialChild(
            child: Icon(
              Icons.card_membership,
              color: Colors.green,
              //color: Theme.of(context).primaryColor,
            ),
            backgroundColor: Colors.white,
            label: "Exibir somente Associados",
            labelStyle: TextStyle(color: Colors.black, fontSize: 14),
            onTap: () {
              _userBloc.getFilterMembersOnly();
            }),
        SpeedDialChild(
            child: Icon(
              Icons.person_outline,
              color: Colors.green,
              //color: Theme.of(context).primaryColor,
            ),
            backgroundColor: Colors.white,
            label: "Exibir somente Não Associados",
            labelStyle: TextStyle(color: Colors.black, fontSize: 14),
            onTap: () {
              _userBloc.getFilterNoMembersOnly();
            }),
        SpeedDialChild(
            child: Icon(
              Icons.person_add,
              color: Colors.green,
              //color: Theme.of(context).primaryColor,
            ),
            backgroundColor: Colors.white,
            label: "Adicionar Usuário",
            labelStyle: TextStyle(color: Colors.black, fontSize: 14),
            onTap: () {
              Navigator.of(context).push(
                  MaterialPageRoute(builder: (context) => AddUserScreen()));
            }),
        SpeedDialChild(
            child: Icon(
              Icons.file_download,
              color: Colors.green,
              //color: Theme.of(context).primaryColor,
            ),
            backgroundColor: Colors.white,
            label: "Exportar lista",
            labelStyle: TextStyle(color: Colors.black, fontSize: 14),
            onTap: () async {
              await printPdf();
            }),
        SpeedDialChild(
            child: Icon(
              Icons.share,
              color: Colors.green,
              //color: Theme.of(context).primaryColor,
            ),
            backgroundColor: Colors.white,
            label: "Compartilhar lista",
            labelStyle: TextStyle(color: Colors.black, fontSize: 14),
            onTap: () async {
              await sharePdf();
            }),
      ];
    } else {
      return [
        SpeedDialChild(
            child: Icon(
              Icons.people,
              color: Colors.green,
              //color: Theme.of(context).primaryColor,
            ),
            backgroundColor: Colors.white,
            label: "Exibir todos",
            labelStyle: TextStyle(color: Colors.black, fontSize: 14),
            onTap: () {
              _userBloc.getNoFilter();
            }),
        SpeedDialChild(
            child: Icon(
              Icons.card_membership,
              color: Colors.green,
              //color: Theme.of(context).primaryColor,
            ),
            backgroundColor: Colors.white,
            label: "Exibir somente Associados",
            labelStyle: TextStyle(color: Colors.black, fontSize: 14),
            onTap: () {
              _userBloc.getFilterMembersOnly();
            }),
        SpeedDialChild(
            child: Icon(
              Icons.person_outline,
              color: Colors.green,
              //color: Theme.of(context).primaryColor,
            ),
            backgroundColor: Colors.white,
            label: "Exibir somente Não Associados",
            labelStyle: TextStyle(color: Colors.black, fontSize: 14),
            onTap: () {
              _userBloc.getFilterNoMembersOnly();
            }),
      ];
    }
  }

  Future<void> printPdf() async {
    PdfPageFormat format;
    await PdfGenerator.generateListUserDocument(format, _userBloc.getUsers());
    await PdfGenerator.savePdf();
    await PdfGenerator.printPdf();
  }

  Future<void> sharePdf() async {
    PdfPageFormat format;
    await PdfGenerator.generateListUserDocument(format, _userBloc.getUsers());
    await PdfGenerator.savePdf();
    await PdfGenerator.sharePdf();
  }
}
