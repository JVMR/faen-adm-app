import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:faen_app_admin/widgets/feed_card.dart';
import 'package:flutter/material.dart';
import 'package:flutter_staggered_animations/flutter_staggered_animations.dart';

import 'edit_post_feed_screen.dart';

class FeedScreen extends StatefulWidget {
  @override
  _FeedScreenState createState() => _FeedScreenState();
}

class _FeedScreenState extends State<FeedScreen> {
  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(
          image: DecorationImage(
              image: ExactAssetImage("images/Fundo2.png"), fit: BoxFit.cover)),
      child: Scaffold(
        appBar: AppBar(
          centerTitle: true,
          title: Padding(
            padding: const EdgeInsets.all(35.0),
            child: Hero(
              tag: "appBar",
              child: Image.asset("images/AppBarImage.png", fit: BoxFit.contain),
            ),
          ),
        ),
        floatingActionButton: FloatingActionButton(
          child: Icon(Icons.add),
          backgroundColor: Colors.green[900].withOpacity(0.7),
          onPressed: () {
            Navigator.of(context).push(
                MaterialPageRoute(builder: (context) => EditPostFeedScreen()));
          },
        ),
        backgroundColor: Colors.transparent,
        body: FutureBuilder<QuerySnapshot>(
          future: Firestore.instance
            .collection("feed")
            .orderBy("createdAt", descending: true)
            .getDocuments(),
          builder: (context, snapshot) {
            if (!snapshot.hasData)
              return Center(
                child: CircularProgressIndicator(
                  valueColor: AlwaysStoppedAnimation(Colors.white),
                ),
              );
            else if (snapshot.data.documents.length == 0)
              return Center(
                child: Text(
                  "Não há nenhum post no Feed no momento !",
                  textAlign: TextAlign.center,
                  style: TextStyle(color: Colors.white, fontSize: 16),
                ),
              );
            else
              return AnimationLimiter(
              child: ListView.builder(
                padding: const EdgeInsets.all(4.0),
                itemCount: snapshot.data.documents.length,
                itemBuilder: (context, index) {
                  return AnimationConfiguration.staggeredList(
                      position: index,
                      duration: const Duration(milliseconds: 375),
                      child: SlideAnimation(
                        verticalOffset: 50.0,
                        child: FadeInAnimation(
                            child: FeedCard(
                                snapshot.data.documents[index])),
                      ));
                },
              ),
            );
          },
        ),
      ),
    );
  }
}
