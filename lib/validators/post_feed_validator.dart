class PostFeedValidator {
  /*String validateImages(List images) {
    if (images.isEmpty) return "Adicione imagens do produto";
    return null;
  }*/

  String validateTitle(String text) {
    if (text.isEmpty) return "Preencha o título do Post !";
    return null;
  }

  String validateSubTitle(String text) {
    if (text.isEmpty) return "Preencha o subtítulo do Post !";
    return null;
  }

  String validateDescription(String text) {
    if (text.isEmpty) return "Preencha a descrição do Post !";
    return null;
  }
}
